﻿&НаКлиенте
Перем КонтекстЭДОКлиент Экспорт;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Ссылка = Параметры.Ссылка;
	
	КонтекстЭДОСервер = ДокументооборотСКОВызовСервера.ПолучитьОбработкуЭДО();
	
	ИспользуетсяРежимТестирования = КонтекстЭДОСервер.ИспользуетсяРежимТестирования();
	
	СведенияПоОбъекту = КонтекстЭДОСервер.СведенияПоОтправляемымОбъектам(Ссылка);

	НаименованиеОтчета			= СведенияПоОбъекту.Наименование;
	Организация					= СведенияПоОбъекту.Организация;
	ВидКонтролирующегоОргана	= СведенияПоОбъекту.ВидКонтролирующегоОргана;
	КодКонтролирующегоОргана	= СведенияПоОбъекту.КодКонтролирующегоОргана;
	ПредставлениеГосОргана		= СведенияПоОбъекту.ПредставлениеКонтролирующегоОргана;
	ПредставлениеПериода		= СведенияПоОбъекту.ПредставлениеПериода;
	СтраницаЖурнала				= СведенияПоОбъекту.СтраницаЖурнала;

	// Заполняем форму
	ЗаполнитьДанныеВШапкеФормы();
	ПолучитьПоследнююОтправку();
	
	ОбновитьТаблицуЭтаповОтправки();
	УправлениеЭУ(Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПриОткрытииЗавершение", ЭтотОбъект);
	
	ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПротоколНажатие(Элемент)
	
	НомерЭтапа = Число(Прав(Элемент.Имя, 1)) - 1;
	Протокол = ЭтапыСостояния[НомерЭтапа].Протокол;
	
	КонтекстЭДОКлиент.ОткрытьПротокол(Ссылка, ВидКонтролирующегоОргана, Протокол);
	
КонецПроцедуры

&НаКлиенте
Процедура КритическиеОшибкиОбработкаНавигационнойСсылки(Элемент, НавигационнаяСсылка, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	КонтекстЭДОКлиент.ПоказатьКритическиеОшибкиПоСсылке(Ссылка);
КонецПроцедуры

&НаКлиенте
Процедура ПодробнаяИнформацияНажатие(Элемент)
	
	// ФСС
	Если ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСС") Тогда
						
		ПоследняяОтправка = КонтекстЭДОКлиент.ПолучитьПоследнююОтправкуОтчетаВФСС(Ссылка);
		ПоказатьЗначение(, ПоследняяОтправка);
		
	// ФСРАР
	ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСРАР") Тогда
		
		ПоследняяОтправка = КонтекстЭДОКлиент.ПолучитьПоследнююОтправкуОтчетаВФСРАР(Ссылка);
		ПоказатьЗначение(, ПоследняяОтправка);
		
	// РПН
	ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.РПН") Тогда
		
		ПоследняяОтправка = КонтекстЭДОКлиент.ПолучитьПоследнююОтправкуОтчетаВРПН(Ссылка);
		ПоказатьЗначение(, ПоследняяОтправка);
		
	Иначе
		
		// Остальные контролирующие органы
		ПоследнийЦиклОбмена = ПолучитьПоследнийЦиклОбменаДляДаннойСсылки(Ссылка);
		Если ЗначениеЗаполнено(ПоследнийЦиклОбмена) Тогда
			ПоказатьЗначение(, ПоследнийЦиклОбмена);
		Иначе
			ПоказатьПредупреждение(, НСтр("ru = 'Подробная информация отсутствует'"));
		КонецЕсли;
		
	КонецЕсли;
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормы
//Код процедур и функций
#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВыгрузитьПакетДокументовДляПредставления(Команда)
	
	Если ЗначениеЗаполнено(ПоследнийЦиклОбмена) Тогда
		
		ТекстВопроса = НСтр("ru = 'Выгрузить пакет документов для представления по месту требования?'");
		
		Оповещение = Новый ОписаниеОповещения("ВыгрузитьПакетыПоДокументооборотамСдачиОтчетностиВФНС",ЭтотОбъект);
		ПоказатьВопрос(Оповещение, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Отправить(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ОтправитьЗавершение", ЭтотОбъект);
	КонтекстЭДОКлиент.ОтправитьНеотправленныеИзвещенияОПриеме(Ссылка, Организация, ВидКонтролирующегоОргана, ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтправитьЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	ОбновитьТаблицуЭтаповОтправки();
	УправлениеЭУ();
	
	// Перерисовываем другие формы при необходимости
	ОповеститьОбОкончанииОтправки();
	
КонецПроцедуры

&НаКлиенте
Процедура Обновить(Команда)
	
	ОписаниеОповещения 	= Новый ОписаниеОповещения("ОбновитьЗавершение", ЭтотОбъект);
	
	Если ТипЗнч(Ссылка) = Тип("ДокументСсылка.ЗаявлениеАбонентаСпецоператораСвязи") Тогда
		КонтекстЭДОКлиент.ОбновитьСостояниеЗаявления(ОписаниеОповещения, ЭтаФорма, Ссылка);
	Иначе
		КонтролирующийОрган = ЭлектронныйДокументооборотСКонтролирующимиОрганамиВызовСервера.ИмяПеречисления(ВидКонтролирующегоОргана);
		КонтекстЭДОКлиент.ОсуществитьОбменПоОрганизации(ЭтаФорма, Организация, ОписаниеОповещения, КонтролирующийОрган, Ссылка);
	КонецЕСли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	// перерисовываем элементы
	ПолучитьПоследнююОтправку();
	
	ОбновитьТаблицуЭтаповОтправки();
	УправлениеЭУ();
	
	// Перерисовываем другие формы при необходимости
	ОповеститьОбОкончанииОтправки();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыгрузитьВсеФайлыИПодписи(Команда)
	
	ПоследнийЦиклОбмена = ПолучитьПоследнийЦиклОбменаДляДаннойСсылки(Ссылка);
	Если ЗначениеЗаполнено(ПоследнийЦиклОбмена) Тогда
		ВыгрузитьЦиклыОбмена();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВыгрузитьОтправленныйПакетДокументов(Команда)
	
	Если ВидКонтролирующегоОргана <> ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСС")
		И ВидКонтролирующегоОргана <> ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСРАР")
		И ВидКонтролирующегоОргана <> ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.РПН")
		ИЛИ НЕ ЗначениеЗаполнено(ВидКонтролирующегоОргана) Тогда
		Возврат;
	КонецЕсли;
	
	ПолучитьПоследнююОтправку();
	
	ИмяФайлаПакета = "";
	Адрес = ПолучитьАдресФайлаПакета(ПоследнийЦиклОбмена, ИмяФайлаПакета);
	ПолучитьФайл(Адрес, ИмяФайлаПакета);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыгрузитьПолученнуюКвитанцию(Команда)
	
	Если ВидКонтролирующегоОргана <> ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСС") 
		ИЛИ НЕ ЗначениеЗаполнено(ВидКонтролирующегоОргана) Тогда
		Возврат;
	КонецЕсли;
	
	ПолучитьПоследнююОтправку();

	ИдентификаторОтправкиНаСервере = "";
	Адрес = ПолучитьАдресФайлаКвитанции(ПоследнийЦиклОбмена, ИдентификаторОтправкиНаСервере);
	ПолучитьФайл(Адрес, ИдентификаторОтправкиНаСервере + ".p7e");
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаПодтвердитьПрием(Команда)    
	
	ОписаниеОповещения = Новый ОписаниеОповещения(
		"ПослеПодтвержденияПриемаИлиОтказаВПриеме", 
		ЭтотОбъект);
		
	КонтекстЭДОКлиент.СоздатьРезультатПриемаВходящейОписиИнтерактивноПоДокументуОписи(Ссылка, Истина, ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаОтказатьВПриеме(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения(
		"ПослеПодтвержденияПриемаИлиОтказаВПриеме", 
		ЭтотОбъект);
	
	КонтекстЭДОКлиент.СоздатьРезультатПриемаВходящейОписиИнтерактивноПоДокументуОписи(Ссылка, Ложь, ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура НадписьОтветПоказатьНажатие(Элемент)
	
	КонтекстЭДОКлиент.НажатиеНаКнопкуПоказатьОтветыПоТребованию(Ссылка);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПослеПодтвержденияПриемаИлиОтказаВПриеме(Результат, ВходящийКонтекст) Экспорт
	
	ОбновитьТаблицуЭтаповОтправки();
	УправлениеЭУ();
	
	// Перерисовываем другие формы при необходимости
	ОповеститьОбОкончанииОтправки();
	
КонецПроцедуры

&НаСервере
Функция ЭлементНаименованиеЭтапа(НомерСтроки)
	Возврат Элементы["НаименованиеЭтапа" + Строка(НомерСтроки)];
КонецФункции

&НаСервере
Функция ЭлементДатаСовершенияЭтапа(НомерСтроки)
	Возврат Элементы["ДатаСовершенияЭтапа" + Строка(НомерСтроки)];
КонецФункции

&НаСервере
Функция ЭлементКомментарийЭтапа(НомерСтроки)
	Возврат Элементы["КомментарийЭтапа" + Строка(НомерСтроки)];
КонецФункции

&НаСервере
Функция ЭлементПротоколЭтапа(НомерСтроки)
	Возврат Элементы["ПротоколЭтапа" + Строка(НомерСтроки)];
КонецФункции

&НаКлиенте
Процедура ВыгрузитьПакетыПоДокументооборотамСдачиОтчетностиВФНС(Ответ, Параметры) Экспорт
	
	Если Ответ <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	КонтекстЭДОКлиент.ВыгрузитьПакетыПоДокументооборотамСдачиОтчетностиВФНС(ПоследнийЦиклОбмена)
	
КонецПроцедуры

&НаКлиенте
Процедура ВыгрузитьЦиклыОбмена()
	
	ТекстВопроса = НСтр("ru = 'Выгрузить все файлы и подписи?'");
	Оповещение = Новый ОписаниеОповещения("ПродолжитьВыгрузкуЦикловОбмена", ЭтотОбъект);
	ПоказатьВопрос(Оповещение, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
	
КонецПроцедуры

&НаКлиенте
Процедура ПродолжитьВыгрузкуЦикловОбмена(Ответ, Параметры) Экспорт
	
	Если Ответ <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	КонтекстЭДОКлиент.ВыгрузитьЦиклыОбмена(ПоследнийЦиклОбмена, Истина, Истина);
	
КонецПроцедуры

&НаСервере
Процедура УправлениеЭУ(Отказ = Ложь)
	
	КонтекстЭДОСервер = ДокументооборотСКОВызовСервера.ПолучитьОбработкуЭДО();
	ТекущееСостояниеОтправки = КонтекстЭДОСервер.ТекущееСостояниеОтправки(Ссылка, ВидКонтролирующегоОргана);
	
	Если ТекущееСостояниеОтправки = Неопределено Тогда
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	
	// Определяем состав пунктов меню Выгрузка
	ОтпределитьСоставМенюВыгрузка();
	
	// Неотправленные извещения
	Элементы.БлокНеотправленныхСообщений.Видимость = ТекущееСостояниеОтправки.НеотправленныеИзвещения.ЕстьНеотправленныеИзвещения;
	
	// Критические сообщения
	// Проверка на Такси - это обход ошибки платформы - в управляемых формах форматированная строка с картинкой не выводится, в Такси - выводится
	ЭтоВариантИнтерфейсаТакси = ЭлектронныйДокументооборотСКонтролирующимиОрганамиВызовСервера.ЭтоВариантИнтерфейсаТакси(); 
	
	// Критические сообщения
	ЕстьКритическиеОшибки = ТекущееСостояниеОтправки.ЕстьКритическиеОшибки;
	
	// Проверка на Такси - это обход ошибки платформы - в управляемых формах форматированная строка с картинкой не выводится, в Такси - выводится
	ЭтоВариантИнтерфейсаТакси = ЭлектронныйДокументооборотСКонтролирующимиОрганамиВызовСервера.ЭтоВариантИнтерфейсаТакси(); 
	
	Элементы.БлокКритическихОшибок.Видимость = ТекущееСостояниеОтправки.ЕстьКритическиеОшибки;
	Элементы.ЗначокКритическойОшибки.Видимость = НЕ ЭтоВариантИнтерфейсаТакси И ЕстьКритическиеОшибки;
	
	// Текст
	МассивПодстрок = Новый Массив;
	Если ЭтоВариантИнтерфейсаТакси Тогда 
		МассивПодстрок.Добавить(Элементы.ЗначокКритическойОшибки.Картинка);
		МассивПодстрок.Добавить(" ");
	КонецЕсли;
	МассивПодстрок.Добавить(НСтр("ru = 'Обнаружены '"));
	МассивПодстрок.Добавить(Новый ФорматированнаяСтрока(НСтр("ru = 'критические ошибки'"),,,, "НажатиеНаКритическиеОшибки"));
	
	КритическиеОшибки = Новый ФорматированнаяСтрока(МассивПодстрок);
	
	// Таблица состояний
	ТаблицаСостояний = РеквизитФормыВЗначение("ЭтапыСостояния");
	
	ЭтоДокументыРеализации = ТипЗнч(Ссылка) = Тип("СправочникСсылка.ДокументыРеализацииПолномочийНалоговыхОрганов");
	Элементы.БлокЭтаповДокументовРеализацииПолномочий.Видимость = ЭтоДокументыРеализации;
		
	Если ЭтоДокументыРеализации Тогда
		// У требований свои этапы с кнопками
		СкрытьВсеСостоянияНаФормеКромеПервого();
		ПрорисоватьБлокЭтаповДокументовРеализацииПолномочий(ТекущееСостояниеОтправки);
	Иначе
		СкрытьЛишниеСостоянияНаФорме(ТаблицаСостояний);
	КонецЕсли;
	ПрорисоватьТаблицуСостояний(ТаблицаСостояний);
	
	УстановитьВидимостьКнопкиОбновить(КонтекстЭДОСервер, ТекущееСостояниеОтправки);
	
	УстановитьВидимостьИсторииОтправки();
				
КонецПроцедуры
	
&НаСервере
Процедура УстановитьВидимостьКнопкиОбновить(КонтекстЭДОСервер, ТекущееСостояниеОтправки)

	Если ТипЗнч(Ссылка) = Тип("ДокументСсылка.ЗаявлениеАбонентаСпецоператораСвязи") Тогда
		Элементы.ФормаОбновить.Видимость = КонтекстЭДОСервер.КнопкаОбновленияВЗаявленииНаПодключениеДоступна(Ссылка);
		Элементы.ПодробнаяИнформация.Видимость = Ложь;
	Иначе
		ТекущийЭтап = ТекущееСостояниеОтправки.ТекущийЭтапОтправки;
		Если ТекущийЭтап <> Неопределено Тогда
			СостояниеСдачи = ТекущийЭтап.СостояниеСдачиОтчетности;
			Элементы.ФормаОбновить.Видимость = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.ВидимостьКнопкиОтправкаПоСостоянию(СостояниеСдачи);
		КонецЕсли;
		Элементы.ПодробнаяИнформация.Видимость = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьВидимостьИсторииОтправки()

	Элементы.ИсторияОтправок.Видимость = 
		ИспользуетсяРежимТестирования
		И (СтраницаЖурнала = Перечисления.СтраницыЖурналаОтчетность.Отчеты
		ИЛИ СтраницаЖурнала = Перечисления.СтраницыЖурналаОтчетность.Уведомления);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеВШапкеФормы()

	Период = ПредставлениеПериода;
	Если Найти(Период, "0001") > 0 Тогда
		Период = "";
	КонецЕсли;
	Период = ?(ЗначениеЗаполнено(Период)," (" + Период + ")", "");
	Элементы.НаименованиеОтчета.Заголовок	= НаименованиеОтчета + Период;
	
	Получатель 	= ПредставлениеГосОргана;
	Отправитель = Организация;
	
	// Если это входящая переписка заполняем получателя и отправителя наоборот
	Если ТипЗнч(Ссылка) = Тип("СправочникСсылка.ПерепискаСКонтролирующимиОрганами") Тогда
		Если Ссылка.Статус = Перечисления.СтатусыПисем.Полученное Тогда
			Получатель 	= Организация;
			Отправитель = ПредставлениеГосОргана;
		КонецЕсли;
	КонецЕсли;
	
	// Отправитель
	Если ЗначениеЗаполнено(Отправитель) Тогда
		Элементы.ОтКого.Заголовок = Отправитель;
	Иначе
		Элементы.ЗаголовокОтКого.Видимость 	= Ложь;
		Элементы.ОтКого.Видимость 			= Ложь;
	КонецЕсли;
	
	// Получатель
	Если ЗначениеЗаполнено(Получатель) Тогда
		Элементы.Кому.Заголовок	= Получатель;
	Иначе
		Элементы.ЗаголовокКому.Видимость 	= Ложь;
		Элементы.Кому.Видимость 			= Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УдалитьСтрокиТаблицыСостоянияСоСтатусомНеОтправлено(ТаблицаСостояний)

	// Удаляем строку со статусом "Не отправлено"
	Для каждого СтрокаТаблицыСостояний Из ТаблицаСостояний Цикл
		Если СтрокаТаблицыСостояний.ТекстНадписи = НСтр("ru = 'Не отправлено'") Тогда
			ТаблицаСостояний.Удалить(СтрокаТаблицыСостояний);
		КонецЕсли;
	КонецЦикла; 

КонецПроцедуры

&НаСервере
Процедура СкрытьЛишниеСостоянияНаФорме(ТаблицаСостояний)
	
	// Скрываем лишние блоки
	ЭлементыТаблицыЭтапов = Элементы.ОбщийБлокЭтапов.ПодчиненныеЭлементы;
	Для каждого ЭлементФормы Из ЭлементыТаблицыЭтапов Цикл
		ИмяЭлемента = ЭлементФормы.Имя;
		Если Найти(ИмяЭлемента, "БлокЭтапаОтправки") > 0 Тогда
			НомерБлока = Число(СтрЗаменить(ИмяЭлемента, "БлокЭтапаОтправки",""));
			Если НомерБлока > ТаблицаСостояний.Количество() Тогда
				ЭлементФормы.Видимость = Ложь;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;

КонецПроцедуры

&НаСервере
Процедура СкрытьВсеСостоянияНаФормеКромеПервого()
	
	// Скрываем лишние блоки
	ЭлементыТаблицыЭтапов = Элементы.ОбщийБлокЭтапов.ПодчиненныеЭлементы;
	Для каждого ЭлементФормы Из ЭлементыТаблицыЭтапов Цикл
		ИмяЭлемента = ЭлементФормы.Имя;
		Если Найти(ИмяЭлемента, "БлокЭтапаОтправки") > 0 Тогда
			НомерБлока = Число(СтрЗаменить(ИмяЭлемента, "БлокЭтапаОтправки",""));
			Если НомерБлока > 1 Тогда
				ЭлементФормы.Видимость = Ложь;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;

КонецПроцедуры

&НаСервере
Функция НомерТекущегоСостоянияВТаблицеСостояний(ТаблицаСостояний)
	
	НомерТекущегоЭтапа = 0;
	Для каждого СтрокаТаблицыСостояний Из ТаблицаСостояний Цикл
		
		НомерСтроки = ТаблицаСостояний.Индекс(СтрокаТаблицыСостояний) + 1;
		Если СтрокаТаблицыСостояний.ЭтапПройден Тогда
			 НомерТекущегоЭтапа = НомерСтроки;
		КонецЕсли; 
		
	КонецЦикла;
	
	Возврат НомерТекущегоЭтапа;

КонецФункции

&НаСервере
Процедура ПрорисоватьТаблицуСостояний(ТаблицаСостояний)
	
	// Определяем номер текущего этапа
	НомерТекущегоЭтапа = НомерТекущегоСостоянияВТаблицеСостояний(ТаблицаСостояний);
	
	// Заполняем данные в состояниях
	Для каждого СтрокаТаблицыСостояний Из ТаблицаСостояний Цикл
		
		НомерСтроки = ТаблицаСостояний.Индекс(СтрокаТаблицыСостояний) + 1;
		
		// Определяем текст надписей
		
		// Наименование
		ЭлементНаименованиеЭтапа(НомерСтроки).Заголовок = СтрокаТаблицыСостояний.ТекстНадписи;
		
		// Дата
		Если ЗначениеЗаполнено(СтрокаТаблицыСостояний.Дата) Тогда
			
			Если ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФНС И СтрокаТаблицыСостояний.ТекстНадписи = "Принято в обработку" Тогда
				ДатаНаступленияЭтапа = Формат(СтрокаТаблицыСостояний.Дата, "ДЛФ=D");
			Иначе
				ДатаНаступленияЭтапа = СтрокаТаблицыСостояний.Дата;
			КонецЕсли;
				
			ЭлементДатаСовершенияЭтапа(НомерСтроки).Заголовок = ДатаНаступленияЭтапа;
		Иначе
			ЭлементДатаСовершенияЭтапа(НомерСтроки).Заголовок = "";
		КонецЕсли;
		
		// Комментарий выводим только для текущего состояния
		Если ЗначениеЗаполнено(СтрокаТаблицыСостояний.КомментарийКСостоянию) И НомерСтроки = НомерТекущегоЭтапа Тогда
			ЭлементКомментарийЭтапа(НомерСтроки).Заголовок = СтрокаТаблицыСостояний.КомментарийКСостоянию;
			ЭлементКомментарийЭтапа(НомерСтроки).Видимость = Истина;
		Иначе
			ЭлементКомментарийЭтапа(НомерСтроки).Видимость = Ложь;
		КонецЕсли;
		
		// Протокол выводим все время
		Если ЗначениеЗаполнено(СтрокаТаблицыСостояний.НаименованиеПротокола) Тогда 
			 ЭлементПротоколЭтапа(НомерСтроки).Заголовок = СтрокаТаблицыСостояний.НаименованиеПротокола;
			 ЭлементПротоколЭтапа(НомерСтроки).Видимость = Истина;
		 Иначе
			// Если протокол отсуствует, то комментарий должен подняться на место протокола
			Если ЗначениеЗаполнено(СтрокаТаблицыСостояний.КомментарийКСостоянию) И НомерСтроки = НомерТекущегоЭтапа Тогда // обход ошибки платформы
				ЭлементПротоколЭтапа(НомерСтроки).Видимость = Ложь;
			Иначе
				ЭлементПротоколЭтапа(НомерСтроки).Заголовок = "";
				ЭлементПротоколЭтапа(НомерСтроки).Видимость = Истина;
			КонецЕсли;
		КонецЕсли;
		
		// Определяем фон и доступность
		СостояниеСдачиОтчетности 	= СтрокаТаблицыСостояний.СостояниеСдачиОтчетности;
		ЭтапПройден 				= СтрокаТаблицыСостояний.ЭтапПройден;
		ЭлементыБлокЭтапа 			= Элементы["БлокЭтапа" + Строка(НомерСтроки)];
		ЭлементыПротоколЭтапа		= Элементы["ПротоколЭтапа" + Строка(НомерСтроки)];
		ЭлементыНаименованиеЭтапа	= Элементы["НаименованиеЭтапа" + Строка(НомерСтроки)];
		
		Если ЭтапПройден И НомерСтроки = НомерТекущегоЭтапа Тогда
			
			// Определяем цвет фона
			ЦветФона = ЦветаСтиля.ЦветФонаНеначавшейсяОтправки;
			
			Если СостояниеСдачиОтчетности = Перечисления.СостояниеСдачиОтчетности.ДокументооборотНачат Тогда
				ЦветФона = ЦветаСтиля.ЦветФонаТекущейОтправки;
			ИначеЕсли СостояниеСдачиОтчетности = Перечисления.СостояниеСдачиОтчетности.ОтрицательныйРезультатДокументооборота Тогда
				ЦветФона = ЦветаСтиля.ЦветФонаОшибкиОтправки;
			ИначеЕсли СостояниеСдачиОтчетности = Перечисления.СостояниеСдачиОтчетности.ПоложительныйРезультатДокументооборота Тогда
				ЦветФона = ЦветаСтиля.ЦветФонаУдачнойОтправки;
			Иначе
				ЦветФона = ЦветаСтиля.ЦветФонаНеначавшейсяОтправки;
			КонецЕсли;
			
			ЭлементыБлокЭтапа.ЦветФона = ЦветФона; 
			ЭлементыБлокЭтапа.Доступность = Истина;
			
			ЭлементыПротоколЭтапа.ЦветТекста = ЦветаСтиля.ЦветГиперссылкиБРО;
			ЭлементыНаименованиеЭтапа.ЦветТекста = ЦветаСтиля.ЦветТекстаФормы;
			
		ИначеЕсли НЕ ЭтапПройден Тогда
			
			// Делаем непройденные этапы недоступными
			ЭлементыБлокЭтапа.ЦветФона 		= ЦветаСтиля.БазовыйЦветФонаЭтапаОтправки;
			ЭлементыБлокЭтапа.Доступность 	= Ложь;
			// Гиперссылки протоколов делаем серыми
			ЭлементыПротоколЭтапа.ЦветТекста = ЦветаСтиля.ЦветШрифтаНенаступившегоЭтапа;
			// Наименование этапа делаем серым
			ЭлементыНаименованиеЭтапа.ЦветТекста = ЦветаСтиля.ЦветШрифтаНенаступившегоЭтапа;
			
		ИначеЕсли ЭтапПройден Тогда
			
			// Делаем непройденные этапы недоступными
			ЭлементыБлокЭтапа.ЦветФона 		= ЦветаСтиля.БазовыйЦветФонаЭтапаОтправки;
			ЭлементыБлокЭтапа.Доступность 	= Истина;
			// Гиперссылки протоколов делаем серыми
			ЭлементыПротоколЭтапа.ЦветТекста = ЦветаСтиля.ЦветГиперссылкиБРО;
			// Наименование этапа делаем серым
			ЭлементыНаименованиеЭтапа.ЦветТекста = ЦветаСтиля.ЦветТекстаФормы;
			
		КонецЕсли;
		
	КонецЦикла;


КонецПроцедуры

&НаСервере
Процедура ПрорисоватьБлокЭтаповДокументовРеализацииПолномочий(ТекущееСостояниеОтправки)
	
	НадписьОтветПоказатьЗаголовок 	= "";
	НадписьПанелиПриемаЗаголовок 	= "";
	ГруппаКнопкиПриемаВидимость 	= Ложь;
	ГруппаПанельОтправкиЦветФона	= ЦветаСтиля.ЦветФонаПодтвержденногоПолучения;	//серый
	
	Если ТекущееСостояниеОтправки <> Неопределено Тогда
		ТекущийЭтапОтправки = ТекущееСостояниеОтправки.ТекущийЭтапОтправки;
		Если ТекущийЭтапОтправки <> Неопределено Тогда
			
			ТекстДатаПриемаОписиНами 		= Формат(ТекущийЭтапОтправки.Дата, "ДЛФ=DT; ДП=' '");
			НадписьСтатусаЗаголовок 		= ТекущийЭтапОтправки.ТекстНадписи;
			НадписьОтветПоказатьЗаголовок 	= ТекущийЭтапОтправки.НаименованиеПротокола;
			НадписьПанелиПриемаЗаголовок 	= ТекущийЭтапОтправки.КомментарийКСостоянию;
			
			СостояниеСдачиОтчетности = ТекущийЭтапОтправки.СостояниеСдачиОтчетности;
			
			ГруппаКнопкиПриемаВидимость 	= СостояниеСдачиОтчетности = Перечисления.СостояниеСдачиОтчетности.ТребуетсяПодтверждениеПриема; 
			ГруппаПанельОтправкиЦветФона 	= ДокументооборотСКОВызовСервера.ЦветФонаПанелиОтправкиПоСтатусу(СостояниеСдачиОтчетности);
			
		КонецЕсли;
	КонецЕсли;
	
	Элементы.ГруппаПанельОтправки.ЦветФона = ГруппаПанельОтправкиЦветФона;
	Элементы.НадписьСтатуса.Заголовок = НадписьСтатусаЗаголовок;
	
	Если НадписьОтветПоказатьЗаголовок = "" Тогда
		Элементы.НадписьОтветПоказать.Видимость = Ложь;
	Иначе
		Элементы.НадписьОтветПоказать.Заголовок = НадписьОтветПоказатьЗаголовок;
	КонецЕсли;
	
	Если НадписьПанелиПриемаЗаголовок = "" Тогда
		Элементы.НадписьПанелиПриема.Видимость = Ложь;
	Иначе
		Элементы.НадписьПанелиПриема.Заголовок 	= НадписьПанелиПриемаЗаголовок;	
	КонецЕсли;
	
	Элементы.ГруппаКнопкиПриема.Видимость 	= ГруппаКнопкиПриемаВидимость;
	Элементы.ДатаСовершенияЭтапа.Видимость  = НЕ ГруппаКнопкиПриемаВидимость;
	Элементы.ДатаСовершенияЭтапа.Заголовок 	= ТекстДатаПриемаОписиНами;
	
КонецПроцедуры

&НаСервере
Процедура ОтпределитьСоставМенюВыгрузка()

	Элементы.ВыгрузитьПакетДокументовДляПредставления.Видимость = Ложь;
	Элементы.ВыгрузитьВсеФайлыИПодписи.Видимость 				= Ложь;
	Элементы.ВыгрузитьОтправленныйПакетДокументов.Видимость 	= Ложь;
	Элементы.ВыгрузитьПолученнуюКвитанцию.Видимость 			= Ложь;
	
	// Выгрузить пакет документов для представления по месту требования
	Если ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФНС Тогда
		Если ЗначениеЗаполнено(ПоследнийЦиклОбмена) Тогда
			Если ПоследнийЦиклОбмена.Тип = Перечисления.ТипыЦикловОбмена.НалоговаяИлиБухгалтерскаяОтчетность Тогда
				Элементы.ВыгрузитьПакетДокументовДляПредставления.Видимость = Истина;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	
	// Все файлы и подписи
	Если ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФНС 
		ИЛИ ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ПФР
		ИЛИ ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФСГС	Тогда
		Элементы.ВыгрузитьВсеФайлыИПодписи.Видимость = Истина;
	КонецЕсли;
	
	// Отправленный пакет документов
	Если ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФСС 
		ИЛИ ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФСРАР
		ИЛИ ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.РПН Тогда
		Элементы.ВыгрузитьОтправленныйПакетДокументов.Видимость = Истина;
	КонецЕсли;
	
	// Полученную квитанцию
	Если ВидКонтролирующегоОргана = Перечисления.ТипыКонтролирующихОрганов.ФСС Тогда
		Если ЗначениеЗаполнено(ПоследнийЦиклОбмена) Тогда
			Если ПоследнийЦиклОбмена.СтатусОтправки <> Перечисления.СтатусыОтправки.Отправлен
				И ПоследнийЦиклОбмена.СтатусОтправки <> Перечисления.СтатусыОтправки.НеПринят Тогда
				Элементы.ВыгрузитьПолученнуюКвитанцию.Видимость = Истина;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьАдресФайлаПакета(ОтправкаСсылка, ИмяФайлаПакета = "")
	
	ИмяФайлаПакета = ОтправкаСсылка.ИмяФайлаПакета; 
	Возврат ПоместитьВоВременноеХранилище(ОтправкаСсылка.ЗашифрованныйПакет.Получить());
	
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьАдресФайлаКвитанции(ОтправкаСсылка, ИдентификаторОтправкиНаСервере)
	
	ИдентификаторОтправкиНаСервере = ОтправкаСсылка.ИдентификаторОтправкиНаСервере;
	Возврат ПоместитьВоВременноеХранилище(ОтправкаСсылка.Квитанция.Получить());
	
КонецФункции

&НаСервере
Процедура ПолучитьПоследнююОтправку()
	
	УстановитьПривилегированныйРежим(Истина);
	
	КонтекстЭДОСервер = ДокументооборотСКОВызовСервера.ПолучитьОбработкуЭДО();
	
	Если ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСС") Тогда
		ПоследнийЦиклОбмена = КонтекстЭДОСервер.ПолучитьПоследнююОтправкуОтчетаВФСС(Ссылка);
	ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСРАР") Тогда
		ПоследнийЦиклОбмена = КонтекстЭДОСервер.ПолучитьПоследнююОтправкуОтчетаВФСРАР(Ссылка);
	ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.РПН") Тогда
		ПоследнийЦиклОбмена = КонтекстЭДОСервер.ПолучитьПоследнююОтправкуОтчетаВРПН(Ссылка);
	Иначе
		ПоследнийЦиклОбмена = КонтекстЭДОСервер.ПолучитьПоследнийЦиклОбмена(Ссылка);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытииЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьПоследнийЦиклОбменаДляДаннойСсылки(Ссылка)

	КонтекстЭДОСервер = ДокументооборотСКОВызовСервера.ПолучитьОбработкуЭДО();

	Предмет = Ссылка;
	Если ТипЗнч(Ссылка) = Тип("СправочникСсылка.ДокументыРеализацииПолномочийНалоговыхОрганов") Тогда
		
		// Для документа реализации полномочий не существует цикла обмена, существует только для описи в целом 
		Предмет = КонтекстЭДОСервер.ПолучитьОписьВходящихДокументовПоТребованию(Ссылка);
		
	КонецЕсли;
	
	Возврат КонтекстЭДОСервер.ПолучитьПоследнийЦиклОбмена(Предмет);

КонецФункции

&НаСервере
Процедура ОбновитьТаблицуЭтаповОтправки()
	
	// Заполняем всю таблицу состояний
	КонтекстЭДОСервер = ДокументооборотСКОВызовСервера.ПолучитьОбработкуЭДО();
	
	ТаблицаЭтаповОтправки = КонтекстЭДОСервер.ТаблицаЭтаповОтправки(Ссылка, ВидКонтролирующегоОргана);
	Если ТаблицаЭтаповОтправки = Неопределено Тогда 
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	УдалитьСтрокиТаблицыСостоянияСоСтатусомНеОтправлено(ТаблицаЭтаповОтправки);
	ЗначениеВРеквизитФормы(ТаблицаЭтаповОтправки, "ЭтапыСостояния");
	
КонецПроцедуры

&НаКлиенте
Процедура ОповеститьОбОкончанииОтправки()
	
	ПараметрыОповещения = Новый Структура;
	ПараметрыОповещения.Вставить("Организация", Организация);
	ПараметрыОповещения.Вставить("Ссылка", 		Ссылка);
	
	Оповестить("Завершение отправки", ПараметрыОповещения, Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ИсторияОтправокНажатие(Элемент)
	
	Попытка
	
		Если ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФНС")
			ИЛИ ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ПФР")
			ИЛИ ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСГС") Тогда
			
			ОткрытьИсториюОтправки();
			
		Иначе
			
			ИмяФормыИсторииОтправок = "";
			Если ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСС") Тогда
				ИмяФормыИсторииОтправок = "СписокОтправокПоОтчетуФСС";
			ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСРАР") Тогда
				ИмяФормыИсторииОтправок = "СписокОтправокПоОтчетуФСРАР";
			ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.РПН") Тогда
				ИмяФормыИсторииОтправок = "СписокОтправокПоОтчетуРПН";
			КонецЕсли;
			
			Если ИмяФормыИсторииОтправок <> "" Тогда
				
				УсловияОтбора = Новый Структура("ОтчетСсылка", Ссылка);
				ПараметрыФормы = Новый Структура("Отбор", УсловияОтбора);
				ОткрытьФорму(КонтекстЭДОКлиент.ПутьКОбъекту + ".Форма." + ИмяФормыИсторииОтправок, ПараметрыФормы);

			КонецЕсли;
			
		КонецЕсли;
	
	Исключение
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'При открытии истории отправок возникла ошибка'"));
		
	КонецПопытки; 
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьИсториюОтправки()

	// Открываем форму цикла обмена
	ОткрытьФорму(КонтекстЭДОКлиент.ПутьКОбъекту + ".Форма.УправлениеОбменом");
	
	ИмяСобытия = ИмяСобытияОткрытияИсторииОтправки();
	Если НЕ ЗначениеЗаполнено(ИмяСобытия) Тогда
		// Если это не регламентированный отчет, то имя события определяется в переопределяемой функции
		ИмяСобытия = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентПереопределяемый.ИмяСобытияОткрытияИсторииОтправки(Ссылка);
	КонецЕсли;
	
	Оповестить(ИмяСобытия, Ссылка);
	
КонецПроцедуры

&НаСервере
Функция ИмяСобытияОткрытияИсторииОтправки()
	
	ИмяСобытия = "";
	
	Если ТипЗнч(Ссылка) = Тип("СправочникСсылка.ЭлектронныеПредставленияРегламентированныхОтчетов") Тогда
		
		Если Ссылка.ТипПолучателя = Перечисления.ТипыКонтролирующихОрганов.ПФР Тогда
		    ИмяСобытия = "Показать циклы обмена отчета ПФР";
		ИначеЕсли Ссылка.ВидОтчета.ТипДокумента = Перечисления.ТипыОтправляемыхДокументов.ЗаявлениеОВвозеТоваров Тогда
			ИмяСобытия = "Показать циклы обмена заявления";
		ИначеЕсли Ссылка.ТипПолучателя = Перечисления.ТипыКонтролирующихОрганов.ФСГС Тогда
			ИмяСобытия = "Показать циклы обмена отчета статистики";
		Иначе
			Если Ссылка.ВидОтчета = Справочники.ВидыОтправляемыхДокументов.УведомлениеОКонтролируемыхСделках
				ИЛИ Ссылка.ВидОтчета.ПринадлежитЭлементу(Справочники.ВидыОтправляемыхДокументов.Уведомления) Тогда
				ИмяСобытия = "Показать журнал отправки электронного представления уведомления";
			Иначе
				ИмяСобытия = "Показать циклы обмена";
			КонецЕсли;
		КонецЕсли;
		
	ИначеЕсли ТипЗнч(Ссылка) = Тип("ДокументСсылка.РегламентированныйОтчет") Тогда
		
		// Для регламентированных отчетов из БРО имя события жестко определены
		Если ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФНС") Тогда
			
			Если ТипЗнч(Ссылка) = Тип("ДокументСсылка.РегламентированныйОтчет") Тогда
				// Определяем имя события для регламентированных отчетов
				ИсточникОтчета = Ссылка.ИсточникОтчета;
				Если ИсточникОтчета = "РегламентированныйОтчетЗаявлениеОВвозеТоваров" Тогда
					ИмяСобытия = "Показать циклы обмена заявления";
				ИначеЕсли ИсточникОтчета = "РегламентированныйОтчетУведомлениеОКонтролируемыхСделках" Тогда
					ИмяСобытия = "Показать циклы обмена уведомления";
				Иначе
					ИмяСобытия = "Показать циклы обмена";
				КонецЕсли;
			КонецЕсли;
				
			
		ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ПФР") Тогда
			
			ИмяСобытия = "Показать циклы обмена отчета ПФР";
			
		ИначеЕсли ВидКонтролирующегоОргана = ПредопределенноеЗначение("Перечисление.ТипыКонтролирующихОрганов.ФСГС") Тогда

			ИмяСобытия = "Показать циклы обмена отчета статистики";
			
		КонецЕсли;
		
	ИначеЕсли ТипЗнч(Ссылка) = Тип("ДокументСсылка.УведомлениеОСпецрежимахНалогообложения") Тогда
				
		ИмяСобытия = "Показать циклы обмена уведомления";
			
	КонецЕсли;
	
	Возврат ИмяСобытия;

КонецФункции


#КонецОбласти
