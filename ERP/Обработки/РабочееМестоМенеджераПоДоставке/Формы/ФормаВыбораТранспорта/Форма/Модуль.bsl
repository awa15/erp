﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	КоэффициентПересчетаВТонны     			 = Константы.КоэффициентПересчетаВТонны.Получить();	
	КоэффициентПересчетаВКубическиеМетры     = Константы.КоэффициентПересчетаВКубическиеМетры.Получить();
	
	Если КоэффициентПересчетаВТонны = 0 Тогда
		КоэффициентПересчетаВТонны = 1;
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru='Не задан коэффициент пересчета в тонны. Обратитесь к администратору.';"));
	КонецЕсли;
	
	Если КоэффициентПересчетаВКубическиеМетры = 0 Тогда	
		КоэффициентПересчетаВКубическиеМетры = 1;
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru='Не задан коэффициент пересчета в кубические метры. Обратитесь к администратору.';"));
	КонецЕсли;
	
	ДатаОтбор        = Параметры.ДатаОтбор;
	Склад	         = Параметры.Склад;
	ТребуемыйВес     = Параметры.ИтогоВес * КоэффициентПересчетаВТонны;
	ТребуемыйОбъем   = Параметры.ИтогоОбъем * КоэффициентПересчетаВКубическиеМетры;
	ОсталосьВес      = ТребуемыйВес;
	ОсталосьОбъем    = ТребуемыйОбъем;
	НабралиВесПроц   = 0;
	НабралиОбъемПроц = 0;
	Если НЕ ЗначениеЗаполнено(ДатаОтбор) Тогда
		ДатаОтбор = ТекущаяДата();
	КонецЕсли;
	ЗаданияФормируемые.Загрузить(ПолучитьИзВременногоХранилища(Параметры.АдресЗаданийФормируемых));
	ОбновитьСписокТранспорта();
	ДоставкаТоваровКлиентСервер.ЗаполнитьСписокВыбораПоляВремени(Элементы.ВыбранныеТранспортныеСредстваВремяС);
	ДоставкаТоваровКлиентСервер.ЗаполнитьСписокВыбораПоляВремени(Элементы.ВыбранныеТранспортныеСредстваВремяПо);
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	Если Не ПропуститьВопросПередЗакрытием
		И НЕ ПеренестиВДоставку
		И ВыбранныеТранспортныеСредства.Количество() > 0 Тогда
		Отказ = Истина;
		Кнопки = Новый СписокЗначений;
		Кнопки.Добавить(КодВозвратаДиалога.Да, НСтр("ru = 'Перенести и закрыть'"));
		Кнопки.Добавить(КодВозвратаДиалога.Нет, НСтр("ru = 'Не переносить и закрыть'"));
		Кнопки.Добавить(КодВозвратаДиалога.Отмена);
		ПоказатьВопрос(Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект),
			НСтр("ru = 'Выбранные транспортные средства не перенесены в список формируемых заданий. Перенести?'"), Кнопки);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	ОтветНаВопрос = РезультатВопроса;
	Если ОтветНаВопрос = КодВозвратаДиалога.Нет Тогда
		ПропуститьВопросПередЗакрытием = Истина;
		Закрыть();
	ИначеЕсли ОтветНаВопрос = КодВозвратаДиалога.Да Тогда
		ПеренестиВДоставку = Истина;
		ПропуститьВопросПередЗакрытием = Истина;
		Закрыть();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	Если ПеренестиВДоставку Тогда
		АдресТранспортВХранилище = ПоместитьТранспортВХранилище();
		Структура = Новый Структура("АдресТранспортВХранилище", АдресТранспортВХранилище);
		ОповеститьОВыборе(Структура);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

&НаКлиенте
Процедура ДатаОтборПриИзменении(Элемент)
	ПодключитьОбработчикОжидания("ДатаОтборПриИзмененииОбработчикОжидания", 0.1, Истина);
КонецПроцедуры

&НаКлиенте
Процедура ДатаОтборРегулирование(Элемент, Направление, СтандартнаяОбработка)
	Если НЕ ЗначениеЗаполнено(ДатаОтбор) Тогда
		СтандартнаяОбработка = Ложь;
		ДатаОтбор = ТекущаяДата();
		ПодключитьОбработчикОжидания("ДатаОтборПриИзмененииОбработчикОжидания", 0.3, Истина);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ТранспортныеСредстваВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ПеренестиСтроки(Элементы.ТранспортныеСредства.ВыделенныеСтроки)
КонецПроцедуры

&НаКлиенте
Процедура ТранспортныеСредстваНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	НачалиПеретаскивание = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ТранспортныеСредстваОкончаниеПеретаскивания(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка)
	НачалиПеретаскивание = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ТипыТранспортныхСредствВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ПеренестиСтроки(Элементы.ТипыТранспортныхСредств.ВыделенныеСтроки, Ложь)
КонецПроцедуры

&НаКлиенте
Процедура ТипыТранспортныхСредствНачалоПеретаскивания(Элемент, ПараметрыПеретаскивания, Выполнение)
	НачалиПеретаскивание = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ТипыТранспортныхСредствОкончаниеПеретаскивания(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка)
	НачалиПеретаскивание = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ВыбранныеТранспортныеСредстваПроверкаПеретаскивания(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка, Строка, Поле)
	Если НЕ НачалиПеретаскивание Тогда
		ПараметрыПеретаскивания.Действие = ДействиеПеретаскивания.Отмена;
	Иначе
		ПараметрыПеретаскивания.Действие = ДействиеПеретаскивания.Перемещение;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВыбранныеТранспортныеСредстваПеретаскивание(Элемент, ПараметрыПеретаскивания, СтандартнаяОбработка, Строка, Поле)
	Если ТипЗнч(ПараметрыПеретаскивания.Значение[0]) =Тип("СправочникСсылка.ТипыТранспортныхСредств") Тогда
		ПеренестиСтроки(Элементы.ТипыТранспортныхСредств.ВыделенныеСтроки, Ложь)
	ИначеЕсли ПараметрыПеретаскивания.Значение[0].Свойство("ТранспортноеСредство") Тогда
		ПеренестиСтроки(Элементы.ТранспортныеСредства.ВыделенныеСтроки)
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВыбранныеТранспортныеСредстваПередУдалением(Элемент, Отказ)
	Отказ = Истина;
	УдалитьСтрокиВыбранныхТранспортныхСредств(Элементы.ВыбранныеТранспортныеСредства.ВыделенныеСтроки);
КонецПроцедуры

&НаКлиенте
Процедура ВыбранныеТранспортныеСредстваПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	Отказ = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ВыбранныеТранспортныеСредстваДатаВремяПриИзменении(Элемент)
	
	ПроверитьСкорректироватьОтсортироватьПоВремени(Элемент.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура ТранспортныеСредстваПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	ПараметрыФормы = Новый Структура;
	Если Копирование Тогда
		ПараметрыФормы.Вставить("ЗначениеКопирования", Элементы.ТранспортныеСредства.ТекущиеДанные.ТранспортноеСредство);
	КонецЕсли;
	ОбновитьСписокТранспорта();
	Отказ = Истина;
	ОткрытьФорму("Справочник.ТранспортныеСредства.ФормаОбъекта",ПараметрыФормы,,,,, Неопределено, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
КонецПроцедуры

&НаКлиенте
Процедура ТранспортныеСредстваИзменить(Команда)
	ТекущиеДанные = Элементы.ТранспортныеСредства.ТекущиеДанные;
	Если ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;
	ПараметрыФормы = Новый Структура("Ключ", ТекущиеДанные.ТранспортноеСредство);
	ОткрытьФорму("Справочник.ТранспортныеСредства.ФормаОбъекта",ПараметрыФормы,,,,, Новый ОписаниеОповещения("ТранспортныеСредстваИзменитьЗавершение", ЭтотОбъект, Новый Структура("ТекущиеДанные", ТекущиеДанные)), РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
КонецПроцедуры

&НаКлиенте
Процедура ТранспортныеСредстваИзменитьЗавершение(Результат, ДополнительныеПараметры) Экспорт
    
    ТекущиеДанные = ДополнительныеПараметры.ТекущиеДанные;
    
    
    ОбновитьСпозиционироватьСписокТранспортаСервер(ТекущиеДанные.ТранспортноеСредство);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Выбрать(Команда)
	Если Элементы.СтраницыТранспортныеСредстваТипы.ТекущаяСтраница = Элементы.СтраницыТранспортныеСредстваТипы.ПодчиненныеЭлементы.СтраницаТранспортныеСредства Тогда
		ПеренестиСтроки(Элементы.ТранспортныеСредства.ВыделенныеСтроки)
	Иначе 
		ПеренестиСтроки(Элементы.ТипыТранспортныхСредств.ВыделенныеСтроки, Ложь)
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура Убрать(Команда)
	Если Элементы.ВыбранныеТранспортныеСредства.ВыделенныеСтроки.Количество() > 0 Тогда
		УдалитьСтрокиВыбранныхТранспортныхСредств(Элементы.ВыбранныеТранспортныеСредства.ВыделенныеСтроки)
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПеренестиВыбранныеВДоставку(Команда)
	ПеренестиВДоставку = (ВыбранныеТранспортныеСредства.Количество() > 0);
	Закрыть(КодВозвратаДиалога.OK);
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТранспортныеСредстваНаименование.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ТранспортныеСредства.НормЧастота");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Больше;
	ОтборЭлемента.ПравоеЗначение = 0.7;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ЦветИтоговыхПоказателейДокументов);
	Элемент.Оформление.УстановитьЗначениеПараметра("Шрифт", Новый Шрифт(WindowsШрифты.DefaultGUIFont, , , Истина, Ложь, Ложь, Ложь, ));

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТранспортныеСредстваЗаданияНаПеревозку.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ТранспортныеСредства.ЗаданияНаПеревозку");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = НСтр("ru = '<нет заданий на перевозку>'");

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ЦветТекстаОтмененнойСтрокиДокумента);

КонецПроцедуры

&НаСервере
Процедура УстановитьЗаголовокТранспортныеСредстваЗаданияНаПеревозку()
	Если НЕ ЗначениеЗаполнено(ДатаОтбор) Тогда
		Элементы.ТранспортныеСредстваЗаданияНаПеревозку.Видимость = Ложь;
	Иначе
		Элементы.ТранспортныеСредстваЗаданияНаПеревозку.Видимость = Истина;
		Элементы.ТранспортныеСредстваЗаданияНаПеревозку.Заголовок = НСтр("ru = 'Задания на перевозку на ';") + Формат(ДатаОтбор, "ДФ=""д МММ""");
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ОбновитьСписокТранспорта();
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ЗаданиеНаПеревозку.ТранспортноеСредство КАК ТранспортноеСредство,
	|	СУММА(1) КАК Частота
	|ПОМЕСТИТЬ ВТЧастотаИспользованияТранспорта
	|ИЗ
	|	Документ.ЗаданиеНаПеревозку КАК ЗаданиеНаПеревозку
	|ГДЕ
	|	ЗаданиеНаПеревозку.Проведен
	|	И ЗаданиеНаПеревозку.Дата >= &ДатаДляСтатистики
	|	И НЕ ЗаданиеНаПеревозку.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЗаданийНаПеревозку.Формируется)
	|
	|СГРУППИРОВАТЬ ПО
	|	ЗаданиеНаПеревозку.ТранспортноеСредство
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТранспортныеСредства.Наименование,
	|	ТранспортныеСредства.Тип КАК Тип,
	|	ТранспортныеСредства.Ссылка КАК ТранспортноеСредство,
	|	ВЫБОР
	|		КОГДА влЧастотаИспользованияТранспорта.Частота ЕСТЬ NULL 
	|			ТОГДА 0
	|		КОГДА ИтогиЧастотаИспользованияТранспорта.МаксЧастота = 0
	|			ТОГДА 0
	|		ИНАЧЕ влЧастотаИспользованияТранспорта.Частота / ИтогиЧастотаИспользованияТранспорта.МаксЧастота
	|	КОНЕЦ КАК НормЧастота,
	|	&СтрокаНетЗаданий КАК ЗаданияНаПеревозку,
	|	ТранспортныеСредства.ГрузоподъемностьВТоннах,
	|	ТранспортныеСредства.ВместимостьВКубическихМетрах,
	|	ТранспортныеСредства.Марка КАК Марка,
	|	ЛОЖЬ КАК Выбран,
	|	0 КАК ВыбранСчетчик
	|ИЗ
	|	Справочник.ТранспортныеСредства КАК ТранспортныеСредства
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТЧастотаИспользованияТранспорта КАК влЧастотаИспользованияТранспорта
	|		ПО (влЧастотаИспользованияТранспорта.ТранспортноеСредство = ТранспортныеСредства.Ссылка)
	|		ЛЕВОЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
	|			МАКСИМУМ(ВТЧастотаИспользованияТранспорта.Частота) КАК МаксЧастота
	|		ИЗ
	|			ВТЧастотаИспользованияТранспорта КАК ВТЧастотаИспользованияТранспорта) КАК ИтогиЧастотаИспользованияТранспорта
	|		ПО (ИСТИНА)
	|ГДЕ
	|	НЕ ТранспортныеСредства.ПометкаУдаления
	|	И ТранспортныеСредства.УдалитьИспользоватьВДоставке";
	Запрос.УстановитьПараметр("ДатаДляСтатистики",ДобавитьМесяц(ТекущаяДата(), -2));
	Запрос.УстановитьПараметр("СтрокаНетЗаданий", НСтр("ru = '<нет заданий на перевозку>'"));
	Результат = Запрос.Выполнить();
	Если Результат.Пустой() Тогда
		Возврат
	КонецЕсли;
	
	ТаблицаТранспорта = Новый ТаблицаЗначений;
	ТаблицаТранспорта = Результат.Выгрузить();
	ЗначениеВДанныеФормы(ТаблицаТранспорта, ТранспортныеСредства);
	ЗаполнитьЗаданияНаПеревозку();
	УстановитьЗаголовокТранспортныеСредстваЗаданияНаПеревозку()
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьЗаданияНаПеревозку()
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ЗаданияНаПеревозкуФормируемые.ТранспортноеСредство,
		|	ЗаданияНаПеревозкуФормируемые.Ссылка,
		|	ЗаданияНаПеревозкуФормируемые.Статус,
		|	ЗаданияНаПеревозкуФормируемые.ВремяС,
		|	ЗаданияНаПеревозкуФормируемые.ВремяПо
		|ПОМЕСТИТЬ ВТЗаданияНаПеревозкуФормируемые
		|ИЗ
		|	&ЗаданияНаПеревозкуФормируемые КАК ЗаданияНаПеревозкуФормируемые
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ЕСТЬNULL(ВТЗаданияНаПеревозкуФормируемые.ТранспортноеСредство, ЗаданиеНаПеревозку.ТранспортноеСредство) КАК ТранспортноеСредство,
		|	ЕСТЬNULL(ВТЗаданияНаПеревозкуФормируемые.Статус, ЗаданиеНаПеревозку.Статус) КАК Статус,
		|	ЕСТЬNULL(ВТЗаданияНаПеревозкуФормируемые.ВремяС, ВЫБОР
		|			КОГДА ЗаданиеНаПеревозку.ДатаВремяРейсаФактС = ДАТАВРЕМЯ(1, 1, 1)
		|				ТОГДА ЗаданиеНаПеревозку.ДатаВремяРейсаПланС
		|			ИНАЧЕ ЗаданиеНаПеревозку.ДатаВремяРейсаФактС
		|		КОНЕЦ) КАК ВремяС,
		|	ЕСТЬNULL(ВТЗаданияНаПеревозкуФормируемые.ВремяПо, ВЫБОР
		|			КОГДА ЗаданиеНаПеревозку.ДатаВремяРейсаФактПо = ДАТАВРЕМЯ(1, 1, 1)
		|				ТОГДА ЗаданиеНаПеревозку.ДатаВремяРейсаПланПо
		|			ИНАЧЕ ЗаданиеНаПеревозку.ДатаВремяРейсаФактПо
		|		КОНЕЦ) КАК ВремяПо
		|ИЗ
		|	ВТЗаданияНаПеревозкуФормируемые КАК ВТЗаданияНаПеревозкуФормируемые
		|		ПОЛНОЕ СОЕДИНЕНИЕ Документ.ЗаданиеНаПеревозку КАК ЗаданиеНаПеревозку
		|		ПО ВТЗаданияНаПеревозкуФормируемые.Ссылка = ЗаданиеНаПеревозку.Ссылка
		|ГДЕ
		|	ЗаданиеНаПеревозку.Проведен
		|	И НАЧАЛОПЕРИОДА(ВЫБОР
		|				КОГДА ЗаданиеНаПеревозку.ДатаВремяРейсаФактС = ДАТАВРЕМЯ(1, 1, 1)
		|					ТОГДА ЗаданиеНаПеревозку.ДатаВремяРейсаПланС
		|				ИНАЧЕ ЗаданиеНаПеревозку.ДатаВремяРейсаФактС
		|			КОНЕЦ, ДЕНЬ) = &ДатаОтбор
		|	И НЕ ЗаданиеНаПеревозку.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЗаданийНаПеревозку.Закрыт)
		|
		|УПОРЯДОЧИТЬ ПО
		|	ВремяС,
		|	ВремяПо";
	
	Запрос.УстановитьПараметр("ДатаОтбор",ДатаОтбор);
	Запрос.УстановитьПараметр("ЗаданияНаПеревозкуФормируемые",ЗаданияФормируемые.Выгрузить());
	Результат = Запрос.Выполнить();
	Если Результат.Пустой() Тогда
		Возврат
	КонецЕсли;
	
	ТаблицаЗаданийНаПеревозку = Новый ТаблицаЗначений;
	ТаблицаЗаданийНаПеревозку = Результат.Выгрузить();
	
	Для Каждого Стр из ТранспортныеСредства Цикл
		МассивЗаданийНаПеревозку = ТаблицаЗаданийНаПеревозку.НайтиСтроки(Новый Структура("ТранспортноеСредство", Стр.ТранспортноеСредство));
		СтрЗаданияНаПеревозку = "";
		Для Каждого ВлСтр из МассивЗаданийНаПеревозку Цикл
			Если НачалоДня(ВлСтр.ВремяС) = НачалоДня(ВлСтр.ВремяПо) Тогда
				СтрЗаданияНаПеревозку = СтрЗаданияНаПеревозку + ?(СтрЗаданияНаПеревозку = "","",Символы.ПС) + НСтр("ru='С';")
										+ Символы.НПП + Формат(ВлСтр.ВремяС, "ДФ=""ЧЧ:мм""") + Символы.НПП + НСтр("ru='до';")
										+ Символы.НПП + Формат(ВлСтр.ВремяПо, "ДФ=""ЧЧ:мм""") + " (" + ВлСтр.Статус + ")"
			Иначе
				СтрЗаданияНаПеревозку = СтрЗаданияНаПеревозку + ?(СтрЗаданияНаПеревозку = "","",Символы.ПС) + НСтр("ru='С';")
										+ Символы.НПП + Формат(ВлСтр.ВремяС, "ДФ=""ЧЧ:мм""") + Символы.НПП + НСтр("ru='весь день (';") + ВлСтр.Статус + ")"
			КонецЕсли;
		КонецЦикла;
		Если СтрЗаданияНаПеревозку <> "" Тогда
			Стр.ЗаданияНаПеревозку = СтрЗаданияНаПеревозку;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура ДатаОтборПриИзмененииОбработчикОжидания()
	ОбновитьСписокТранспорта();	
КонецПроцедуры

&НаСервере
Процедура ПеренестиСтроки(Знач МассивСтрок, ЭтоТранспортныеСредства = Истина)
	Если МассивСтрок.Количество() = 0 тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru='Не выбран транспорт';"));
		Возврат;
	КонецЕсли;
	Для Каждого Элемент из МассивСтрок Цикл
		НоваяСтр = ВыбранныеТранспортныеСредства.Добавить();
		Если ЭтоТранспортныеСредства Тогда
			СтрокаТранспорт = ТранспортныеСредства.НайтиПоИдентификатору(Элемент);
			СтрокаТранспорт.Выбран = Истина;
			СтрокаТранспорт.ВыбранСчетчик = СтрокаТранспорт.ВыбранСчетчик + 1;
			ЗаполнитьЗначенияСвойств(НоваяСтр, СтрокаТранспорт);
		Иначе
			СтруктураРеквизитов = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Элемент,"Наименование, ГрузоподъемностьВТоннах, ВместимостьВКубическихМетрах, Описание");
			ЗаполнитьЗначенияСвойств(НоваяСтр, СтруктураРеквизитов);
			НоваяСтр.ТранспортноеСредство = Элемент;
		КонецЕсли;
		НоваяСтр.ВремяС = НачалоДня(ДатаОтбор);
		НоваяСтр.ВремяПо = НачалоДня(ДатаОтбор);
		НоваяСтр.Склад = Склад;
	КонецЦикла;
	Если ТребуемыйВес <> 0 Тогда
		ОсталосьВес = ТребуемыйВес - ВыбранныеТранспортныеСредства.Итог("ГрузоподъемностьВТоннах");
		НабралиВесПроц = 100 * (ТребуемыйВес - ОсталосьВес) / ТребуемыйВес;
	КонецЕсли;
	Если ТребуемыйОбъем <> 0 Тогда
		ОсталосьОбъем = ТребуемыйОбъем - ВыбранныеТранспортныеСредства.Итог("ВместимостьВКубическихМетрах");
		НабралиОбъемПроц = 100 * (ТребуемыйОбъем - ОсталосьОбъем) / ТребуемыйОбъем;
	КонецЕсли;
	ВыбранныеТранспортныеСредства.Сортировать("ВремяС, ВремяПо");
	
КонецПроцедуры

&НаСервере
Процедура УдалитьСтрокиВыбранныхТранспортныхСредств(Знач МассивСтрок)
	Для Каждого ИД из МассивСтрок Цикл
		СтрокаТранспорт = ВыбранныеТранспортныеСредства.НайтиПоИдентификатору(ИД);
		Если ТребуемыйВес <> 0 Тогда
			ОсталосьВес = ОсталосьВес + СтрокаТранспорт.ГрузоподъемностьВТоннах;
			НабралиВесПроц = 100 * (ТребуемыйВес - ОсталосьВес) / ТребуемыйВес;
		КонецЕсли;
		Если ТребуемыйОбъем <> 0 Тогда
			ОсталосьОбъем = ОсталосьОбъем + СтрокаТранспорт.ВместимостьВКубическихМетрах;
			НабралиОбъемПроц = 100 * (ТребуемыйОбъем - ОсталосьОбъем) / ТребуемыйОбъем;
		КонецЕсли;
		ВыбранныеТранспортныеСредства.Удалить(СтрокаТранспорт);
		Если ТипЗнч(СтрокаТранспорт.ТранспортноеСредство) = Тип("СправочникСсылка.ТранспортныеСредства") Тогда
			МассивСтрок = ТранспортныеСредства.НайтиСтроки(Новый Структура("ТранспортноеСредство",
																			СтрокаТранспорт.ТранспортноеСредство));
			Если МассивСтрок.Количество() > 0 Тогда
				Если МассивСтрок[0].ВыбранСчетчик < 2 Тогда
					МассивСтрок[0].ВыбранСчетчик = 0;
					МассивСтрок[0].Выбран = Ложь;
				Иначе
					МассивСтрок[0].ВыбранСчетчик = МассивСтрок[0].ВыбранСчетчик - 1;
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

&НаСервере
Функция ПоместитьТранспортВХранилище()
	АдресТранспортВХранилище = ПоместитьВоВременноеХранилище(ВыбранныеТранспортныеСредства.Выгрузить(), УникальныйИдентификатор);
	Возврат АдресТранспортВХранилище;
КонецФункции

&НаКлиенте
Процедура ПроверитьСкорректироватьОтсортироватьПоВремени(ИмяЭлемента)
	
	ТекДанные = Элементы.ВыбранныеТранспортныеСредства.ТекущиеДанные;
	ТекДанные.ВремяС = НачалоДня(ТекДанные.ВремяС)
				+ Час(ТекДанные.ВремяСБезДаты)*60*60 + Минута(ТекДанные.ВремяСБезДаты)*60;
	ТекДанные.ВремяПо = НачалоДня(ТекДанные.ВремяПо)
				+ Час(ТекДанные.ВремяПоБезДаты)*60*60 + Минута(ТекДанные.ВремяПоБезДаты)*60;
	Если ТекДанные.ВремяПо < ТекДанные.ВремяС Тогда
		Если Найти(ИмяЭлемента,"По") = 0 Тогда
			ТекДанные.ВремяПо = ТекДанные.ВремяС;
			ТекДанные.ВремяПоБезДаты = ТекДанные.ВремяСБезДаты;
		Иначе
			ТекДанные.ВремяС = ТекДанные.ВремяПо;
			ТекДанные.ВремяCБезДаты = ТекДанные.ВремяПоБезДаты;
		КонецЕсли;
	КонецЕсли;
	ТекущаяСтрока = ВыбранныеТранспортныеСредства.НайтиПоИдентификатору(Элементы.ВыбранныеТранспортныеСредства.ТекущаяСтрока);
	ТекущийИндекс = ВыбранныеТранспортныеСредства.Индекс(ТекДанные);
	
	Если (ТекущийИндекс>0 И (ВыбранныеТранспортныеСредства[ТекущийИндекс-1].ВремяС > ТекущаяСтрока.ВремяС
							 ИЛИ (ВыбранныеТранспортныеСредства[ТекущийИндекс-1].ВремяС = ТекущаяСтрока.ВремяС
							 	  И ВыбранныеТранспортныеСредства[ТекущийИндекс-1].ВремяПо > ТекущаяСтрока.ВремяПо)))
		  ИЛИ (ТекущийИндекс < ВыбранныеТранспортныеСредства.Количество()-1
		  	   И (ВыбранныеТранспортныеСредства[ТекущийИндекс+1].ВремяС < ТекущаяСтрока.ВремяС
							 ИЛИ (ВыбранныеТранспортныеСредства[ТекущийИндекс+1].ВремяС = ТекущаяСтрока.ВремяС
							 	  И ВыбранныеТранспортныеСредства[ТекущийИндекс+1].ВремяПо < ТекущаяСтрока.ВремяПо))) Тогда
		ВыбранныеТранспортныеСредства.Сортировать("ВремяС, ВремяПо");
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьСпозиционироватьСписокТранспортаСервер(ТранспортСсылка);
	ОбновитьСписокТранспорта();
	МассивНайденных = ТранспортныеСредства.НайтиСтроки(Новый Структура("ТранспортноеСредство",ТранспортСсылка));
	Если МассивНайденных.Количество() > 0 Тогда
		Элементы.ТранспортныеСредства.ТекущаяСтрока = МассивНайденных[0].ПолучитьИдентификатор();
	КонецЕсли;
КонецПроцедуры

#КонецОбласти
