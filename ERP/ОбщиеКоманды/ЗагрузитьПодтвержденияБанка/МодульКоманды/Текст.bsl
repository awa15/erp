﻿#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ПараметрыЗагрузки = ОбменСБанкамиПоЗарплатнымПроектамКлиент.ПараметрыЗагрузкиФайловИзБанка();
	ПараметрыЗагрузки.ОповещениеЗавершения = Новый ОписаниеОповещения("ОбработкаКомандыЗавершение", ЭтотОбъект);
	
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ЗагрузитьФайлыИзБанка(ПараметрыЗагрузки);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Функция ЗагрузитьПодтвержденияИзБанкаНаСервере(МассивИменФайлов)
	
	Возврат ОбменСБанкамиПоЗарплатнымПроектам.ЗагрузитьПодтвержденияИзБанка(МассивИменФайлов);
	
КонецФункции

&НаКлиенте
Процедура ОбработкаКомандыЗавершение(Результат, ДополнительныеПараметры) Экспорт 
	
	ПомещенныеФайлы = Результат.ПомещенныеФайлы;
	
	Если ПомещенныеФайлы.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	СозданныеДокументы = ЗагрузитьПодтвержденияИзБанкаНаСервере(ПомещенныеФайлы);
	
	Если СозданныеДокументы.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
		
	СоответствиеТипов = Новый Соответствие;
	ПриЗагрузкеБылиОшибки = Ложь;
	Для каждого СозданныйДокумент Из СозданныеДокументы Цикл
		
		Если СозданныйДокумент.Ключ = Неопределено Тогда
			ПриЗагрузкеБылиОшибки = Истина;
			Продолжить;
		КонецЕсли;
		
		СоответствиеТипов.Вставить(ТипЗнч(СозданныйДокумент.Ключ));
		СоответствиеТипов.Вставить(СозданныйДокумент.Значение);
		
	КонецЦикла;
	
	Для каждого ТипДокумента Из СоответствиеТипов Цикл
		ОповеститьОбИзменении(ТипДокумента.Ключ);
		Если ТипДокумента.Ключ = Тип("ДокументСсылка.ПодтверждениеОткрытияЛицевыхСчетовСотрудников") Тогда
			Оповестить("ЗагруженоПодтверждениеОткрытияЛицевыхСчетов");
		ИначеЕсли ТипДокумента.Ключ = Тип("ДокументСсылка.ПодтверждениеЗачисленияЗарплаты") Тогда
			Оповестить("ЗагруженоПодтверждениеЗачисленияЗарплаты");
		КонецЕсли;
	КонецЦикла;
	
	Если ПриЗагрузкеБылиОшибки Тогда
		Состояние(НСтр("ru = 'При загрузке файлов подтверждений банка были ошибки'"));
	Иначе
		Состояние(НСтр("ru = 'Все файлы успешно загружены'"));
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
