﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	
#Область ОбработчикиСобытий

Процедура ПриЗаписи(Отказ)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Константы.ИспользуетсяСборкаРазборкаИСерииНоменклатуры.Установить(
			Константы.ИспользоватьСборкуРазборку.Получить() 
			И Константы.ИспользоватьСерииНоменклатуры.Получить());
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли