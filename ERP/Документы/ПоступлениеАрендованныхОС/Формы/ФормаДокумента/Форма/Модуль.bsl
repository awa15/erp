﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("Автотест") Тогда
		Возврат;
	КонецЕсли;
	
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ИсточникВыбора.ИмяФормы = "Справочник.ОбъектыЭксплуатации.Форма.ФормаВыбора" Тогда
		Если ВыбранноеЗначение.Количество() > 0 Тогда
			МассивИдентификаторыСтрок = Новый Массив;
			Для Каждого ЭлементМассива Из ВыбранноеЗначение Цикл
				НоваяСтрока = Объект.ОС.Добавить();
				НоваяСтрока.ОсновноеСредство = ЭлементМассива;
				МассивИдентификаторыСтрок.Добавить(НоваяСтрока.ПолучитьИдентификатор());
			КонецЦикла;
			ЗаполнитьИнвентарныеНомера(МассивИдентификаторыСтрок);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТабличнойЧастиОсновныеСредства

&НаКлиенте
Процедура ОСОсновноеСредствоПриИзменении(Элемент)
	
	ЭлементКоллекции = Элементы.ОС.ТекущиеДанные;
	Если ЗначениеЗаполнено(ЭлементКоллекции.ОсновноеСредство) Тогда
		ЭлементКоллекции.ИнвентарныйНомер = ОбщегоНазначенияУТВызовСервера.ЗначениеРеквизитаОбъекта(ЭлементКоллекции.ОсновноеСредство, "Код");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подбор(Команда)
	
	ПараметрыОтбор = Новый Структура;
	ПараметрыОтбор.Вставить("БУСостояние", ПредопределенноеЗначение("Перечисление.СостоянияОС.НеПринятоКУчету"));
	ПараметрыОтбор.Вставить("БУОрганизация", ПредопределенноеЗначение("Справочник.Организации.ПустаяСсылка"));
	ПараметрыОтбор.Вставить("БУПодразделение", ПредопределенноеЗначение("Справочник.СтруктураПредприятия.ПустаяСсылка"));
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Контекст", "БУ, МФУ");
	ПараметрыФормы.Вставить("ДатаСведений", Объект.Дата);
	ПараметрыФормы.Вставить("ТекущийРегистратор", Объект.Ссылка);
	ПараметрыФормы.Вставить("Отбор", ПараметрыОтбор);
	ПараметрыФормы.Вставить("ЗакрыватьПриВыборе", Ложь);
	
	ОткрытьФорму("Справочник.ОбъектыЭксплуатации.ФормаВыбора", ПараметрыФормы, ЭтаФорма);
	
КонецПроцедуры

#Область ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры

#КонецОбласти

#Область СтандартныеПодсистемы_Печать

&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры

#КонецОбласти

#Область СтандартныеПодсистемы_ДополнительныеОтчетыИОбработки

&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ЗаполнитьИнвентарныеНомера(МассивИдентификаторыСтроки)
	
	МассивСтрок = Новый Массив;
	Для Каждого ИдентификаторСтроки ИЗ МассивИдентификаторыСтроки Цикл
		МассивСтрок.Добавить(Объект.ОС.НайтиПоИдентификатору(ИдентификаторСтроки));
	КонецЦикла;
	СоответствиеКодов = ОбщегоНазначения.ЗначенияРеквизитовОбъектов(
		Объект.ОС.Выгрузить(МассивСтрок, "ОсновноеСредство").ВыгрузитьКолонку("ОсновноеСредство"),
		"Код");
	
	Для Каждого Строка Из МассивСтрок Цикл
		Строка.ИнвентарныйНомер = СоответствиеКодов.Получить(Строка.ОсновноеСредство).Код;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти
