﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ИспользуетсяРасчетЗарплаты = ПолучитьФункциональнуюОпцию("ИспользоватьРасчетЗарплатыРасширенная");
	
	Если Параметры.Ключ.Пустая() Тогда
		
		// Создается новый документ.
		ЗначенияДляЗаполнения = Новый Структура("Организация, Месяц, Ответственный", "Объект.Организация", "Объект.ПериодРегистрации", "Объект.Ответственный");
		ЗарплатаКадры.ЗаполнитьПервоначальныеЗначенияВФорме(ЭтаФорма, ЗначенияДляЗаполнения);
		
		ЗаполнитьДанныеФормыПоОрганизации();
		ПриПолученииДанныхНаСервере();
		
	КонецЕсли;
	
	// Обработчик подсистемы "Дополнительные отчеты и обработки".
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Обработчик подсистемы "ВерсионированиеОбъектов".
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Обработчик подсистемы "Печать".
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
	ПриПолученииДанныхНаСервере();
	
	ЗаполнитьФормуПоДаннымОбъекта(ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	УникальныеЗначения = Новый Соответствие;
	
	ИндексСтроки = 0;
	
	Для каждого ТекСтрока Из Сотрудники Цикл
		Если УникальныеЗначения[ТекСтрока.Сотрудник] = Неопределено Тогда
			УникальныеЗначения.Вставить(ТекСтрока.Сотрудник, Истина);
		Иначе
			ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Информация о сотруднике %1 была введена в документе ранее.'"), ТекСтрока.Сотрудник);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки, Объект.Ссылка, "Сотрудники[" + Формат(ИндексСтроки, "ЧН=0; ЧГ=0") + "].Сотрудник", ,Отказ);
		КонецЕсли;
		ИндексСтроки = ИндексСтроки + 1;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	ЗаполнитьОбъектПоДаннымФормы(ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	ЗаполнитьФормуПоДаннымОбъекта(ТекущийОбъект);
	
	УстановитьДоступностьЭлементов();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура МесяцНачисленияСтрокойПриИзменении(Элемент)
	
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(ЭтаФорма, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой", Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтаФорма, ЭтаФорма, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой");
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойРегулирование(Элемент, Направление, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(ЭтаФорма, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой", Направление, Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура МесяцНачисленияСтрокойОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	ОрганизацияПриИзмененииНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ВремяУчтеноПриИзменении(Элемент)
	ВремяУчтеноПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ВремяУчтеноПриИзмененииНаСервере()
	ЗарплатаКадрыРасширенный.УстановитьВторогоОтветственногоВМногофункциональныхДокументах(ЭтаФорма, РегистрацияВремениДоступна);
КонецПроцедуры

#Область ОбработчикиСобытийТаблицыСотрудники

&НаКлиенте
Процедура СотрудникиПриИзменении(Элемент)
	Если РегистрацияВремениДоступна Тогда 
		УстановитьСвойствоВремяУчтено();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура СотрудникиОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	ОбработкаПодбораНаСервере(ВыбранноеЗначение);
	
КонецПроцедуры

&НаКлиенте
Процедура ДниСверхурочнойРаботыОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если ЗначениеЗаполнено(ВыбранноеЗначение) Тогда
		ДобавитьДатуСервер(ВыбранноеЗначение);
	КонецЕсли;	
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаКлиенте
Процедура ДобавитьДату(Команда)
	
	СтруктураПараметровВыбора = Новый Структура;
	СтруктураПараметровВыбора.Вставить("Заголовок", НСтр("ru = 'Выбор даты'"));
	СтруктураПараметровВыбора.Вставить("ПоясняющийТекст", НСтр("ru = 'Выберите дату сверхурочной работы'"));
	
	ДатаСверхурочнойРаботы = ОткрытьФорму("ОбщаяФорма.ВыборДаты", СтруктураПараметровВыбора, Элементы.ДниСверхурочнойРаботы);
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьДату(Команда)
	
	МассивДат = Новый Массив;
	
	Для Каждого ИдентификаторСтроки Из Элементы.ДниСверхурочнойРаботы.ВыделенныеСтроки Цикл 
	
		ЭлементСписка = ДниСверхурочнойРаботы.НайтиПоИдентификатору(ИдентификаторСтроки);
		
		МассивДат.Добавить(ЭлементСписка.Значение);
		
		ДниСверхурочнойРаботы.Удалить(ЭлементСписка);
		
	КонецЦикла;	
	
	УдалитьКолонкиТаблицыСотрудники(МассивДат);
	
КонецПроцедуры

&НаКлиенте
Процедура Подбор(Команда)
	
	КоличествоДат = ДниСверхурочнойРаботы.Количество();
	
	Если КоличествоДат > 0 Тогда 
		ДатаНачала = ДниСверхурочнойРаботы[0].Значение;
		ДатаОкончания = ДниСверхурочнойРаботы[КоличествоДат-1].Значение;
	Иначе 
		ДатаНачала = Объект.ПериодРегистрации;
		ДатаОкончания = Объект.ПериодРегистрации;
	КонецЕсли;
	
	КадровыйУчетКлиент.ВыбратьСотрудниковРаботающихВПериоде(
		Элементы.Сотрудники, Объект.Организация, , ДатаНачала, ДатаОкончания, Истина, АдресСпискаПодобранныхСотрудников());
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ПриПолученииДанныхНаСервере()
	
	УстановитьДоступностьРегистрацииВремени();
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДате(ЭтаФорма, "Объект.ПериодРегистрации", "МесяцНачисленияСтрокой");
	УстановитьДоступностьЭлементов();
	ЗарплатаКадрыРасширенный.УстановитьВторогоОтветственногоВМногофункциональныхДокументах(ЭтаФорма, РегистрацияВремениДоступна);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПодбораНаСервере(МассивСотрудников)
	
	Для Каждого Сотрудник Из МассивСотрудников Цикл
		Если Сотрудники.НайтиСтроки(Новый Структура("Сотрудник", Сотрудник)).Количество() = 0 Тогда
			СтрокаТаблицы = Сотрудники.Добавить();
			СтрокаТаблицы.Сотрудник = Сотрудник;
			СтрокаТаблицы.СпособКомпенсацииПереработки = ПредопределенноеЗначение("Перечисление.СпособыКомпенсацииПереработки.ПовышеннаяОплата");
		КонецЕсли;	
	КонецЦикла;	
	
	Сотрудники.Сортировать("Сотрудник");
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьДатуСервер(ДатаСверхурочнойРаботы)
	
	Если ДниСверхурочнойРаботы.НайтиПоЗначению(ДатаСверхурочнойРаботы) = Неопределено Тогда 
		
		НовыйЭлемент = ДниСверхурочнойРаботы.Добавить(ДатаСверхурочнойРаботы, Формат(ДатаСверхурочнойРаботы, "ДЛФ=Д"));
		
		ДниСверхурочнойРаботы.СортироватьПоЗначению();
		
		ДобавитьКолонкиТаблицыСотрудники(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ДатаСверхурочнойРаботы));
		
		УстановитьДоступностьЭлементов();
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьКолонкиТаблицыСотрудники(МассивДат)
	
	// Формирование массива имен колонок таблицы Сотрудники.
	РеквизитыТаблицыСотрудники = ПолучитьРеквизиты("Сотрудники");
	
	ИменаКолонок = Новый Массив;
	
	Для Каждого РеквизитТаблицы Из РеквизитыТаблицыСотрудники Цикл 
		ИменаКолонок.Добавить(РеквизитТаблицы.Имя);
	КонецЦикла;
	
	// Добавление реквизитов формы.
	ДобавляемыеРеквизиты = Новый Массив;
	
	ПараметрыЧисла = Новый КвалификаторыЧисла(7, 2, ДопустимыйЗнак.Неотрицательный);
	
	Для Каждого ДатаРаботы Из МассивДат Цикл 
		
		Если ДатаРаботы = '00010101' Тогда 
			Продолжить;
		КонецЕсли;	
		
		ИмяКолонки = ИмяКолонкиСтрока(ДатаРаботы);
        ЗаголовокКолонки = Формат(ДатаРаботы, "ДЛФ=Д");
		
		Если ИменаКолонок.Найти(ИмяКолонки) = Неопределено Тогда 
			РеквизитФормы = Новый РеквизитФормы(ИмяКолонки, Новый ОписаниеТипов("Число", ПараметрыЧисла), "Сотрудники", ЗаголовокКолонки, Истина); 
			ДобавляемыеРеквизиты.Добавить(РеквизитФормы);
		КонецЕсли;
		
	КонецЦикла;

	ИзменитьРеквизиты(ДобавляемыеРеквизиты);

	// Добавление элементов формы
	Для Каждого РеквизитФормы Из ДобавляемыеРеквизиты Цикл 
		
		СледующийЭлемент = СледующийЭлементТаблицыСотрудники(РеквизитФормы.Имя);
		
		Элемент = Элементы.Вставить(РеквизитФормы.Имя, Тип("ПолеФормы"), Элементы.СотрудникиДаты, СледующийЭлемент); 
		Элемент.Вид = ВидПоляФормы.ПолеВвода;
		Элемент.ПутьКДанным = "Сотрудники." + РеквизитФормы.Имя;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура УдалитьКолонкиТаблицыСотрудники(МассивДат)
	
	// Удаление реквизитов формы
	УдаляемыеРеквизиты = Новый Массив;
	
	Для Каждого ДатаРаботы Из МассивДат Цикл 
		
		ПутьКРеквизиту = "Сотрудники." + ИмяКолонкиСтрока(ДатаРаботы);
		УдаляемыеРеквизиты.Добавить(ПутьКРеквизиту);
		
	КонецЦикла;

	ИзменитьРеквизиты(, УдаляемыеРеквизиты);

	// Удаление элементов формы
	Для Каждого ПутьКРеквизиту Из УдаляемыеРеквизиты Цикл 
		
		ИмяЭлемента = Прав(ПутьКРеквизиту, 12);
		
		Элемент = Элементы.Найти(ИмяЭлемента);
		
		Если Элемент <> Неопределено Тогда 
			Элементы.Удалить(Элемент);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Функция СледующийЭлементТаблицыСотрудники(ИмяНовойКолонки)

	Для Каждого Колонка Из Элементы.Сотрудники.ПодчиненныеЭлементы Цикл
		Если Лев(Колонка.Имя, 4) = "Дата" И ИмяНовойКолонки < Колонка.Имя Тогда 
			Возврат Колонка;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Неопределено;
	
КонецФункции

&НаСервере
Процедура ЗаполнитьОбъектПоДаннымФормы(ТекущийОбъект)
	
	УстановитьПривилегированныйРежим(Истина);
	
	ТекущийОбъект.Сотрудники.Очистить();
	
	ДниРаботы = Новый Структура;
	
	Для Каждого ЭлементСписка Из ДниСверхурочнойРаботы Цикл 
		ДниРаботы.Вставить(ИмяКолонкиСтрока(ЭлементСписка.Значение), ЭлементСписка.Значение);
	КонецЦикла;
	
	Для Каждого ТекСтрока Из Сотрудники Цикл 
		
		Для Каждого ДеньРаботы Из ДниРаботы Цикл 
			
			НоваяСтрока = ТекущийОбъект.Сотрудники.Добавить();
			НоваяСтрока.СпособКомпенсацииПереработки = ТекСтрока.СпособКомпенсацииПереработки;
			НоваяСтрока.Сотрудник 		= ТекСтрока.Сотрудник;
			НоваяСтрока.Дата 			= ДеньРаботы.Значение;
			НоваяСтрока.ОтработаноЧасов = ТекСтрока[ДеньРаботы.Ключ];
			
		КонецЦикла;
		
		Если ДниРаботы.Количество() = 0 Тогда 
			НоваяСтрока = ТекущийОбъект.Сотрудники.Добавить();
			НоваяСтрока.Сотрудник = ТекСтрока.Сотрудник;
		КонецЕсли;	
		
	КонецЦикла;
	
	Если Сотрудники.Количество() = 0 Тогда 
		Для Каждого ДеньРаботы Из ДниРаботы Цикл 
			НоваяСтрока = ТекущийОбъект.Сотрудники.Добавить();
			НоваяСтрока.Дата = ДеньРаботы.Значение;
		КонецЦикла;
	КонецЕсли;
	
	ТекущийОбъект.ДатаНачалаСобытия = ?(ДниСверхурочнойРаботы.Количество() > 0, ДниСверхурочнойРаботы[0].Значение, Объект.ПериодРегистрации);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьФормуПоДаннымОбъекта(ТекущийОбъект)
	
	УстановитьПривилегированныйРежим(Истина);
	
	ДниСверхурочнойРаботы.Очистить();
	Сотрудники.Очистить();
	
	Запрос = Новый Запрос;
	
	Запрос.УстановитьПараметр("Ссылка", ТекущийОбъект.Ссылка);
	
	Запрос.Текст = "ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	               |	РаботаСверхурочноСотрудники.Дата КАК Дата
	               |ИЗ
	               |	Документ.РаботаСверхурочно.Сотрудники КАК РаботаСверхурочноСотрудники
	               |ГДЕ
	               |	РаботаСверхурочноСотрудники.Ссылка = &Ссылка
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	Дата
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ РАЗРЕШЕННЫЕ
	               |	РаботаСверхурочноСотрудники.Сотрудник КАК Сотрудник,
	               |	РаботаСверхурочноСотрудники.Дата КАК Дата,
	               |	РаботаСверхурочноСотрудники.ОтработаноЧасов КАК ОтработаноЧасов,
	               |	РаботаСверхурочноСотрудники.СпособКомпенсацииПереработки КАК СпособКомпенсацииПереработки
	               |ИЗ
	               |	Документ.РаботаСверхурочно.Сотрудники КАК РаботаСверхурочноСотрудники
	               |ГДЕ
	               |	РаботаСверхурочноСотрудники.Ссылка = &Ссылка
	               |	И РаботаСверхурочноСотрудники.Сотрудник <> ЗНАЧЕНИЕ(Справочник.Сотрудники.ПустаяСсылка)
	               |ИТОГИ ПО
	               |	Сотрудник,
	               |	СпособКомпенсацииПереработки";
				   
	РезультатыЗапроса = Запрос.ВыполнитьПакет();
	
	// Заполнение таблицы ДниСверхурочнойРаботы и создание реквизитов таблицы Сотрудники.
	Выборка = РезультатыЗапроса[0].Выбрать();
	
	Пока Выборка.Следующий() Цикл 
		Если Выборка.Дата <> '00010101' Тогда 
			ДниСверхурочнойРаботы.Добавить(Выборка.Дата, Формат(Выборка.Дата, "ДЛФ=Д"));
		КонецЕсли;	
	КонецЦикла;	
	
	ДобавитьКолонкиТаблицыСотрудники(ДниСверхурочнойРаботы.ВыгрузитьЗначения());
	
	// Заполнение таблицы Сотрудники.
	ВыборкаИтогов = РезультатыЗапроса[1].Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	Пока ВыборкаИтогов.Следующий() Цикл 
		ВыборкаПоСотрудникам = ВыборкаИтогов.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		Пока ВыборкаПоСотрудникам.Следующий() Цикл 
			Выборка = ВыборкаПоСотрудникам.Выбрать();
			НоваяСтрока = Сотрудники.Добавить();
			НоваяСтрока.Сотрудник = ВыборкаПоСотрудникам.Сотрудник;
			НоваяСтрока.СпособКомпенсацииПереработки = ВыборкаПоСотрудникам.СпособКомпенсацииПереработки;
			Пока Выборка.Следующий() Цикл 
				Если Выборка.Дата <> '00010101' Тогда 
					ИмяКолонки = ИмяКолонкиСтрока(Выборка.Дата);
					НоваяСтрока[ИмяКолонки] = Выборка.ОтработаноЧасов;
				КонецЕсли;
			КонецЦикла;
		КонецЦикла;
	КонецЦикла;		

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ИмяКолонкиСтрока(Период)
	
	Возврат "Дата" + Формат(Период, "ДФ=""ггггММдд""");

КонецФункции

&НаСервере
Процедура УстановитьДоступностьРегистрацииВремени()
	
	РегистрацияВремениДоступна = Документы.РаботаСверхурочно.ПолныеПраваНаДокумент(); 
	
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступностьЭлементов()
	
	ИспользуетсяРасчетЗарплаты = ПолучитьФункциональнуюОпцию("ИспользоватьРасчетЗарплатыРасширенная");
	
	Для Каждого Колонка Из Элементы.Сотрудники.ПодчиненныеЭлементы Цикл
		Если Лев(Колонка.Имя, 4) = "Дата" Тогда 
			Колонка.ТолькоПросмотр = Не РегистрацияВремениДоступна;
		КонецЕсли;
	КонецЦикла;
	
	Если ИспользуетсяРасчетЗарплаты И Не РегистрацияВремениДоступна И Объект.ВремяУчтено Тогда 
		ТолькоПросмотр = Истина;
	КонецЕсли;
	
	Элементы.ИнфоНадпись.Видимость = ИспользуетсяРасчетЗарплаты И Не РегистрацияВремениДоступна И Объект.ВремяУчтено;
	
КонецПроцедуры	

&НаСервере
Процедура УстановитьСвойствоВремяУчтено()
	
	Если РегистрацияВремениДоступна Тогда 
		Объект.ВремяУчтено = Истина;
	КонецЕсли;
	
	ЗарплатаКадрыРасширенный.УстановитьВторогоОтветственногоВМногофункциональныхДокументах(ЭтаФорма, РегистрацияВремениДоступна);
	
КонецПроцедуры	

&НаСервере
Функция АдресСпискаПодобранныхСотрудников()
	
	Возврат ПоместитьВоВременноеХранилище(Сотрудники.Выгрузить(,"Сотрудник").ВыгрузитьКолонку("Сотрудник"), УникальныйИдентификатор);
	
КонецФункции

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	
	ЗаполнитьДанныеФормыПоОрганизации();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеФормыПоОрганизации()
	
	Если НЕ ЗначениеЗаполнено(Объект.Организация) Тогда
		Возврат;
	КонецЕсли; 
	
	ЗапрашиваемыеЗначения = Новый Структура;
	ЗапрашиваемыеЗначения.Вставить("Организация", "Объект.Организация");
	
	ЗапрашиваемыеЗначения.Вставить("Руководитель", "Объект.Руководитель");
	ЗапрашиваемыеЗначения.Вставить("ДолжностьРуководителя", "Объект.ДолжностьРуководителя");
	
	ЗарплатаКадры.ЗаполнитьЗначенияВФорме(ЭтаФорма, ЗапрашиваемыеЗначения, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("Организация"));	
	
КонецПроцедуры

&НаКлиенте
Процедура СотрудникиПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	Если НоваяСтрока Тогда
		Элемент.ТекущиеДанные.СпособКомпенсацииПереработки = ПредопределенноеЗначение("Перечисление.СпособыКомпенсацииПереработки.ПовышеннаяОплата");
	КонецЕсли;
КонецПроцедуры

#КонецОбласти
