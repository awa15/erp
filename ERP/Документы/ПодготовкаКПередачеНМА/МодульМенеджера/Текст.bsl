﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ДанныеДокумента.Ссылка КАК Ссылка,
	|	ДанныеДокумента.Дата КАК Период,
	|	ДанныеДокумента.Номер КАК Номер,
	|	ДанныеДокумента.Организация КАК Организация,
	|	ДанныеДокумента.Подразделение КАК Подразделение,
	|	ДанныеДокумента.НематериальныйАктив КАК НематериальныйАктив,
	|	ДанныеДокумента.СтатьяРасходов КАК СтатьяРасходов,
	|	ДанныеДокумента.АналитикаРасходов КАК АналитикаРасходов,
	|	ДанныеДокумента.СтатьяРасходов.ПринятиеКНалоговомуУчету КАК СтатьяРасходовПринятиеКНалоговомуУчету,
	|	ДанныеДокумента.СтатьяРасходов.ВариантРаспределенияРасходов КАК СтатьяРасходовВариантРаспределенияРасходов
	|ИЗ
	|	Документ.ПодготовкаКПередачеНМА КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка";
	
	Результат = Запрос.Выполнить();
	Реквизиты = Результат.Выбрать();
	Реквизиты.Следующий();
	
	УправлениеВнеоборотнымиАктивами.ИнициализироватьПараметрыЗапросаПриОтраженииАмортизации(Запрос, ДополнительныеСвойства);
	Запрос.УстановитьПараметр("Граница", Новый Граница(НачалоМесяца(Реквизиты.Период), ВидГраницы.Исключая));
	Запрос.УстановитьПараметр("КонецМесяца", Новый Граница(КонецМесяца(Реквизиты.Период), ВидГраницы.Включая));
	Запрос.УстановитьПараметр("СписаниеОстаточнойСтоимости", Истина);
	Для Каждого Колонка Из Результат.Колонки Цикл
		Запрос.УстановитьПараметр(Колонка.Имя, Реквизиты[Колонка.Имя]);
	КонецЦикла;
	
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстыЗапроса.Добавить(УправлениеВнеоборотнымиАктивами.ВременнаяТаблицаНачисленнаяАмортизация(), "");
	ТекстыЗапроса.Добавить(УправлениеВнеоборотнымиАктивами.ВременнаяТаблицаСтоимостиУпр(), "");
	ТекстыЗапроса.Добавить(УправлениеВнеоборотнымиАктивами.ВременнаяТаблицаАмортизацияПрочиеРасходы(), "");
	ТекстыЗапроса.Добавить(ВременнаяТаблицаОстаточнойСтоимости(), "");
	ТекстыЗапроса.Добавить(ПервоначальныеСведенияНМАБухгалтерскийУчет(), "ПервоначальныеСведенияНМАБухгалтерскийУчет");
	ТекстыЗапроса.Добавить(ПервоначальныеСведенияНМАНалоговыйУчет(), "ПервоначальныеСведенияНМАНалоговыйУчет");
	ТекстыЗапроса.Добавить(СостоянияНМАОрганизаций(), "СостоянияНМАОрганизаций");
	ТекстыЗапроса.Добавить(УправлениеВнеоборотнымиАктивами.ПрочиеРасходы(ПрочиеРасходы()), "ПрочиеРасходы");
	ТекстыЗапроса.Добавить(УправлениеВнеоборотнымиАктивами.ПартииПрочихРасходов(ПартииПрочихРасходов()), "ПартииПрочихРасходов");
	ТекстыЗапроса.Добавить(УправлениеВнеоборотнымиАктивами.ПорядокОтраженияПрочихОпераций(), "ТаблицаПорядокОтраженияПрочихОпераций");
	
	ПроведениеСервер.ИницализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений);
	
КонецПроцедуры

Функция ВременнаяТаблицаОстаточнойСтоимости()
	
	Возврат "
	|////////////////////////////////////////////////////////////////////////////////
	|// Временная таблица ОстаточнаяСтоимостьНМА
	|"+
	"ВЫБРАТЬ
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаОстатокДт,0)
	|	- ЕСТЬNULL(ДанныеСчетАмортизации.СуммаОстатокКт,0)
	|	- ЕСТЬNULL(Амортизация.СуммаБУ,0) КАК ОстаточнаяСтоимостьБУ,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаПРОстатокДт,0)
	|	- ЕСТЬNULL(ДанныеСчетАмортизации.СуммаПРОстатокКт,0)
	|	- ЕСТЬNULL(Амортизация.СуммаПР,0) КАК ОстаточнаяСтоимостьПР,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаВРОстатокДт,0)
	|	- ЕСТЬNULL(ДанныеСчетАмортизации.СуммаВРОстатокКт,0)
	|	- ЕСТЬNULL(Амортизация.СуммаВР,0) КАК ОстаточнаяСтоимостьВР
	|ПОМЕСТИТЬ ОстаточнаяСтоимостьНМА
	|ИЗ
	|	Документ.ПодготовкаКПередачеНМА КАК ДанныеДокумента
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ (
	|		ВЫБРАТЬ
	|			Т.НМА КАК НематериальныйАктив,
	|			СУММА(Т.СуммаБУ) КАК СуммаБУ,
	|			СУММА(Т.СуммаНУ) КАК СуммаНУ,
	|			СУММА(Т.СуммаПР) КАК СуммаПР,
	|			СУММА(Т.СуммаВР) КАК СуммаВР
	|		ИЗ Документ.ПодготовкаКПередачеНМА.НачисленнаяАмортизация КАК Т
	|		ГДЕ Т.Ссылка = &Ссылка
	|		СГРУППИРОВАТЬ ПО Т.НМА
	|		) КАК Амортизация
	|		ПО ДанныеДокумента.НематериальныйАктив = Амортизация.НематериальныйАктив
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&КонецМесяца,
	|			Счет В 
	|				(ВЫБРАТЬ СчетаОтражения.СчетУчета
	|				ИЗ РегистрСведений.СчетаБухгалтерскогоУчетаНМА.СрезПоследних(
	|					&Период,
	|					(Организация, НематериальныйАктив) В
	|						(ВЫБРАТЬ
	|							Т.Организация,
	|							Т.НематериальныйАктив
	|						ИЗ
	|							Документ.ПодготовкаКПередачеНМА КАК Т
	|						ГДЕ Т.Ссылка = &Ссылка)) КАК СчетаОтражения),,
	|			(Организация, Подразделение, Субконто1) В
	|				(ВЫБРАТЬ
	|					Т.Организация,
	|					Т.Подразделение,
	|					Т.НематериальныйАктив
	|				ИЗ
	|					Документ.ПодготовкаКПередачеНМА КАК Т
	|				ГДЕ Т.Ссылка = &Ссылка)
	|			) КАК ДанныеСчетУчета
	|		ПО ДанныеДокумента.НематериальныйАктив = ДанныеСчетУчета.Субконто1
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&КонецМесяца,
	|			Счет В 
	|				(ВЫБРАТЬ СчетаОтражения.СчетНачисленияАмортизации
	|				ИЗ РегистрСведений.СчетаБухгалтерскогоУчетаНМА.СрезПоследних(
	|					&Период,
	|					(Организация, НематериальныйАктив) В
	|						(ВЫБРАТЬ
	|							Т.Организация,
	|							Т.НематериальныйАктив
	|						ИЗ
	|							Документ.ПодготовкаКПередачеНМА КАК Т
	|						ГДЕ Т.Ссылка = &Ссылка)) КАК СчетаОтражения),,
	|			(Организация, Подразделение, Субконто1) В
	|				(ВЫБРАТЬ
	|					Т.Организация,
	|					Т.Подразделение,
	|					Т.НематериальныйАктив
	|				ИЗ
	|					Документ.ПодготовкаКПередачеНМА КАК Т
	|				ГДЕ Т.Ссылка = &Ссылка)
	|			) КАК ДанныеСчетАмортизации
	|		ПО ДанныеДокумента.НематериальныйАктив = ДанныеСчетАмортизации.Субконто1
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|" + ";";
	
КонецФункции

функция ПервоначальныеСведенияНМАБухгалтерскийУчет()
	
	Возврат "
	|////////////////////////////////////////////////////////////////////////////////
	|// Таблица ПервоначальныеСведенияНМАБухгалтерскийУчет
	|"+
	"ВЫБРАТЬ
	|	&Ссылка КАК Регистратор,
	|	&Период КАК Период,
	|	
	|	&НематериальныйАктив КАК НематериальныйАктив,
	|	
	|	&Организация КАК Организация,
	|	НЕОПРЕДЕЛЕНО КАК ПервоначальнаяСтоимость,
	|	НЕОПРЕДЕЛЕНО КАК СпособПоступления,
	|	ЛОЖЬ КАК НачислятьАмортизацию,
	|	НЕОПРЕДЕЛЕНО КАК СпособНачисленияАмортизации,
	|	НЕОПРЕДЕЛЕНО КАК СрокПолезногоИспользования,
	|	НЕОПРЕДЕЛЕНО КАК ОбъемПродукцииРаботДляВычисленияАмортизации,
	|	НЕОПРЕДЕЛЕНО КАК Коэффициент" + ";";
	
КонецФункции

функция ПервоначальныеСведенияНМАНалоговыйУчет()
	
	Возврат "
	|////////////////////////////////////////////////////////////////////////////////
	|// Таблица ПервоначальныеСведенияНМАНалоговыйУчет
	|"+
	"ВЫБРАТЬ
	|	&Ссылка КАК Регистратор,
	|	&Период КАК Период,
	|	
	|	&НематериальныйАктив КАК НематериальныйАктив,
	|	
	|	НЕОПРЕДЕЛЕНО КАК АмортизацияДо2002,
	|	НЕОПРЕДЕЛЕНО КАК АмортизацияДо2009,
	|	НЕОПРЕДЕЛЕНО КАК ДатаПриобретения,
	|	НЕОПРЕДЕЛЕНО КАК МетодНачисленияАмортизации,
	|	ЛОЖЬ КАК НачислятьАмортизацию,
	|	НЕОПРЕДЕЛЕНО КАК Организация,
	|	НЕОПРЕДЕЛЕНО КАК ПервоначальнаяСтоимостьНУ,
	|	НЕОПРЕДЕЛЕНО КАК СрокПолезногоИспользования,
	|	НЕОПРЕДЕЛЕНО КАК СтоимостьДо2002,
	|	НЕОПРЕДЕЛЕНО КАК ФактическийСрокИспользованияДо2009" + ";";
	
КонецФункции

функция СостоянияНМАОрганизаций()
	
	Возврат "
	|////////////////////////////////////////////////////////////////////////////////
	|// Таблица СостоянияНМАОрганизаций
	|"+
	"ВЫБРАТЬ
	|	&Ссылка КАК Регистратор,
	|	&Период КАК Период,
	|	
	|	&Организация КАК Организация,
	|	&НематериальныйАктив КАК НематериальныйАктив,
	|	
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСостоянийНМА.Списан) КАК Состояние" + ";";
	
КонецФункции

Функция ПрочиеРасходы()
	
	Возврат
	"ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Ссылка КАК Регистратор,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	&Организация КАК Организация,
	|	&Подразделение КАК Подразделение,
	|	&СтатьяРасходов КАК СтатьяРасходов,
	|	&АналитикаРасходов КАК АналитикаРасходов,
	|	
	|	0 КАК Сумма,
	|	
	|	0 КАК СуммаБезНДС,
	|	
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьБУ КАК СуммаРегл,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьПР КАК ПостояннаяРазница,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьВР КАК ВременнаяРазница,
	|	
	|	Неопределено КАК ХозяйственнаяОперация,
	|	Неопределено КАК АналитикаУчетаНоменклатуры
	|ИЗ
	|	ОстаточнаяСтоимостьНМА КАК ОстаточнаяСтоимостьНМА
	|ГДЕ
	|	&СтатьяРасходовПринятиеКНалоговомуУчету = ИСТИНА
	|	И НЕ &СтатьяРасходовВариантРаспределенияРасходов В (
	|		ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаПрочиеАктивы),
	|		ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|	)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Ссылка КАК Регистратор,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	&Организация КАК Организация,
	|	&Подразделение КАК Подразделение,
	|	&СтатьяРасходов КАК СтатьяРасходов,
	|	&АналитикаРасходов КАК АналитикаРасходов,
	|	
	|	0 КАК Сумма,
	|	
	|	0 КАК СуммаБезНДС,
	|	
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьБУ КАК СуммаРегл,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьБУ КАК ПостояннаяРазница,
	|	0 КАК ВременнаяРазница,
	|	
	|	Неопределено КАК ХозяйственнаяОперация,
	|	Неопределено КАК АналитикаУчетаНоменклатуры
	|ИЗ
	|	ОстаточнаяСтоимостьНМА КАК ОстаточнаяСтоимостьНМА
	|ГДЕ
	|	&СтатьяРасходовПринятиеКНалоговомуУчету = ЛОЖЬ
	|	И НЕ &СтатьяРасходовВариантРаспределенияРасходов В (
	|		ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаПрочиеАктивы),
	|		ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров)
	|	)";
	
КонецФункции

Функция ПартииПрочихРасходов()
	
	Возврат
	"ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Ссылка КАК Регистратор,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	
	|	&Организация КАК Организация,
	|	&Подразделение КАК Подразделение,
	|	&Ссылка КАК ДокументПоступленияРасходов,
	|	&СтатьяРасходов КАК СтатьяРасходов,
	|	&АналитикаРасходов КАК АналитикаРасходов,
	|	НЕОПРЕДЕЛЕНО КАК АналитикаУчетаПартий,
	|	
	|	0 КАК Стоимость,
	|	0 КАК СтоимостьБезНДС,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьБУ КАК СтоимостьРегл,
	|	0 КАК НДСРегл,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьПР КАК ПостояннаяРазница,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьВР КАК ВременнаяРазница
	|ИЗ
	|	ОстаточнаяСтоимостьНМА КАК ОстаточнаяСтоимостьНМА
	|ГДЕ
	|	&СтатьяРасходовПринятиеКНалоговомуУчету = ИСТИНА
	|	И &СтатьяРасходовВариантРаспределенияРасходов В (ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров))
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	&Период КАК Период,
	|	&Ссылка КАК Регистратор,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	
	|	&Организация КАК Организация,
	|	&Подразделение КАК Подразделение,
	|	&Ссылка КАК ДокументПоступленияРасходов,
	|	&СтатьяРасходов КАК СтатьяРасходов,
	|	&АналитикаРасходов КАК АналитикаРасходов,
	|	НЕОПРЕДЕЛЕНО КАК АналитикаУчетаПартий,
	|	
	|	0 КАК Стоимость,
	|	0 КАК СтоимостьБезНДС,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьБУ КАК СтоимостьРегл,
	|	0 КАК НДСРегл,
	|	ОстаточнаяСтоимостьНМА.ОстаточнаяСтоимостьБУ КАК ПостояннаяРазница,
	|	0 КАК ВременнаяРазница
	|ИЗ
	|	ОстаточнаяСтоимостьНМА КАК ОстаточнаяСтоимостьНМА
	|ГДЕ
	|	&СтатьяРасходовПринятиеКНалоговомуУчету = ЛОЖЬ
	|	И &СтатьяРасходовВариантРаспределенияРасходов В (ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаСебестоимостьТоваров))";
	
КонецФункции

#КонецОбласти

#Область ПроведениеПоРегламентированномуУчету

// Возвращает текст запроса для отражения документа в регламентированном учете.
//
// Возвращаемое значение:
// 		Строка - Текст запроса
//
Функция ТекстОтраженияВРеглУчете() Экспорт
	
	Разделитель = Символы.ПС + "ОБЪЕДИНИТЬ ВСЕ" + Символы.ПС;
	
	Возврат УчетНМА.ТекстОтраженияВРеглУчетеНачисленнойАмортизации("ПодготовкаКПередачеНМА")
		+ Разделитель + ПереносНачисленнойАмортизацииНМА()
		+ Разделитель + СписаниеОстаточнойСтоимостиНМА();
	
КонецФункции

Функция ПереносНачисленнойАмортизацииНМА()
	
	Возврат "
	|////////////////////////////////////////////////////////////////////////////////////////////////////
	|// Перенос начисленной амортизации НМА (Дт СчетНакопленияАмортизации :: Кт СчетУчета)
	|ВЫБРАТЬ
	|	Операция.Дата КАК Период,
	|	Операция.Организация КАК Организация,
	|	НЕОПРЕДЕЛЕНО КАК ИдентификаторСтроки,
	|	
	|	ЕСТЬNULL(ДанныеСчетАмортизации.СуммаОстатокКт,0) + ЕСТЬNULL(Амортизация.СуммаБУ, 0) КАК Сумма,
	|	
	|	// Дт ///////////////////////////////////////////////////////////////////////////////////////////
	|	
	|	НЕОПРЕДЕЛЕНО КАК ВидСчетаДт,
	|	НЕОПРЕДЕЛЕНО КАК АналитикаУчетаДт,
	|	НЕОПРЕДЕЛЕНО КАК МестоУчетаДт,
	|	
	|	НЕОПРЕДЕЛЕНО КАК ВалютаДт,
	|	Операция.Подразделение КАК ПодразделениеДт,
	|	СчетаОтражения.СчетНачисленияАмортизации КАК СчетДт,
	|	
	|	Операция.НематериальныйАктив КАК СубконтоДт1,
	|	НЕОПРЕДЕЛЕНО КАК СубконтоДт2,
	|	НЕОПРЕДЕЛЕНО КАК СубконтоДт3,
	|	
	|	0 КАК ВалютнаяСуммаДт,
	|	0 КАК КоличествоДт,
	|	ЕСТЬNULL(ДанныеСчетАмортизации.СуммаНУОстатокКт,0) + ЕСТЬNULL(Амортизация.СуммаНУ, 0) КАК СуммаНУДт,
	|	ЕСТЬNULL(ДанныеСчетАмортизации.СуммаПРОстатокКт,0) + ЕСТЬNULL(Амортизация.СуммаПР, 0) КАК СуммаПРДт,
	|	ЕСТЬNULL(ДанныеСчетАмортизации.СуммаВРОстатокКт,0) + ЕСТЬNULL(Амортизация.СуммаВР, 0) КАК СуммаВРДт,
	|	
	|	// Кт ///////////////////////////////////////////////////////////////////////////////////////////
	|	
	|	НЕОПРЕДЕЛЕНО КАК ВидСчетаКт,
	|	НЕОПРЕДЕЛЕНО КАК ГруппаФинансовогоУчетаКт,
	|	НЕОПРЕДЕЛЕНО КАК МестоУчетаКт,
	|	
	|	НЕОПРЕДЕЛЕНО КАК ВалютаКт,
	|	Операция.Подразделение КАК ПодразделениеКт,
	|	СчетаОтражения.СчетУчета КАК СчетКт,
	|	
	|	Операция.НематериальныйАктив КАК СубконтоКт1,
	|	НЕОПРЕДЕЛЕНО КАК СубконтоКт2,
	|	НЕОПРЕДЕЛЕНО КАК СубконтоКт3,
	|	
	|	0 КАК ВалютнаяСуммаКт,
	|	0 КАК КоличествоКт,
	|	ЕСТЬNULL(ДанныеСчетАмортизации.СуммаНУОстатокКт,0) + ЕСТЬNULL(Амортизация.СуммаНУ, 0) КАК СуммаНУКт,
	|	ЕСТЬNULL(ДанныеСчетАмортизации.СуммаПРОстатокКт,0) + ЕСТЬNULL(Амортизация.СуммаПР, 0) КАК СуммаПРКт,
	|	ЕСТЬNULL(ДанныеСчетАмортизации.СуммаВРОстатокКт,0) + ЕСТЬNULL(Амортизация.СуммаВР, 0) КАК СуммаВРКт
	|ИЗ
	|	Документ.ПодготовкаКПередачеНМА КАК Операция
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ (
	|		ВЫБРАТЬ
	|			Т.НМА КАК НематериальныйАктив,
	|			СУММА(Т.СуммаБУ) КАК СуммаБУ,
	|			СУММА(Т.СуммаНУ) КАК СуммаНУ,
	|			СУММА(Т.СуммаПР) КАК СуммаПР,
	|			СУММА(Т.СуммаВР) КАК СуммаВР
	|		ИЗ Документ.ПодготовкаКПередачеНМА.НачисленнаяАмортизация КАК Т
	|		ГДЕ Т.Ссылка = &Ссылка
	|		СГРУППИРОВАТЬ ПО Т.НМА
	|		) КАК Амортизация
	|		ПО Операция.НематериальныйАктив = Амортизация.НематериальныйАктив
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.СчетаБухгалтерскогоУчетаНМА.СрезПоследних(
	|			&Дата,
	|			(Организация, НематериальныйАктив) В
	|				(ВЫБРАТЬ
	|					Т.Организация,
	|					Т.НематериальныйАктив
	|				ИЗ
	|					Документ.ПодготовкаКПередачеНМА КАК Т
	|				ГДЕ Т.Ссылка = &Ссылка)) КАК СчетаОтражения
	|		ПО Операция.НематериальныйАктив = СчетаОтражения.НематериальныйАктив
	|	
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаМесяцОкончание,
	|			Счет В 
	|				(ВЫБРАТЬ СчетаОтражения.СчетНачисленияАмортизации
	|				ИЗ РегистрСведений.СчетаБухгалтерскогоУчетаНМА.СрезПоследних(
	|					&Дата,
	|					(Организация, НематериальныйАктив) В
	|						(ВЫБРАТЬ
	|							Т.Организация,
	|							Т.НематериальныйАктив
	|						ИЗ
	|							Документ.ПодготовкаКПередачеНМА КАК Т
	|						ГДЕ Т.Ссылка = &Ссылка)) КАК СчетаОтражения),,
	|			(Организация, Подразделение, Субконто1) В
	|				(ВЫБРАТЬ
	|					Т.Организация,
	|					Т.Подразделение,
	|					Т.НематериальныйАктив
	|				ИЗ
	|					Документ.ПодготовкаКПередачеНМА КАК Т
	|				ГДЕ Т.Ссылка = &Ссылка)
	|			) КАК ДанныеСчетАмортизации
	|		ПО Операция.НематериальныйАктив = ДанныеСчетАмортизации.Субконто1
	|			И СчетаОтражения.СчетНачисленияАмортизации = ДанныеСчетАмортизации.Счет
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ Справочник.НематериальныеАктивы КАК НематериальныеАктивы
	|		ПО Операция.НематериальныйАктив = НематериальныеАктивы.Ссылка
	|	
	|ГДЕ
	|	Операция.Ссылка = &Ссылка
	|	И ЕСТЬNULL(НематериальныеАктивы.ВидОбъектаУчета, Неопределено) <> ЗНАЧЕНИЕ(Перечисление.ВидыОбъектовУчетаНМА.РасходыНаНИОКР)
	|";
	
КонецФункции

Функция СписаниеОстаточнойСтоимостиНМА()
	
	Возврат "
	|////////////////////////////////////////////////////////////////////////////////////////////////////
	|// Списание остаточной стоимости НМА (Дт Расходы :: Кт СчетУчета)
	|ВЫБРАТЬ
	|	Операция.Дата КАК Период,
	|	Операция.Организация КАК Организация,
	|	НЕОПРЕДЕЛЕНО КАК ИдентификаторСтроки,
	|	
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаОстатокДт,0) - ЕСТЬNULL(ДанныеСчетАмортизации.СуммаОстатокКт,0) - ЕСТЬNULL(Амортизация.СуммаБУ, 0) КАК Сумма,
	|	
	|	// Дт ///////////////////////////////////////////////////////////////////////////////////////////
	|	
	|	ВЫБОР КОГДА СтатьиСтроительства.ВидЦенностиНДС = ЗНАЧЕНИЕ(Перечисление.ВидыЦенностей.ПрочиеРаботыИУслуги)
	|		ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыСчетовРеглУчета.ПрочиеОперации)
	|		ИНАЧЕ ЗНАЧЕНИЕ(Перечисление.ВидыСчетовРеглУчета.Расходы)
	|	КОНЕЦ КАК ВидСчетаДт,
	|	Операция.СтатьяРасходов КАК АналитикаУчетаДт,
	|	Операция.Подразделение КАК МестоУчетаДт,
	|	
	|	ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка) КАК ВалютаДт,
	|	Операция.Подразделение КАК ПодразделениеДт,
	|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПустаяСсылка) КАК СчетДт,
	|	
	|	Операция.СтатьяРасходов КАК СубконтоДт1,
	|	Операция.НематериальныйАктив КАК СубконтоДт2,
	|	Операция.АналитикаРасходов КАК СубконтоДт3,
	|	
	|	0 КАК ВалютнаяСуммаДт,
	|	0 КАК КоличествоДт,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаНУОстатокДт,0) - ЕСТЬNULL(ДанныеСчетАмортизации.СуммаНУОстатокКт,0) - ЕСТЬNULL(Амортизация.СуммаНУ, 0) КАК СуммаНУДт,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаПРОстатокДт,0) - ЕСТЬNULL(ДанныеСчетАмортизации.СуммаПРОстатокКт,0) - ЕСТЬNULL(Амортизация.СуммаПР, 0) КАК СуммаПРДт,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаВРОстатокДт,0) - ЕСТЬNULL(ДанныеСчетАмортизации.СуммаВРОстатокКт,0) - ЕСТЬNULL(Амортизация.СуммаВР, 0) КАК СуммаВРДт,
	|	
	|	// Кт ///////////////////////////////////////////////////////////////////////////////////////////
	|	
	|	НЕОПРЕДЕЛЕНО КАК ВидСчетаКт,
	|	НЕОПРЕДЕЛЕНО КАК ГруппаФинансовогоУчетаКт,
	|	НЕОПРЕДЕЛЕНО КАК МестоУчетаКт,
	|	
	|	НЕОПРЕДЕЛЕНО КАК ВалютаКт,
	|	Операция.Подразделение КАК ПодразделениеКт,
	|	СчетаОтражения.СчетУчета КАК СчетКт,
	|	
	|	Операция.НематериальныйАктив КАК СубконтоКт1,
	|	НЕОПРЕДЕЛЕНО КАК СубконтоКт2,
	|	НЕОПРЕДЕЛЕНО КАК СубконтоКт3,
	|	
	|	0 КАК ВалютнаяСуммаКт,
	|	0 КАК КоличествоКт,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаНУОстатокДт,0) - ЕСТЬNULL(ДанныеСчетАмортизации.СуммаНУОстатокКт,0) - ЕСТЬNULL(Амортизация.СуммаНУ, 0) КАК СуммаНУКт,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаПРОстатокДт,0) - ЕСТЬNULL(ДанныеСчетАмортизации.СуммаПРОстатокКт,0) - ЕСТЬNULL(Амортизация.СуммаПР, 0) КАК СуммаПРКт,
	|	ЕСТЬNULL(ДанныеСчетУчета.СуммаВРОстатокДт,0) - ЕСТЬNULL(ДанныеСчетАмортизации.СуммаВРОстатокКт,0) - ЕСТЬNULL(Амортизация.СуммаВР, 0) КАК СуммаВРКт
	|ИЗ
	|	Документ.ПодготовкаКПередачеНМА КАК Операция
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ (
	|		ВЫБРАТЬ
	|			Т.НМА КАК НематериальныйАктив,
	|			СУММА(Т.СуммаБУ) КАК СуммаБУ,
	|			СУММА(Т.СуммаНУ) КАК СуммаНУ,
	|			СУММА(Т.СуммаПР) КАК СуммаПР,
	|			СУММА(Т.СуммаВР) КАК СуммаВР
	|		ИЗ Документ.ПодготовкаКПередачеНМА.НачисленнаяАмортизация КАК Т
	|		ГДЕ Т.Ссылка = &Ссылка
	|		СГРУППИРОВАТЬ ПО Т.НМА
	|		) КАК Амортизация
	|		ПО Операция.НематериальныйАктив = Амортизация.НематериальныйАктив
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.СчетаБухгалтерскогоУчетаНМА.СрезПоследних(
	|			&Дата,
	|			(Организация, НематериальныйАктив) В
	|				(ВЫБРАТЬ
	|					Т.Организация,
	|					Т.НематериальныйАктив
	|				ИЗ
	|					Документ.ПодготовкаКПередачеНМА КАК Т
	|				ГДЕ Т.Ссылка = &Ссылка)) КАК СчетаОтражения
	|		ПО Операция.НематериальныйАктив = СчетаОтражения.НематериальныйАктив
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаМесяцОкончание,
	|			Счет В 
	|				(ВЫБРАТЬ СчетаОтражения.СчетУчета
	|				ИЗ РегистрСведений.СчетаБухгалтерскогоУчетаНМА.СрезПоследних(
	|					&Дата,
	|					(Организация, НематериальныйАктив) В
	|						(ВЫБРАТЬ
	|							Т.Организация,
	|							Т.НематериальныйАктив
	|						ИЗ
	|							Документ.ПодготовкаКПередачеНМА КАК Т
	|						ГДЕ Т.Ссылка = &Ссылка)) КАК СчетаОтражения),,
	|			(Организация, Подразделение, Субконто1) В
	|				(ВЫБРАТЬ
	|					Т.Организация,
	|					Т.Подразделение,
	|					Т.НематериальныйАктив
	|				ИЗ
	|					Документ.ПодготовкаКПередачеНМА КАК Т
	|				ГДЕ Т.Ссылка = &Ссылка)
	|			) КАК ДанныеСчетУчета
	|		ПО Операция.НематериальныйАктив = ДанныеСчетУчета.Субконто1
	|			И СчетаОтражения.СчетУчета = ДанныеСчетУчета.Счет
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрБухгалтерии.Хозрасчетный.Остатки(
	|			&ГраницаМесяцОкончание,
	|			Счет В 
	|				(ВЫБРАТЬ СчетаОтражения.СчетНачисленияАмортизации
	|				ИЗ РегистрСведений.СчетаБухгалтерскогоУчетаНМА.СрезПоследних(
	|					&Дата,
	|					(Организация, НематериальныйАктив) В
	|						(ВЫБРАТЬ
	|							Т.Организация,
	|							Т.НематериальныйАктив
	|						ИЗ
	|							Документ.ПодготовкаКПередачеНМА КАК Т
	|						ГДЕ Т.Ссылка = &Ссылка)) КАК СчетаОтражения),,
	|			(Организация, Подразделение, Субконто1) В
	|				(ВЫБРАТЬ
	|					Т.Организация,
	|					Т.Подразделение,
	|					Т.НематериальныйАктив
	|				ИЗ
	|					Документ.ПодготовкаКПередачеНМА КАК Т
	|				ГДЕ Т.Ссылка = &Ссылка)
	|			) КАК ДанныеСчетАмортизации
	|		ПО Операция.НематериальныйАктив = ДанныеСчетАмортизации.Субконто1
	|			И СчетаОтражения.СчетНачисленияАмортизации = ДанныеСчетАмортизации.Счет
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		ПланВидовХарактеристик.СтатьиРасходов КАК СтатьиСтроительства
	|	ПО
	|		Операция.СтатьяРасходов = СтатьиСтроительства.Ссылка
	|		И СтатьиСтроительства.ВариантРаспределенияРасходов = ЗНАЧЕНИЕ(Перечисление.ВариантыРаспределенияРасходов.НаПрочиеАктивы)
	|	
	|ГДЕ
	|	Операция.Ссылка = &Ссылка
	|	И НЕ (ДанныеСчетУчета.СуммаОстатокДт ЕСТЬ NULL И ДанныеСчетАмортизации.СуммаОстатокКт ЕСТЬ NULL)
	|";
	
КонецФункции

#КонецОбласти

#Область Печать

Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	
КонецПроцедуры

#КонецОбласти

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления 2.0.10.18
// Создает документы подготовки к передаче из документов "РеализацияУслугПрочихАктивов"
//
// Параметры:
// 		Параметры - Структура - Структура параметров обработчика обновления
//
Процедура СоздатьДокументыПоРеализацииУслугПрочихАктивов(Параметры) Экспорт
	
	Параметры.ОбработкаЗавершена = Ложь;
	
	Запрос = Новый Запрос;
	#Область ТекстЗапроса
	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1000
	|	ТаблицаДокументов.Ссылка КАК СсылкаРеализацияУслугПрочихАктивов,
	|	ТаблицаДокументов.Дата,
	|	ТаблицаДокументов.Организация,
	|	ТаблицаДокументов.Подразделение,
	|	ТаблицаДокументов.Менеджер КАК Ответственный,
	|	ТаблицаДокументов.Партнер,
	|	ТаблицаДокументов.Контрагент
	|ПОМЕСТИТЬ втДокументы
	|ИЗ
	|	Документ.РеализацияУслугПрочихАктивов КАК ТаблицаДокументов
	|ГДЕ
	|	ТаблицаДокументов.Проведен
	|	И НЕ ТаблицаДокументов.ПометкаУдаления
	|	И ТаблицаДокументов.Ссылка В
	|			(ВЫБРАТЬ ПЕРВЫЕ 1
	|				Т.Регистратор
	|			ИЗ
	|				РегистрСведений.СостоянияНМАОрганизаций КАК Т
	|			ГДЕ
	|				Т.Регистратор = ТаблицаДокументов.Ссылка)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втДокументы.СсылкаРеализацияУслугПрочихАктивов,
	|	втДокументы.Дата,
	|	втДокументы.Организация,
	|	втДокументы.Подразделение,
	|	втДокументы.Ответственный,
	|	втДокументы.Партнер,
	|	втДокументы.Контрагент
	|ИЗ
	|	втДокументы КАК втДокументы
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТЧРеализацияУслугПрочихАктивов.Ссылка КАК СсылкаРеализацияУслугПрочихАктивов,
	|	ТЧРеализацияУслугПрочихАктивов.НомерСтроки,
	|	ТЧРеализацияУслугПрочихАктивов.АналитикаДоходов КАК НематериальныйАктив
	|ИЗ
	|	Документ.РеализацияУслугПрочихАктивов.Доходы КАК ТЧРеализацияУслугПрочихАктивов
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втДокументы КАК втДокументы
	|		ПО ТЧРеализацияУслугПрочихАктивов.Ссылка = втДокументы.СсылкаРеализацияУслугПрочихАктивов
	|ГДЕ
	|	ТЧРеализацияУслугПрочихАктивов.СтатьяДоходов.ДоходыПоНМАиНИОКР";
	#КонецОбласти
	Результат = Запрос.ВыполнитьПакет();
	
	Если Результат[1].Пустой() Тогда
		Параметры.ОбработкаЗавершена = Истина;
		Возврат;
	КонецЕсли;
	
	Если ОбщегоНазначения.ЭтоПодчиненныйУзелРИБ() Тогда
		// Изменения, позволяющие установить признак завершения обработки,
		// должны быть выполнены и переданны из корневого узла РИБ
		Параметры.ОбработкаЗавершена = Ложь;
		Возврат;
	КонецЕсли;
	
	НачатьТранзакцию();
	
	#Область БлокировкаДанных
	
	Блокировка = Новый БлокировкаДанных;
	
	ЭлементБлокировки = Блокировка.Добавить("Документ.РеализацияУслугПрочихАктивов");
	ЭлементБлокировки.Режим = РежимБлокировкиДанных.Разделяемый;
	ЭлементБлокировки.ИсточникДанных = Результат[1];
	ЭлементБлокировки.ИспользоватьИзИсточникаДанных("Ссылка", "СсылкаРеализацияУслугПрочихАктивов");
	
	ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.ПервоначальныеСведенияНМАБухгалтерскийУчет.НаборЗаписей");
	ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
	ЭлементБлокировки.ИсточникДанных = Результат[1];
	ЭлементБлокировки.ИспользоватьИзИсточникаДанных("Регистратор", "СсылкаРеализацияУслугПрочихАктивов");
	
	ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.ПервоначальныеСведенияНМАНалоговыйУчет.НаборЗаписей");
	ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
	ЭлементБлокировки.ИсточникДанных = Результат[1];
	ЭлементБлокировки.ИспользоватьИзИсточникаДанных("Регистратор", "СсылкаРеализацияУслугПрочихАктивов");
	
	ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.СостоянияНМАОрганизаций.НаборЗаписей");
	ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
	ЭлементБлокировки.ИсточникДанных = Результат[1];
	ЭлементБлокировки.ИспользоватьИзИсточникаДанных("Регистратор", "СсылкаРеализацияУслугПрочихАктивов");
	
	ЭлементБлокировки = Блокировка.Добавить("РегистрБухгалтерии.Хозрасчетный.НаборЗаписей");
	ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
	ЭлементБлокировки.ИсточникДанных = Результат[1];
	ЭлементБлокировки.ИспользоватьИзИсточникаДанных("Регистратор", "СсылкаРеализацияУслугПрочихАктивов");
	
	Блокировка.Заблокировать();
	
	#КонецОбласти
	
	СтруктураЗаполненияДокумента = Новый Структура("Проведен", Истина);
	
	СтруктураОбходаВыборкиТЧ = Новый Структура("СсылкаРеализацияУслугПрочихАктивов");
	
	Выборка = Результат[1].Выбрать();
	ВыборкаТЧ = Результат[2].Выбрать();
	Пока Выборка.Следующий() Цикл
		
		НаборРеализацияХозрасчетный = РегистрыБухгалтерии.Хозрасчетный.СоздатьНаборЗаписей();
		НаборРеализацияХозрасчетный.Отбор.Регистратор.Установить(Выборка.СсылкаРеализацияУслугПрочихАктивов);
		НаборРеализацияХозрасчетный.Прочитать();
		РеализацияХозрасчетный = НаборРеализацияХозрасчетный.Выгрузить();
		
		НаборРеализацияПервоначальныеСведенияБУ = РегистрыСведений.ПервоначальныеСведенияНМАБухгалтерскийУчет.СоздатьНаборЗаписей();
		НаборРеализацияПервоначальныеСведенияБУ.Отбор.Регистратор.Установить(Выборка.СсылкаРеализацияУслугПрочихАктивов);
		НаборРеализацияПервоначальныеСведенияБУ.Прочитать();
		РеализацияПервоначальныеСведенияБУ = НаборРеализацияПервоначальныеСведенияБУ.Выгрузить();
		
		НаборРеализацияПервоначальныеСведенияНУ = РегистрыСведений.ПервоначальныеСведенияНМАНалоговыйУчет.СоздатьНаборЗаписей();
		НаборРеализацияПервоначальныеСведенияНУ.Отбор.Регистратор.Установить(Выборка.СсылкаРеализацияУслугПрочихАктивов);
		НаборРеализацияПервоначальныеСведенияНУ.Прочитать();
		РеализацияПервоначальныеСведенияНУ = НаборРеализацияПервоначальныеСведенияНУ.Выгрузить();
		
		НаборРеализацияСостояния = РегистрыСведений.СостоянияНМАОрганизаций.СоздатьНаборЗаписей();
		НаборРеализацияСостояния.Отбор.Регистратор.Установить(Выборка.СсылкаРеализацияУслугПрочихАктивов);
		НаборРеализацияСостояния.Прочитать();
		РеализацияСостояния = НаборРеализацияСостояния.Выгрузить();
		
		ВыборкаТЧ.Сбросить();
		ЗаполнитьЗначенияСвойств(СтруктураОбходаВыборкиТЧ, Выборка);
		Пока ВыборкаТЧ.НайтиСледующий(СтруктураОбходаВыборкиТЧ) Цикл
			
			ПодготовкаДокумент = Документы.ПодготовкаКПередачеНМА.СоздатьДокумент();
			ЗаполнитьЗначенияСвойств(ПодготовкаДокумент, Выборка);
			ЗаполнитьЗначенияСвойств(ПодготовкаДокумент, ВыборкаТЧ);
			ЗаполнитьЗначенияСвойств(ПодготовкаДокумент, СтруктураЗаполненияДокумента);
			ПодготовкаДокумент.УстановитьНовыйНомер();
			
			Попытка
				ОбновлениеИнформационнойБазы.ЗаписатьДанные(ПодготовкаДокумент, Истина);
			Исключение
				ОтменитьТранзакцию();
				ВызватьИсключение;
			КонецПопытки;
			
			СтруктураПоиска = Новый Структура(
				"ВидСубконтоКт1, СубконтоКт1",
				ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.НематериальныеАктивы,
				ПодготовкаДокумент.НематериальныйАктив);
			
			ПодготовкаХозрасчетный = РеализацияХозрасчетный.СкопироватьКолонки();
			Для Каждого НайденнаяСтрока Из РеализацияХозрасчетный.НайтиСтроки(СтруктураПоиска) Цикл
				ЗаполнитьЗначенияСвойств(ПодготовкаХозрасчетный.Добавить(), НайденнаяСтрока);
				РеализацияХозрасчетный.Удалить(НайденнаяСтрока);
			КонецЦикла;
			ПодготовкаХозрасчетный.ЗаполнитьЗначения(ПодготовкаДокумент.Ссылка, "Регистратор");
			НаборПодготовкаХозрасчетный = РегистрыБухгалтерии.Хозрасчетный.СоздатьНаборЗаписей();
			НаборПодготовкаХозрасчетный.Отбор.Регистратор.Установить(ПодготовкаДокумент.Ссылка);
			НаборПодготовкаХозрасчетный.Загрузить(ПодготовкаХозрасчетный);
			
			СтруктураПоиска = Новый Структура("НематериальныйАктив", ПодготовкаДокумент.НематериальныйАктив);
			
			ПодготовкаПервоначальныеСведенияБУ = РеализацияПервоначальныеСведенияБУ.СкопироватьКолонки();
			Для Каждого НайденнаяСтрока Из РеализацияПервоначальныеСведенияБУ.НайтиСтроки(СтруктураПоиска) Цикл
				ЗаполнитьЗначенияСвойств(ПодготовкаПервоначальныеСведенияБУ.Добавить(), НайденнаяСтрока);
				РеализацияПервоначальныеСведенияБУ.Удалить(НайденнаяСтрока);
			КонецЦикла;
			ПодготовкаПервоначальныеСведенияБУ.ЗаполнитьЗначения(ПодготовкаДокумент.Ссылка, "Регистратор");
			НаборПодготовкаПервоначальныеСведенияБУ = РегистрыСведений.ПервоначальныеСведенияНМАБухгалтерскийУчет.СоздатьНаборЗаписей();
			НаборПодготовкаПервоначальныеСведенияБУ.Отбор.Регистратор.Установить(ПодготовкаДокумент.Ссылка);
			НаборПодготовкаПервоначальныеСведенияБУ.Загрузить(ПодготовкаПервоначальныеСведенияБУ);
			
			ПодготовкаПервоначальныеСведенияНУ = РеализацияПервоначальныеСведенияНУ.СкопироватьКолонки();
			Для Каждого НайденнаяСтрока Из РеализацияПервоначальныеСведенияНУ.НайтиСтроки(СтруктураПоиска) Цикл
				ЗаполнитьЗначенияСвойств(ПодготовкаПервоначальныеСведенияНУ.Добавить(), НайденнаяСтрока);
				РеализацияПервоначальныеСведенияНУ.Удалить(НайденнаяСтрока);
			КонецЦикла;
			ПодготовкаПервоначальныеСведенияНУ.ЗаполнитьЗначения(ПодготовкаДокумент.Ссылка, "Регистратор");
			НаборПодготовкаПервоначальныеСведенияНУ = РегистрыСведений.ПервоначальныеСведенияНМАНалоговыйУчет.СоздатьНаборЗаписей();
			НаборПодготовкаПервоначальныеСведенияНУ.Отбор.Регистратор.Установить(ПодготовкаДокумент.Ссылка);
			НаборПодготовкаПервоначальныеСведенияНУ.Загрузить(ПодготовкаПервоначальныеСведенияНУ);
			
			ПодготовкаСостояния = РеализацияСостояния.СкопироватьКолонки();
			Для Каждого НайденнаяСтрока Из РеализацияСостояния.НайтиСтроки(СтруктураПоиска) Цикл
				ЗаполнитьЗначенияСвойств(ПодготовкаСостояния.Добавить(), НайденнаяСтрока);
				РеализацияСостояния.Удалить(НайденнаяСтрока);
			КонецЦикла;
			ПодготовкаСостояния.ЗаполнитьЗначения(ПодготовкаДокумент.Ссылка, "Регистратор");
			НаборПодготовкаСостояния = РегистрыСведений.СостоянияНМАОрганизаций.СоздатьНаборЗаписей();
			НаборПодготовкаСостояния.Отбор.Регистратор.Установить(ПодготовкаДокумент.Ссылка);
			НаборПодготовкаСостояния.Загрузить(ПодготовкаСостояния);
			
			Попытка
				ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборПодготовкаХозрасчетный, Истина);
				ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборПодготовкаПервоначальныеСведенияБУ, Истина);
				ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборПодготовкаПервоначальныеСведенияНУ, Истина);
				ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборПодготовкаСостояния, Истина);
			Исключение
				ОтменитьТранзакцию();
				ВызватьИсключение;
			КонецПопытки;
			
		КонецЦикла;
		
		НаборРеализацияХозрасчетный.Загрузить(РеализацияХозрасчетный);
		НаборРеализацияПервоначальныеСведенияБУ.Загрузить(РеализацияПервоначальныеСведенияБУ);
		НаборРеализацияПервоначальныеСведенияНУ.Загрузить(РеализацияПервоначальныеСведенияНУ);
		НаборРеализацияСостояния.Загрузить(РеализацияСостояния);
		
		Попытка
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборРеализацияХозрасчетный, Истина);
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборРеализацияПервоначальныеСведенияБУ, Истина);
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборРеализацияПервоначальныеСведенияНУ, Истина);
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(НаборРеализацияСостояния, Истина);
		Исключение
			ОтменитьТранзакцию();
			ВызватьИсключение;
		КонецПопытки;
		
	КонецЦикла;
	
	ЗафиксироватьТранзакцию();
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли