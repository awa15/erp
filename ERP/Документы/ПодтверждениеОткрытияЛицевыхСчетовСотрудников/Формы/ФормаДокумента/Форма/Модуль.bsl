﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	УстановитьДоступностьЭлементов();
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.Печать
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Параметры.Ключ.Пустая() Тогда
		ПодключитьОбработчикОжидания("ЗагрузитьПодтвержденияБанка", 0.1, Истина);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДате(ЭтаФорма, "Объект.МесяцОткрытия", "МесяцОткрытияСтрокой");
КонецПроцедуры

#Область РедактированиеМесяцаСтрокой

&НаКлиенте
Процедура МесяцОткрытияПриИзменении(Элемент)
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(ЭтаФорма, "Объект.МесяцОткрытия", "МесяцОткрытияСтрокой", Модифицированность);
КонецПроцедуры

&НаКлиенте
Процедура МесяцОткрытияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтаФорма, ЭтаФорма, "Объект.МесяцОткрытия", "МесяцОткрытияСтрокой");
КонецПроцедуры

&НаКлиенте
Процедура МесяцОткрытияРегулирование(Элемент, Направление, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(ЭтаФорма, "Объект.МесяцОткрытия", "МесяцОткрытияСтрокой", Направление, Модифицированность);
КонецПроцедуры

&НаКлиенте
Процедура МесяцОткрытияАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура МесяцОткрытияОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийТаблицыФормыИмяТаблицыФормы

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура УстановитьДоступностьЭлементов()
	
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "Организация", "ТолькоПросмотр", Истина);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "Подразделение", "ТолькоПросмотр", Истина);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ПервичныйДокумент", "ТолькоПросмотр", Истина);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ЗарплатныйПроект", "ТолькоПросмотр", Истина);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ДатаНомер", "ТолькоПросмотр", Истина);
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "Сотрудники", "ТолькоПросмотр", Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьПодтвержденияБанка()
	
	ПараметрыЗагрузки = ОбменСБанкамиПоЗарплатнымПроектамКлиент.ПараметрыЗагрузкиФайловИзБанка();
	ПараметрыЗагрузки.МножественныйВыбор = Истина;
	ПараметрыЗагрузки.ОповещениеЗавершения = Новый ОписаниеОповещения("ЗагрузитьПодтвержденияБанкаЗавершение", ЭтотОбъект);
	
	ОбменСБанкамиПоЗарплатнымПроектамКлиент.ЗагрузитьФайлыИзБанка(ПараметрыЗагрузки);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьПодтвержденияБанкаЗавершение(Результат, ПараметрыЗагрузки) Экспорт
	
	ПомещенныеФайлы = Результат.ПомещенныеФайлы;
	
	Если ПомещенныеФайлы.Количество() = 0 Тогда
		Закрыть();
		Возврат;
	КонецЕсли;
	
	СообщениеОбОшибке = Неопределено;
	СозданныеДокументы = ЗагрузитьПодтвержденияИзБанкаНаСервере(ПомещенныеФайлы, СообщениеОбОшибке);
	Если СозданныеДокументы.Количество() = 0 Тогда
		ПоказатьПредупреждение(, СообщениеОбОшибке);
		Закрыть();
		Возврат;
	КонецЕсли;
		
	СоответствиеТипов = Новый Соответствие;
	ПриЗагрузкеБылиОшибки = Ложь;
	Для каждого СозданныйДокумент Из СозданныеДокументы Цикл
		
		Если СозданныйДокумент.Ключ = Неопределено Тогда
			ПриЗагрузкеБылиОшибки = Истина;
			Продолжить;
		КонецЕсли;
		
		СоответствиеТипов.Вставить(ТипЗнч(СозданныйДокумент.Ключ));
		СоответствиеТипов.Вставить(СозданныйДокумент.Значение);
		
	КонецЦикла;
	
	Для каждого ТипДокумента Из СоответствиеТипов Цикл
		ОповеститьОбИзменении(ТипДокумента.Ключ);
		Если ТипДокумента.Ключ = Тип("ДокументСсылка.ПодтверждениеОткрытияЛицевыхСчетовСотрудников") Тогда
			Оповестить("ЗагруженоПодтверждениеОткрытияЛицевыхСчетов");
		ИначеЕсли ТипДокумента.Ключ = Тип("ДокументСсылка.ПодтверждениеЗачисленияЗарплаты") Тогда
			Оповестить("ЗагруженоПодтверждениеЗачисленияЗарплаты");
		КонецЕсли;
	КонецЦикла;
	
	Если ПриЗагрузкеБылиОшибки Тогда
		Состояние(НСтр("ru = 'При загрузке файлов подтверждений банка были ошибки'"));
	Иначе
		Состояние(НСтр("ru = 'Все файлы успешно загружены'"));
	КонецЕсли;
		
КонецПроцедуры

&НаСервере
Функция ЗагрузитьПодтвержденияИзБанкаНаСервере(МассивИменФайлов, СообщениеОбОшибке)
	
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	ДокументыПодтверждения = ОбменСБанкамиПоЗарплатнымПроектам.ЗагрузитьПодтвержденияИзБанка(МассивИменФайлов, ДокументОбъект);
	ЗначениеВРеквизитФормы(ДокументОбъект, "Объект");
	ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДате(ЭтаФорма, "Объект.МесяцОткрытия", "МесяцОткрытияСтрокой");
	
	Если ДокументыПодтверждения.Количество() = 0 Тогда
		СообщенияПользователю = ПолучитьСообщенияПользователю(Истина);
		Если СообщенияПользователю.Количество() > 0 Тогда
			СообщениеОбОшибке = СообщенияПользователю.Получить(0).Текст;
		Иначе
			СообщениеОбОшибке = НСтр("ru = 'Неверный формат файла.'");
		КонецЕсли;
	КонецЕсли;
	
	Возврат ДокументыПодтверждения;
	
КонецФункции

#КонецОбласти
