﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Обработчик подсистемы "Дополнительные отчеты и обработки".
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Конец Обработчик подсистемы "Дополнительные отчеты и обработки".
	
	// Обработчик подсистемы "ВерсионированиеОбъектов".
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	// Конец Обработчик подсистемы "ВерсионированиеОбъектов".

	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.Печать
	
	Если Параметры.Ключ.Пустая() Тогда
		
		ЗаполнитьДанныеФормыПоОрганизации();
		
	КонецЕсли; 
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	СохраняемыеЗначения = Новый Структура;
	СохраняемыеЗначения.Вставить("Исполнитель", ТекущийОбъект.Исполнитель);
	СохраняемыеЗначения.Вставить("ДолжностьИсполнителя", ТекущийОбъект.ДолжностьИсполнителя);
	
	ЗарплатаКадры.СохранитьЗначенияЗаполненияОтветственныхРаботников(ТекущийОбъект.Организация, СохраняемыеЗначения);
		
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ОрганизацияПриИзмененииНаСервере();
	
КонецПроцедуры

#Область ОбработчикиСобытийТаблицыФормыНачисления

&НаКлиенте
Процедура НачисленияАкционерПриИзменении(Элемент)
	РассчитатьНДФЛПоТекущейСтроке();
КонецПроцедуры

&НаКлиенте
Процедура НачисленияНачисленоПриИзменении(Элемент)
	РассчитатьНДФЛПоТекущейСтроке();
КонецПроцедуры

&НаКлиенте
Процедура НачисленияВычетПоНДФЛПриИзменении(Элемент)
	РассчитатьНДФЛПоТекущейСтроке();
КонецПроцедуры

&НаКлиенте
Процедура НачисленияОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	Если ТипЗнч(ВыбранноеЗначение) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
		ДобавитьНовуюСтроку(ВыбранноеЗначение);
	Иначе
		Для Каждого ФизическоеЛицо Из ВыбранноеЗначение Цикл
			ДобавитьНовуюСтроку(ФизическоеЛицо);
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаКлиенте
Процедура ПодборАкционеров(Команда)
	
	ФизическиеЛицаЗарплатаКадрыРасширенныйКлиент.ОткрытьФормуПодбораФизическихЛицПоРоли(
		Элементы.Начисления,
		Объект.Организация,
		ПредопределенноеЗначение("Перечисление.РолиФизическихЛиц.Акционер"),
		АдресСпискаПодобранныхФизическихЛиц());
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервереБезКонтекста
Функция РассчитатьНДФЛАкционераНаСервере(ДатаПолученияДохода, Акционер, Начислено, ВычетПоНДФЛ)
	Возврат УчетНДФЛРасширенный.НалогСДивидендовАкционера(ДатаПолученияДохода, Акционер, Начислено, ВычетПоНДФЛ);
КонецФункции

&НаКлиенте
Процедура РассчитатьНДФЛПоТекущейСтроке();
	
	Акционер = Элементы.Начисления.ТекущиеДанные.Акционер;
	Начислено = Элементы.Начисления.ТекущиеДанные.Начислено;
	ВычетПоНДФЛ = Элементы.Начисления.ТекущиеДанные.ВычетПоНДФЛ;
	Если ЗначениеЗаполнено(Акционер) И 
		ЗначениеЗаполнено(Начислено) Тогда
		НДФЛ = РассчитатьНДФЛАкционераНаСервере(Объект.ДатаВыплаты, Акционер, Начислено, ВычетПоНДФЛ);
		Элементы.Начисления.ТекущиеДанные.НДФЛ = НДФЛ;
		Элементы.Начисления.ТекущиеДанные.КВыплате = Элементы.Начисления.ТекущиеДанные.Начислено - Элементы.Начисления.ТекущиеДанные.НДФЛ;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция АдресСпискаПодобранныхФизическихЛиц()
	
	Возврат ПоместитьВоВременноеХранилище(Объект.Начисления.Выгрузить(,"Акционер").ВыгрузитьКолонку("Акционер"), УникальныйИдентификатор);
	
КонецФункции

&НаКлиенте
Процедура ДобавитьНовуюСтроку(ФизическоеЛицо)
	
	СтрокиНачислений = Объект.Начисления.НайтиСтроки(Новый Структура("Акционер", ФизическоеЛицо));
	
	Если СтрокиНачислений.Количество() = 0 Тогда
		НоваяСтрока = Объект.Начисления.Добавить();
		НоваяСтрока.Акционер = ФизическоеЛицо;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	
	ЗаполнитьДанныеФормыПоОрганизации();
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДанныеФормыПоОрганизации()
	
	Если НЕ ЗначениеЗаполнено(Объект.Организация) Тогда
		Возврат;
	КонецЕсли; 
	
	ЗапрашиваемыеЗначения = Новый Структура;
	ЗапрашиваемыеЗначения.Вставить("Организация", "Объект.Организация");
	
	ЗапрашиваемыеЗначения.Вставить("Исполнитель", "Объект.Исполнитель");
	ЗапрашиваемыеЗначения.Вставить("ДолжностьИсполнителя", "Объект.ДолжностьИсполнителя");
	
	ЗарплатаКадры.ЗаполнитьЗначенияВФорме(ЭтаФорма, ЗапрашиваемыеЗначения, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("Организация"));	
	
КонецПроцедуры

#КонецОбласти
