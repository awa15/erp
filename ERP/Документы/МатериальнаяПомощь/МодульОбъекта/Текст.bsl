﻿#Если Не ТолстыйКлиентУправляемоеПриложение Или Сервер Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИФизическимЛицам(ЭтотОбъект, Таблица, "Организация", "ФизическиеЛица.ФизическоеЛицо");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	ИсправлениеДокументовЗарплатаКадры.ПроверитьЗаполнение(ЭтотОбъект, ПроверяемыеРеквизиты, Отказ, , "ПериодРегистрации");
	
	МассивНачисленийДокумента = Новый Массив;
	МассивНачисленийДокумента.Добавить(ВидРасчета);
	ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивНачисленийДокумента, НачисленияПерерасчет.ВыгрузитьКолонку("Начисление"), Истина);
	
	Если Не УчетНДФЛРасширенный.ДатаВыплатыОбязательнаКЗаполнению(ПорядокВыплаты, МассивНачисленийДокумента) Тогда
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ПланируемаяДатаВыплаты");
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	ДанныеДляПроведения = ДанныеДляПроведения();
	
	МесяцНачисления	= ПериодРегистрации;
	
	ДатаОперации	= УчетНДФЛРасширенный.ДатаОперацииПоДокументу(Дата, ПериодРегистрации);
	МесяцНачисления	= ПериодРегистрации;
		
	// Начисления
	РасчетЗарплатыРасширенный.СформироватьДвиженияНачислений(
		Движения, Отказ, Организация, МесяцНачисления, ДанныеДляПроведения.Начисления, ДанныеДляПроведения.ПоказателиНачислений);
		
	// Удержания
	РасчетЗарплатыРасширенный.СформироватьДвиженияУдержаний(Движения, Отказ, Организация, ДатаОперации, ДанныеДляПроведения.Удержания, ДанныеДляПроведения.ПоказателиУдержаний);
	ИсполнительныеЛисты.СформироватьУдержанияПоИсполнительнымДокументам(Движения, ДанныеДляПроведения.УдержанияПоИсполнительнымДокументам);
		
	// НДФЛ
	УчетНДФЛРасширенный.ЗарегистрироватьДоходыИСуммыНДФЛПоВременнойТаблицеНачислений(
		Ссылка, Движения, Отказ, Организация, Дата, ПериодРегистрации, ПорядокВыплаты, ПланируемаяДатаВыплаты, ДанныеДляПроведения, Истина);
		
	// Учет начисленной зарплаты
	УчетНачисленнойЗарплаты.ЗарегистрироватьНачисленияУдержания(
		Движения, Отказ, Организация, МесяцНачисления, ДанныеДляПроведения.НачисленияПоСотрудникам, ДанныеДляПроведения.УдержанияПоСотрудникам, Неопределено, Неопределено, ПорядокВыплаты);
			
	// - Регистрация начислений и удержаний.
	ОтражениеЗарплатыВБухучетеРасширенный.СформироватьДвиженияБухучетНачисленияУдержанияПоСотрудникам(
				Движения, Отказ, Организация, ПериодРегистрации,
				ДанныеДляПроведения.НачисленияПоСотрудникам,
				ДанныеДляПроведения.УдержанияПоСотрудникам,
				ДанныеДляПроведения.НДФЛПоСотрудникам,
				РасчетЗарплатыРасширенный.ЭтоМежрасчетнаяВыплата(ПорядокВыплаты));
			
	// Страховые взносы
	ОтражениеЗарплатыВБухучете.ДополнитьНачисленияДаннымиОЕНВД(Организация, МесяцНачисления, ДанныеДляПроведения.МенеджерВременныхТаблиц, , ДанныеДляПроведения.НачисленияПоСотрудникам);
	УчетСтраховыхВзносов.СформироватьСведенияОДоходахСтраховыеВзносы(Движения, Отказ, Организация, МесяцНачисления, ДанныеДляПроведения.МенеджерВременныхТаблиц, Ложь, Истина, Ссылка);
		
	// Учет среднего заработка
	УчетСреднегоЗаработка.ЗарегистрироватьДанныеСреднегоЗаработка(Движения, Отказ, ДанныеДляПроведения.НачисленияДляСреднегоЗаработка);
	
	Если ОбщегоНазначенияКлиентСервер.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба.РасчетДенежногоСодержания") Тогда
		Модуль = ОбщегоНазначенияКлиентСервер.ОбщийМодуль("РасчетДенежногоСодержания");
		Модуль.ЗарегистрироватьНачисленияДляРасчетаСохраняемогоДенежногоСодержания(Движения, Отказ, МесяцНачисления, ДанныеДляПроведения.НачисленияДляРегистрацииДенежногоСодержания);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	ДанныеДляБухучета = Документы.МатериальнаяПомощь.ДанныеДляБухучетаЗарплатыПервичныхДокументов(ЭтотОбъект);
	ОтражениеЗарплатыВБухучетеРасширенный.ЗарегистрироватьБухучетЗарплатыПервичныхДокументов(ДанныеДляБухучета);
	
	УстановитьПривилегированныйРежим(Ложь);

КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		Если ДанныеЗаполнения.Свойство("Действие") И ДанныеЗаполнения.Действие = "Исправить" Тогда
			ИсправлениеДокументовЗарплатаКадры.СкопироватьДокумент(ЭтотОбъект, ДанныеЗаполнения.Ссылка);
			ИсправленныйДокумент = ДанныеЗаполнения.Ссылка;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ДанныеДляПроведения()
	
	ДанныеДляПроведения = РасчетЗарплаты.СоздатьДанныеДляПроведенияНачисленияЗарплаты();
	
	РасчетЗарплатыРасширенный.ЗаполнитьНачисления(
		ДанныеДляПроведения, Ссылка, "Начисления,НачисленияПерерасчет", "Ссылка.ПериодРегистрации", "Ссылка.ВидРасчета");
	
	ДанныеДляПроведения.НачисленияПоСотрудникам.Колонки.Добавить("ДокументОснование");
	ДанныеДляПроведения.НачисленияПоСотрудникам.ЗаполнитьЗначения(Ссылка, "ДокументОснование");
	
	РасчетЗарплатыРасширенный.ЗаполнитьСписокФизическихЛиц(ДанныеДляПроведения, Ссылка, "Начисления");
	РасчетЗарплаты.ЗаполнитьУдержания(ДанныеДляПроведения, Ссылка);
	РасчетЗарплаты.ЗаполнитьДанныеНДФЛ(ДанныеДляПроведения, Ссылка);
	
	УчетСреднегоЗаработка.ЗаполнитьТаблицыДляРегистрацииДанныхСреднегоЗаработка(ДанныеДляПроведения, Ссылка, ПериодРегистрации, "Начисления,НачисленияПерерасчет", "Ссылка.ПериодРегистрации", "Ссылка.ВидРасчета");
	
	Если ОбщегоНазначенияКлиентСервер.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба.РасчетДенежногоСодержания") Тогда
		Модуль = ОбщегоНазначенияКлиентСервер.ОбщийМодуль("РасчетДенежногоСодержания");
		НачисленияДляРегистрацииДенежногоСодержания = Модуль.СведенияОНачисленияхДляРегистрацииДенежногоСодержанияДокумента(Ссылка, "Начисления,НачисленияПерерасчет", "Ссылка.ВидРасчета");
		ДанныеДляПроведения.Вставить("НачисленияДляРегистрацииДенежногоСодержания", НачисленияДляРегистрацииДенежногоСодержания);
	КонецЕсли;
	
	Возврат ДанныеДляПроведения;
	
КонецФункции

#КонецОбласти

#КонецЕсли
