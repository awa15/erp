﻿#Область СлужебныйПрограммныйИнтерфейс

#Область СохранениеПолучениеРазмераАвансаПоУмолчанию

// Возвращает размер аванса в процентах по умолчанию, сохраненный в настройках пользователя.
//
// Параметры:
//		Организация			- СправочникСсылка.Организации
//
// Возвращаемое значение:
//		Число
//
Функция РазмерАвансаВПроцентахПоУмолчанию(Организация) Экспорт
	
	Возврат РазмерАвансаПоУмолчанию(Организация, Перечисления.СпособыРасчетаАванса.ПроцентомОтТарифа);
	
КонецФункции

// Возвращает размер аванса по умолчанию, сохраненный в настройках пользователя.
//
// Параметры:
//		Организация			- СправочникСсылка.Организации
//		СпособРасчетаАванса	- ПеречислениеСсылка.СпособыРасчетаАванса
//
// Возвращаемое значение:
//		Число
//
Функция РазмерАвансаПоУмолчанию(Организация, СпособРасчетаАванса) Экспорт
	
	РазмерАванса = 0;
	
	Если СпособРасчетаАванса = Перечисления.СпособыРасчетаАванса.ПроцентомОтТарифа Тогда
		
		РазмерАванса = Неопределено;
			
		СохраненнаяНастройка = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(
			"СпособРасчетаАванса", "ПроцентомОтТарифа");
			
		Если СохраненнаяНастройка <> Неопределено Тогда
			
			СохраненныйРазмерАванса = СохраненнаяНастройка.Получить(Организация);
			Если СохраненныйРазмерАванса <> Неопределено Тогда
				РазмерАванса = СохраненныйРазмерАванса;
			КонецЕсли; 
			
		КонецЕсли;
		
		Если РазмерАванса = Неопределено Тогда
			РазмерАванса = 40;
		КонецЕсли; 
		
	КонецЕсли; 
	
	Возврат РазмерАванса;
	
КонецФункции

// Сохраняет в настройках пользователя размер аванса в процентах.
//
// Параметры:
//		РазмерАванса		- Число
//		Организация			- СправочникСсылка.Организации
//		СпособРасчетаАванса	- ПеречислениеСсылка.СпособыРасчетаАванса
//
Процедура ЗапомнитьРазмерАвансаПоУмолчанию(РазмерАванса, Организация, СпособРасчетаАванса) Экспорт
	
	Если СпособРасчетаАванса <> Перечисления.СпособыРасчетаАванса.ПроцентомОтТарифа Тогда
		Возврат;
	КонецЕсли; 
	
	НастройкаРазмераАванса = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(
		"СпособРасчетаАванса", "ПроцентомОтТарифа");
		
	Если НастройкаРазмераАванса = Неопределено Тогда
		НастройкаРазмераАванса = Новый Соответствие;
	КонецЕсли;
	
	НастройкаРазмераАванса.Вставить(Организация, РазмерАванса);
	
	ОбщегоНазначения.ХранилищеОбщихНастроекСохранить("СпособРасчетаАванса", "ПроцентомОтТарифа", НастройкаРазмераАванса);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
