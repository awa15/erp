﻿
#Область СлужебныеПроцедурыИФункции

// Серверный обработчик события ОбработкаПолученияДанныхВыбора справочника СерииНоменклатуры.
//
Процедура СерииНоменклатурыОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
	Если Параметры.Свойство("ВидНоменклатуры") Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 50
		|	СерииНоменклатуры.Ссылка КАК Серия,
		|	СерииНоменклатуры.Наименование,
		|	СерииНоменклатуры.ГоденДо КАК ГоденДо,
		|	СерииНоменклатуры.Номер КАК Номер
		|ИЗ
		|	Справочник.СерииНоменклатуры КАК СерииНоменклатуры
		|ГДЕ
		|	СерииНоменклатуры.ВидНоменклатуры = &ВидНоменклатуры
		|	И ВЫБОР
		|			КОГДА СерииНоменклатуры.ВидНоменклатуры.ИспользоватьНомерСерии
		|				ТОГДА &НомерНеУказан
		|						ИЛИ СерииНоменклатуры.Номер ПОДОБНО &Номер
		|			ИНАЧЕ ИСТИНА
		|		КОНЕЦ
		|	И ВЫБОР
		|			КОГДА СерииНоменклатуры.ВидНоменклатуры.ИспользоватьСрокГодностиСерии
		|				ТОГДА &ГоденДоНеУказан
		|						ИЛИ СерииНоменклатуры.ГоденДо = &ГоденДо
		|			ИНАЧЕ ИСТИНА
		|		КОНЕЦ
		|
		|УПОРЯДОЧИТЬ ПО
		|	Номер,
		|	ГоденДо";
		
		Запрос.УстановитьПараметр("ВидНоменклатуры", Параметры.ВидНоменклатуры);
		Запрос.УстановитьПараметр("ГоденДоНеУказан", Не ЗначениеЗаполнено(Параметры.ГоденДо));
		Запрос.УстановитьПараметр("ГоденДо",		 Параметры.ГоденДо);
		Запрос.УстановитьПараметр("НомерНеУказан",   Не ЗначениеЗаполнено(Параметры.Номер));
		Запрос.УстановитьПараметр("Номер", 			 "%" + СокрЛП(Параметры.Номер) + "%");
		
		ДанныеВыбора = Новый СписокЗначений;
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			
			Значение = Новый Структура("Серия,Номер,ГоденДо");
			ЗаполнитьЗначенияСвойств(Значение, Выборка);
			
			ДанныеВыбора.Добавить(Значение, Выборка.Наименование);
			
		КонецЦикла;
		
	ИначеЕсли Параметры.Свойство("Номенклатура") 
		И ЗначениеЗаполнено(Параметры.Номенклатура) Тогда 	
		
		СтрокаПоиска = Параметры.СтрокаПоиска;
		
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ ПЕРВЫЕ 50
		|	СпрСерии.Ссылка КАК Серия,
		|	СпрСерии.Наименование КАК СерияПредставление
		|ИЗ
		|	Справочник.Номенклатура КАК СпрНоменклатура
		|ВНУТРЕННЕЕ СОЕДИНЕНИЕ
		|	Справочник.СерииНоменклатуры КАК СпрСерии
		|ПО
		|	СпрСерии.ВидНоменклатуры = СпрНоменклатура.ВидНоменклатуры 
		|ГДЕ
		|	СпрНоменклатура.Ссылка = &Номенклатура
		|	" + ?(СтрокаПоиска = Неопределено, "", "И СпрСерии.Наименование ПОДОБНО &СтрокаПоиска") + "
		|УПОРЯДОЧИТЬ ПО
		|	СерияПредставление";
		
		Запрос.УстановитьПараметр("Номенклатура", Параметры.Номенклатура);
		
		Если СтрокаПоиска <> Неопределено Тогда
			Запрос.УстановитьПараметр("СтрокаПоиска", "%" + СокрЛП(СтрокаПоиска) + "%");
		КонецЕсли;
		
		ДанныеВыбора = Новый СписокЗначений;
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			ДанныеВыбора.Добавить(Выборка.Серия, Выборка.СерияПредставление);
		КонецЦикла;
		
	ИначеЕсли Параметры.СтрокаПоиска <> Неопределено Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ ПЕРВЫЕ 50
		|	СерииНоменклатуры.Ссылка КАК Серия,
		|	СерииНоменклатуры.Номер КАК СерияНомер,
		|	ЕСТЬNULL(ДвиженияСерийТоваров.Номенклатура, НЕОПРЕДЕЛЕНО) КАК Номенклатура,
		|	ЕСТЬNULL(ДвиженияСерийТоваров.Номенклатура.Представление, """") КАК НоменклатураПредставление,
		|	ЕСТЬNULL(ДвиженияСерийТоваров.Характеристика, НЕОПРЕДЕЛЕНО) КАК Характеристика,
		|	ЕСТЬNULL(ДвиженияСерийТоваров.Характеристика.Представление, """") КАК ХарактеристикаПредставление
		|ИЗ
		|	Справочник.СерииНоменклатуры КАК СерииНоменклатуры
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.ДвиженияСерийТоваров КАК ДвиженияСерийТоваров
		|		ПО (ДвиженияСерийТоваров.Серия = СерииНоменклатуры.Ссылка)
		|ГДЕ
		|	СерииНоменклатуры.Наименование ПОДОБНО &СтрокаПоиска
		|
		|УПОРЯДОЧИТЬ ПО
		|	СерияНомер,
		|	НоменклатураПредставление";
		
		Запрос.УстановитьПараметр("СтрокаПоиска", "%" + СокрЛП(Параметры.СтрокаПоиска) + "%");
		
		Результат = Запрос.Выполнить();
		Выборка = Результат.Выбрать();
		
		ДанныеВыбора = Новый СписокЗначений;
		
		Пока Выборка.Следующий() Цикл
			
			Если ЗначениеЗаполнено(Выборка.Номенклатура) Тогда
				
				ПредставлениеНоменклатуры = НоменклатураКлиентСервер.ПредставлениеНоменклатуры(
													Выборка.НоменклатураПредставление,
													Выборка.ХарактеристикаПредставление);
													
				ПредставлениеЗначения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
													НСтр("ru = '%1 (%2)'"),
													Выборка.СерияНомер,
													ПредставлениеНоменклатуры);
			Иначе
				
				ПредставлениеЗначения = Выборка.СерияНомер;
				
			КонецЕсли; 
											
			ДанныеВыбора.Добавить(Выборка.Серия, ПредставлениеЗначения);
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

// Серверный обработчик события ОбработкаПолученияДанныхВыбора справочника ПолитикиУчетаСерий.
//
Процедура ПолитикиУчетаСерийОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
	ИспользоватьСерии = Ложь;
	
	Если Не Параметры.Свойство("ИспользоватьСерии", ИспользоватьСерии) Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ИспользоватьСерии Тогда
		Возврат;
	КонецЕсли;
		
	СтрокаПоиска = Параметры.СтрокаПоиска;
	
	МассивДопустимыхТиповПолитик = НоменклатураКлиентСервер.ПолучитьМассивДопустимыхТиповПолитикУчетаСерий(Параметры);
	
	Если ТипЗнч(Параметры.Склад) = Тип("СправочникСсылка.Склады") Тогда
		
		ТекстЗапроса =
		"ВЫБРАТЬ ПЕРВЫЕ 50
		|	ПолитикиУчетаСерий.Наименование КАК Наименование,
		|	ПолитикиУчетаСерий.Ссылка КАК ПолитикаУчетаСерий
		|ИЗ
		|	Справочник.ПолитикиУчетаСерий КАК ПолитикиУчетаСерий
		|ГДЕ
		|	ПолитикиУчетаСерий.ТипПолитики В(&ТипыПолитик)
		|	И ПолитикиУчетаСерий.ДляСклада
		|	И (ВЫРАЗИТЬ(&Склад КАК Справочник.Склады).ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач
		|		ИЛИ НЕ ПолитикиУчетаСерий.УказыватьПриОтраженииИзлишков
		|			И НЕ ПолитикиУчетаСерий.УказыватьПриОтраженииНедостач)
		|	" + ?(СтрокаПоиска = Неопределено, "", "И ПолитикиУчетаСерий.Наименование ПОДОБНО &СтрокаПоиска") + "
		|
		|УПОРЯДОЧИТЬ ПО
		|	Наименование";
		
	Иначе
		
		ТекстЗапроса =
		"ВЫБРАТЬ ПЕРВЫЕ 50
		|	ПолитикиУчетаСерий.Наименование КАК Наименование,
		|	ПолитикиУчетаСерий.Ссылка КАК ПолитикаУчетаСерий
		|ИЗ
		|	Справочник.ПолитикиУчетаСерий КАК ПолитикиУчетаСерий
		|ГДЕ
		|	ПолитикиУчетаСерий.ТипПолитики В(&ТипыПолитик)
		|	И ПолитикиУчетаСерий.ДляПроизводства
		|	" + ?(СтрокаПоиска = Неопределено, "", "И ПолитикиУчетаСерий.Наименование ПОДОБНО &СтрокаПоиска") + "
		|
		|УПОРЯДОЧИТЬ ПО
		|	Наименование";
		
	КонецЕсли; 
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапроса;
	
	Запрос.УстановитьПараметр("ТипыПолитик", МассивДопустимыхТиповПолитик);
	Запрос.УстановитьПараметр("Склад", Параметры.Склад);
	Если СтрокаПоиска <> Неопределено Тогда
		Запрос.УстановитьПараметр("СтрокаПоиска", СтрокаПоиска + "%");
	КонецЕсли;
	
	ДанныеВыбора = Новый СписокЗначений;

	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		ДанныеВыбора.Добавить(Выборка.ПолитикаУчетаСерий, Выборка.Наименование);
	КонецЦикла;	
	
КонецПроцедуры

// Серверный обработчик события ОбработкаПолученияДанныхВыбора справочника УпаковкиНоменклатуры.
//
Процедура УпаковкиНоменклатурыОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
 	ДанныеВыбора = Новый СписокЗначений;

	Если Параметры.Свойство("ВыборРодителя") Тогда
		
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ
		|	УпаковкиНоменклатуры.Ссылка КАК Упаковка,
		|	ПРЕДСТАВЛЕНИЕ(УпаковкиНоменклатуры.Ссылка) КАК УпаковкаПредставление
		|ИЗ
		|	Справочник.УпаковкиНоменклатуры КАК УпаковкиНоменклатуры
		|ГДЕ
		|	УпаковкиНоменклатуры.Ссылка <> &Ссылка
		|	И УпаковкиНоменклатуры.Владелец = &Владелец
		|
		|УПОРЯДОЧИТЬ ПО
		|	Наименование";
		
		Запрос.УстановитьПараметр("Владелец", Параметры.Владелец);
		Запрос.УстановитьПараметр("Ссылка", Параметры.Ссылка);
		
		Выборка = Запрос.Выполнить().Выбрать();
		
		Пока Выборка.Следующий() Цикл
			ДанныеВыбора.Добавить(Выборка.Упаковка, Выборка.УпаковкаПредставление);
		КонецЦикла;
		
	Иначе
		
		Номенклатура = Неопределено;
		
		Если Не Параметры.Свойство("Номенклатура", Номенклатура) Тогда
			Возврат;
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Номенклатура) Тогда
			Возврат;
		КонецЕсли;
		
		Если Параметры.Свойство("ДобавлятьПустуюУпаковку") Тогда
			ДобавлятьПустуюУпаковку = Параметры.ДобавлятьПустуюУпаковку;	
		ИначеЕсли Параметры.Свойство("Склад")
			И ЗначениеЗаполнено(Параметры.Склад) Тогда
			ДобавлятьПустуюУпаковку = Не СкладыСервер.ИспользоватьАдресноеХранение(Параметры.Склад,Параметры.Помещение);		
		Иначе
			ДобавлятьПустуюУпаковку = Истина;
		КонецЕсли;
		
		ПолучитьСписокДляВыбораУпаковок(Номенклатура, ДанныеВыбора, Параметры.СтрокаПоиска, ДобавлятьПустуюУпаковку);
	КонецЕсли;	
	
КонецПроцедуры

// Серверный обработчик события ОбработкаПолученияДанныхВыбора справочника ХарактеристикиНоменклатуры.
//
Процедура ХарактеристикиНоменклатурыОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	Перем Номенклатура;
	
	Если Не Параметры.Свойство("Номенклатура", Номенклатура) Тогда
		Возврат;
	КонецЕсли;
	
	СтандартнаяОбработка = Ложь;	
	
	СтрокаПоиска = Параметры.СтрокаПоиска;
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ ПЕРВЫЕ 50
	|	СпрХарактеристики.Ссылка КАК Характеристика,
	|	СпрХарактеристики.Наименование КАК ХарактеристикаПредставление
	|ИЗ
	|	Справочник.Номенклатура КАК СпрНоменклатура
	|ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|	Справочник.ХарактеристикиНоменклатуры КАК СпрХарактеристики
	|ПО
	|	СпрХарактеристики.Владелец = ВЫБОР КОГДА СпрНоменклатура.ИспользованиеХарактеристик = ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ОбщиеДляВидаНоменклатуры) ТОГДА
	|			СпрНоменклатура.ВидНоменклатуры
	|		КОГДА СпрНоменклатура.ИспользованиеХарактеристик = ЗНАЧЕНИЕ(Перечисление.ВариантыИспользованияХарактеристикНоменклатуры.ИндивидуальныеДляНоменклатуры) ТОГДА
	|			СпрНоменклатура.Ссылка
	|		ИНАЧЕ
	|			ЛОЖЬ
	|		КОНЕЦ
	|ГДЕ
	|	СпрНоменклатура.Ссылка = &Номенклатура
	|	" + ?(СтрокаПоиска = Неопределено, "", "И СпрХарактеристики.Наименование ПОДОБНО &СтрокаПоиска") + "
	|УПОРЯДОЧИТЬ ПО
	|	ХарактеристикаПредставление
	|");

	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
    	
	Если СтрокаПоиска <> Неопределено Тогда
		Запрос.УстановитьПараметр("СтрокаПоиска", СтрокаПоиска + "%");
	КонецЕсли;
	
	ДанныеВыбора = Новый СписокЗначений;

	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		ДанныеВыбора.Добавить(Выборка.Характеристика, Выборка.ХарактеристикаПредставление);
	КонецЦикла;	
	
КонецПроцедуры

// Серверный обработчик события ОбработкаПолученияДанныхВыбора справочника Номенклатура.
//
Процедура НоменклатураОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
	// Получим структуру возможных полей отбора справочника номенклатуры
	СтруктураРеквизитов = ЗначениеНастроекПовтИсп.РеквизитыСправочника("Номенклатура");
	
	Запрос        = Новый Запрос;
	СтрокаПоиска  = Параметры.СтрокаПоиска;
	УсловиеОтбора = "";
	
	Для Каждого КлючИЗначениеОтбора Из Параметры.Отбор Цикл
		Если СтруктураРеквизитов.Свойство(КлючИЗначениеОтбора.Ключ) Тогда
			УсловиеОтбора = УсловиеОтбора + "
				|	И СпрНоменклатура." + КлючИЗначениеОтбора.Ключ + " В (&" + КлючИЗначениеОтбора.Ключ + ")";
			Запрос.УстановитьПараметр(КлючИЗначениеОтбора.Ключ,КлючИЗначениеОтбора.Значение);
		КонецЕсли;
	КонецЦикла;
	
	Если НЕ Параметры.Отбор.Свойство("ЭтоГруппа") И Параметры.Свойство("ВыборГруппИЭлементов") Тогда
		Если Параметры.ВыборГруппИЭлементов = ИспользованиеГруппИЭлементов.Группы Тогда
			УсловиеОтбора = УсловиеОтбора + "
				|	И СпрНоменклатура.ЭтоГруппа";
		ИначеЕсли Параметры.ВыборГруппИЭлементов = ИспользованиеГруппИЭлементов.Элементы Тогда
			УсловиеОтбора = УсловиеОтбора + "
				|	И НЕ СпрНоменклатура.ЭтоГруппа";
		КонецЕсли;
	КонецЕсли;
	
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 50
	|	СпрНоменклатура.Ссылка,
	|	СпрНоменклатура.Наименование КАК ПредставлениеСовпадения,
	|	СпрНоменклатура.Качество.Порядок КАК Качество,
	|	0 КАК Порядок,
	|	СпрНоменклатура.Код КАК ПредставлениеНоменклатуры,
	|	СпрНоменклатура.ЭтоГруппа
	|ПОМЕСТИТЬ НоменклатураПоиск
	|ИЗ
	|	Справочник.Номенклатура КАК СпрНоменклатура
	|ГДЕ
	|	СпрНоменклатура.Наименование ПОДОБНО &СтрокаПоиска
	|	" + СокрЛП(УсловиеОтбора) + "
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 50
	|	СпрНоменклатура.Ссылка,
	|	СпрНоменклатура.Код,
	|	СпрНоменклатура.Качество.Порядок,
	|	1,
	|	СпрНоменклатура.Наименование,
	|	NULL
	|ИЗ
	|	Справочник.Номенклатура КАК СпрНоменклатура
	|ГДЕ
	|	СпрНоменклатура.Код ПОДОБНО &СтрокаПоиска
	|	" + СокрЛП(УсловиеОтбора) + "
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 50
	|	СпрНоменклатура.Ссылка,
	|	СпрНоменклатура.Артикул,
	|	СпрНоменклатура.Качество.Порядок,
	|	2,
	|	СпрНоменклатура.Наименование,
	|	NULL
	|ИЗ
	|	Справочник.Номенклатура КАК СпрНоменклатура
	|ГДЕ
	|	СпрНоменклатура.Артикул ПОДОБНО &СтрокаПоиска
	|	" + СокрЛП(УсловиеОтбора) + "
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ ПЕРВЫЕ 50
	|	СпрНоменклатура.Ссылка,
	|	СпрНоменклатура.КодДляПоиска,
	|	СпрНоменклатура.Качество.Порядок,
	|	3,
	|	СпрНоменклатура.Наименование,
	|	NULL
	|ИЗ
	|	Справочник.Номенклатура КАК СпрНоменклатура
	|ГДЕ
	|	СпрНоменклатура.КодДляПоиска ПОДОБНО &СтрокаПоиска
	|	" + СокрЛП(УсловиеОтбора) + "
	|;
	|	
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	НоменклатураПоиск.Ссылка,
	|	МИНИМУМ(НоменклатураПоиск.Порядок) КАК Порядок
	|ПОМЕСТИТЬ НоменклатураПоПорядку
	|ИЗ
	|	НоменклатураПоиск КАК НоменклатураПоиск
	|
	|СГРУППИРОВАТЬ ПО
	|	НоменклатураПоиск.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	НоменклатураПоиск.Качество КАК Качество,
	|	НоменклатураПоиск.Ссылка,
	|	НоменклатураПоиск.Порядок КАК Порядок,
	|	НоменклатураПоиск.ПредставлениеСовпадения КАК ПредставлениеСовпадения,
	|	НоменклатураПоиск.ПредставлениеНоменклатуры КАК ПредставлениеНоменклатуры
	|ИЗ
	|	НоменклатураПоПорядку КАК НоменклатураПоПорядку
	|		ЛЕВОЕ СОЕДИНЕНИЕ НоменклатураПоиск КАК НоменклатураПоиск
	|		ПО НоменклатураПоПорядку.Ссылка = НоменклатураПоиск.Ссылка
	|			И НоменклатураПоПорядку.Порядок = НоменклатураПоиск.Порядок
	|
	|УПОРЯДОЧИТЬ ПО
	|	Порядок,
	|	Качество,
	|	ПредставлениеСовпадения,
	|	ПредставлениеНоменклатуры";
	
	Запрос.УстановитьПараметр("СтрокаПоиска", СтрокаПоиска + "%");
	
	ДанныеВыбора = Новый СписокЗначений;

	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		ТекстЗначения = СокрП(Выборка.ПредставлениеСовпадения) + " (" + Выборка.ПредставлениеНоменклатуры + ")";
		ДанныеВыбора.Добавить(Выборка.Ссылка, ТекстЗначения);
		
	КонецЦикла; 	
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

Процедура ПолучитьСписокДляВыбораУпаковок(Номенклатура, ДанныеВыбора, СтрокаПоиска, Знач ДобавлятьПустуюУпаковку)

	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	СпрНоменклатура.ЕдиницаИзмерения КАК ЕдиницаИзмерения,
	|	ПРЕДСТАВЛЕНИЕ(СпрНоменклатура.ЕдиницаИзмерения) КАК ЕдиницаИзмеренияПредставление,
	|	ЕСТЬNULL(СпрУпаковки.Ссылка, НЕОПРЕДЕЛЕНО) КАК Упаковка,
	|	ПРЕДСТАВЛЕНИЕ(СпрУпаковки.Ссылка) КАК УпаковкаПредставление,
	|	ЕСТЬNULL(СпрУпаковки.ЕдиницаИзмерения, НЕОПРЕДЕЛЕНО) КАК ЕдиницаИзмеренияУпаковки,
	|	ЕСТЬNULL(СпрУпаковки.ЕдиницаИзмерения.Представление,"""") КАК ЕдиницаИзмеренияУпаковкиПредставление,
	|	ЕСТЬNULL(СпрУпаковки.Коэффициент,0) КАК Коэффициент
	|ИЗ
	|	Справочник.Номенклатура КАК СпрНоменклатура
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.УпаковкиНоменклатуры КАК СпрУпаковки
	|		ПО (СпрУпаковки.Владелец = ВЫБОР
	|				КОГДА СпрНоменклатура.НаборУпаковок = ЗНАЧЕНИЕ(Справочник.НаборыУпаковок.ИндивидуальныйДляНоменклатуры)
	|					ТОГДА СпрНоменклатура.Ссылка
	|				КОГДА СпрНоменклатура.НаборУпаковок <> ЗНАЧЕНИЕ(Справочник.НаборыУпаковок.ПустаяСсылка)
	|					ТОГДА СпрНоменклатура.НаборУпаковок
	|				ИНАЧЕ ЛОЖЬ
	|			КОНЕЦ)
	|			" + ?(СтрокаПоиска = Неопределено, "", "И СпрУпаковки.Наименование ПОДОБНО &СтрокаПоиска") + "
	|   		И НЕ СпрУпаковки.ПометкаУдаления
	|
	|ГДЕ
	|	СпрНоменклатура.Ссылка = &Номенклатура
	|
	|УПОРЯДОЧИТЬ ПО
	|	Коэффициент,
	|	ЕдиницаИзмеренияУпаковкиПредставление";

	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);

	Если СтрокаПоиска <> Неопределено Тогда
		Запрос.УстановитьПараметр("СтрокаПоиска", СтрокаПоиска + "%");
	КонецЕсли;
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	ЕдиницаХраненияПредставление = "";
	ЕстьЕдиничнаяУпаковка = Ложь;
	
	Пока Выборка.Следующий() Цикл
		
		ЕдиницаХраненияПредставление = Выборка.ЕдиницаИзмеренияПредставление; 
		
		Если Выборка.Упаковка <> Неопределено Тогда
			Если Выборка.Коэффициент = 1
				И Выборка.ЕдиницаИзмеренияУпаковки = Выборка.ЕдиницаИзмерения Тогда
				ДанныеВыбора.Добавить(Выборка.Упаковка, Выборка.УпаковкаПредставление, Истина);
				ЕстьЕдиничнаяУпаковка = Истина;
			Иначе
				ДанныеВыбора.Добавить(Выборка.Упаковка, Выборка.УпаковкаПредставление, Ложь);
			КонецЕсли;
		КонецЕсли;
		
	КонецЦикла;

	Если ДобавлятьПустуюУпаковку
		И Не ЕстьЕдиничнаяУпаковка Тогда
		ДанныеВыбора.Вставить(0,Справочники.УпаковкиНоменклатуры.ПустаяСсылка(), ЕдиницаХраненияПредставление)
	КонецЕсли;
	
КонецПроцедуры

// Процедура заполняет страну происхождения в табличной части
//
// Параметры:
//  ДанныеГТД  - Структура - Структура содержить ссылку на номер гтд и страну происхождения
//
Процедура ЗаполнитьСтрануПроисхожденияДляНомераГТД(ДанныеГТД) Экспорт

	Если ТипЗнч(ДанныеГТД.НомерГТД) = Тип("СправочникСсылка.НомераГТД") Тогда
		ДанныеГТД.СтранаПроисхождения = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ДанныеГТД.НомерГТД, "СтранаПроисхождения");
	КонецЕсли;

КонецПроцедуры

// Процедура заполняет страну происхождения
// Параметры:
// 		ТаблицаФормы - ДанныеФормыКоллекция
//
Процедура ЗаполнитьСтрануПроисхождения(ТаблицаФормы) Экспорт
	
	Если НЕ ПолучитьФункциональнуюОпцию("ИспользоватьИмпортныеЗакупки") Тогда
		Возврат;
	КонецЕсли;
	
	Если ТаблицаФормы.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос("ВЫБРАТЬ
	|	ВЫРАЗИТЬ(Таблица.НомерСтроки КАК ЧИСЛО) КАК НомерСтроки,
	|	ВЫРАЗИТЬ(Таблица.Номенклатура КАК Справочник.Номенклатура) КАК Номенклатура,
	|	ВЫРАЗИТЬ(Таблица.НомерГТД КАК Справочник.НомераГТД) КАК НомерГТД
	|ПОМЕСТИТЬ Таблица
	|ИЗ
	|	&Таблица КАК Таблица
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Таблица.НомерСтроки КАК НомерСтроки,
	|	Таблица.НомерГТД.СтранаПроисхождения КАК СтранаПроисхождения
	|ИЗ
	|	Таблица КАК Таблица
	|ГДЕ
	|	Таблица.Номенклатура.ВестиУчетПоГТД");
	
	Запрос.УстановитьПараметр(
		"Таблица",
		ТаблицаФормы.Выгрузить(,"НомерСтроки, Номенклатура, НомерГТД"));
	
	УстановитьПривилегированныйРежим(Истина);
	Выборка = Запрос.Выполнить().Выбрать();
	УстановитьПривилегированныйРежим(Ложь);
	
	Пока Выборка.Следующий() Цикл
		ЗаполнитьЗначенияСвойств(ТаблицаФормы[Выборка.НомерСтроки-1], Выборка, "СтранаПроисхождения");
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти
