﻿
#Область ПрограммныйИнтерфейс

#Область ЭтикеткиИЦенники

Функция ДанныеДляПечатиЦенниковИЭтикеток(Идентификатор, ОбъектыПечати) Экспорт
	
	Если Идентификатор = "Ценники" Тогда
		Возврат Документы[ОбъектыПечати[0].Метаданные().Имя].ДанныеДляПечатиЦенников(ОбъектыПечати);
	Иначе // Этикетки
		Возврат Документы[ОбъектыПечати[0].Метаданные().Имя].ДанныеДляПечатиЭтикеток(ОбъектыПечати);
	КонецЕсли;
	
КонецФункции

Функция ДанныеДляПечатиЭтикетокСкладскиеЯчейки(Идентификатор, ОбъектыПечати) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	СкладскиеЯчейки.Владелец КАК Склад,
	|	СкладскиеЯчейки.Помещение
	|ИЗ
	|	Справочник.СкладскиеЯчейки КАК СкладскиеЯчейки
	|ГДЕ
	|	СкладскиеЯчейки.Ссылка В(&Ячейки)";
	Запрос.УстановитьПараметр("Ячейки", ОбъектыПечати);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Количество() <> 1 Тогда
		ТекстИсключения = НСтр("ru = 'Выделены ячейки разных складских территорий (помещений).
			|Одновременно можно печатать этикетки только по ячейкам, принадлежащим одной складской территории (помещению).'");
		ВызватьИсключение ТекстИсключения;
	КонецЕсли;
	
	Выборка.Следующий();
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	СкладскиеЯчейки.Ссылка КАК Ячейка
	|ИЗ
	|	Справочник.СкладскиеЯчейки КАК СкладскиеЯчейки
	|ГДЕ
	|	СкладскиеЯчейки.Ссылка В ИЕРАРХИИ(&Ячейки)
	|	И НЕ СкладскиеЯчейки.ПометкаУдаления
	|	И НЕ СкладскиеЯчейки.ЭтоГруппа
	|
	|УПОРЯДОЧИТЬ ПО
	|	СкладскиеЯчейки.Код";
	
	Запрос.УстановитьПараметр("Ячейки", ОбъектыПечати);
	
	ТаблицаЯчеек = Запрос.Выполнить().Выгрузить();
	
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("Склад", 	  Выборка.Склад);
	СтруктураПараметров.Вставить("Помещение", Выборка.Помещение);
	СтруктураПараметров.Вставить("Ячейки",    ТаблицаЯчеек);
	
	Возврат ПоместитьВоВременноеХранилище(СтруктураПараметров);
	
КонецФункции

Функция ДанныеДляПечатиЭтикетокДоставки(Идентификатор, ОбъектыПечати) Экспорт
	
	Возврат Обработки.ПечатьЭтикетокИЦенников.ДанныеДляПечатиЭтикетокДоставки(ОбъектыПечати[0]);
	
КонецФункции

Функция ДанныеДляПечатиЭтикетокУпаковочныеЛисты(ОбъектыПечати) Экспорт
	
	Возврат Обработки.ПечатьЭтикетокИЦенников.ДанныеДляПечатиЭтикетокУпаковочныеЛисты(ОбъектыПечати);
	
КонецФункции

#КонецОбласти

#Область Инв3_Инв19

Функция ПолучитьПараметрыФормирования(МассивПересчетов) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("МассивПересчетов", МассивПересчетов);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ПересчетТоваров.Ссылка КАК Ссылка,
	|	НАЧАЛОПЕРИОДА(ПересчетТоваров.Дата, ДЕНЬ) КАК Дата,
	|	ПересчетТоваров.Склад
	|ПОМЕСТИТЬ СписокПересчетов
	|ИЗ
	|	Документ.ПересчетТоваров КАК ПересчетТоваров
	|ГДЕ
	|	ПересчетТоваров.Ссылка В(&МассивПересчетов)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	ИнвентаризационнаяОпись.Ссылка,
	|	ИнвентаризационнаяОпись.Организация,
	|	ИнвентаризационнаяОпись.ДатаНачала,
	|	ИнвентаризационнаяОпись.ДатаОкончания,
	|	ИнвентаризационнаяОпись.Склад
	|ПОМЕСТИТЬ СписокОписей
	|ИЗ
	|	Документ.ИнвентаризационнаяОпись КАК ИнвентаризационнаяОпись
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ СписокПересчетов КАК ПересчетТоваров
	|		ПО (ПересчетТоваров.Склад = ИнвентаризационнаяОпись.Склад)
	|			И (ПересчетТоваров.Дата МЕЖДУ ИнвентаризационнаяОпись.ДатаНачала И ИнвентаризационнаяОпись.ДатаОкончания)
	|ГДЕ
	|	ИнвентаризационнаяОпись.Проведен
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СписокОписей.Ссылка КАК Ссылка
	|ИЗ
	|	СписокОписей КАК СписокОписей
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	СписокПересчетов.Склад
	|ИЗ
	|	СписокПересчетов КАК СписокПересчетов
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	МИНИМУМ(СписокОписей.ДатаНачала) КАК ДатаНачала,
	|	МАКСИМУМ(СписокОписей.ДатаОкончания) КАК ДатаОкончания
	|ИЗ
	|	СписокОписей КАК СписокОписей
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	МИНИМУМ(СписокПересчетов.Дата) КАК ДатаНачала,
	|	МАКСИМУМ(СписокПересчетов.Дата) КАК ДатаОкончания
	|ИЗ
	|	СписокПересчетов КАК СписокПересчетов
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	СписокОписей.Организация
	|ИЗ
	|	СписокОписей КАК СписокОписей";
	Результат = Запрос.ВыполнитьПакет();
	
	ПараметрыФормирования = Новый Структура;
	ПараметрыФормирования.Вставить("Описи", Результат[2].Выгрузить().ВыгрузитьКолонку("Ссылка"));
	ПараметрыФормирования.Вставить("Склады", Результат[3].Выгрузить().ВыгрузитьКолонку("Склад"));
	Выборка = Результат[4].Выбрать();
	Выборка.Следующий();
	ПараметрыФормирования.Вставить("ДатаНачала", Выборка.ДатаНачала);
	ПараметрыФормирования.Вставить("ДатаОкончания", Выборка.ДатаОкончания);
	Если НЕ ЗначениеЗаполнено(ПараметрыФормирования.ДатаНачала) Тогда
		Выборка = Результат[5].Выбрать();
		Выборка.Следующий();
		ПараметрыФормирования.Вставить("ДатаНачала", Выборка.ДатаНачала);
		ПараметрыФормирования.Вставить("ДатаОкончания", Выборка.ДатаОкончания);
	КонецЕсли;
	ПараметрыФормирования.Вставить("Организации", Результат[6].Выгрузить().ВыгрузитьКолонку("Организация"));
	
	Возврат ПараметрыФормирования;
	
КонецФункции

#КонецОбласти

#Область Км3

Функция ПроверитьПроведенностьИСтатусПробитДокументовДляПечатиИНВ3(Идентификатор, ОбъектыПечати) Экспорт
	
	Результат = Новый Массив;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	СУММА(ВложенныйЗапрос.КоличествоЧеков) КАК КоличествоЧеков,
	|	СУММА(ВложенныйЗапрос.КоличествоОтчетов) КАК КоличествоОтчетов
	|ИЗ
	|	(ВЫБРАТЬ
	|		КОЛИЧЕСТВО(Документ.Ссылка) КАК КоличествоЧеков,
	|		0 КАК КоличествоОтчетов
	|	ИЗ
	|		Документ.ЧекККМВозврат КАК Документ
	|	ГДЕ
	|		Документ.Ссылка В(&МассивДокументов)
	|		И НЕ Документ.Проведен
	|		И НЕ Документ.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЧековККМ.Пробит)
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		КОЛИЧЕСТВО(Документ.Ссылка) КАК КоличествоЧеков,
	|		0 КАК КоличествоОтчетов
	|	ИЗ
	|		Документ.ВозвратПодарочныхСертификатов КАК Документ
	|	ГДЕ
	|		Документ.Ссылка В(&МассивДокументов)
	|		И НЕ Документ.Проведен
	|		И НЕ Документ.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЧековККМ.Пробит)
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		0,
	|		КОЛИЧЕСТВО(Документ.Ссылка)
	|	ИЗ
	|		Документ.ОтчетОРозничныхПродажах КАК Документ
	|	ГДЕ
	|		Документ.Ссылка В(&МассивДокументов)
	|		И НЕ Документ.Проведен) КАК ВложенныйЗапрос";
		
	Запрос.УстановитьПараметр("МассивДокументов", ОбъектыПечати);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Выборка.Выбрать();
	ВозвращаемоеЗначение = Новый Структура("КоличествоЧеков, КоличествоОтчетов", 0, 0);
	Если Выборка.Следующий() Тогда
		ВозвращаемоеЗначение.КоличествоЧеков   = Выборка.КоличествоЧеков;
		ВозвращаемоеЗначение.КоличествоОтчетов = Выборка.КоличествоОтчетов;
	КонецЕсли;
	
	Возврат ВозвращаемоеЗначение;

КонецФункции

#КонецОбласти

#Область ТТН

Функция ПроверитьНаличиеТранспортныхНакладныхДляРаспоряженийИзЗаданийНаПеревозку(МассивОбъектов) Экспорт
	Возврат	Документы.ТранспортнаяНакладная.ПроверитьНаличиеТранспортныхНакладныхДляРаспоряженийИзЗаданийНаПеревозку(МассивОбъектов);
КонецФункции

Функция ПроверитьНаличиеТранспортныхНакладныхДляРаспоряжений(МассивОбъектов) Экспорт
	Возврат	Документы.ТранспортнаяНакладная.ПроверитьНаличиеТранспортныхНакладныхДляРаспоряжений(МассивОбъектов);
КонецФункции

Функция СоздатьТранспортныеНакладныеДляЗаданийНаПеревозку(МассивОбъектов) Экспорт
	ТранспортныеНакладные = Документы.ТранспортнаяНакладная.СоздатьТранспортныеНакладныеДляЗаданийНаПеревозку(МассивОбъектов);
	Возврат ТранспортныеНакладные;
КонецФункции

Функция СоздатьТранспортныеНакладные(МассивОбъектов) Экспорт
	ТранспортныеНакладные = Документы.ТранспортнаяНакладная.СоздатьТранспортныеНакладные(МассивОбъектов);
	Возврат ТранспортныеНакладные;
КонецФункции

Функция ПолучитьТранспортныеНакладныеНаПечать(ОбъектыПечати) Экспорт
	
	ТипДокументов = ТипЗнч(ОбъектыПечати[0]);
	МассивДокументовБезНакладных = Новый Массив;
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("ОбъектыПечати", ОбъектыПечати);
			
	Если ТипДокументов = Тип("ДокументСсылка.ЗаданиеНаПеревозку") Тогда
		
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	ТранспортнаяНакладнаяДокументыОснования.Ссылка КАК ТранспортнаяНакладная,
		|	ТранспортнаяНакладнаяДокументыОснования.Ссылка.ЗаданиеНаПеревозку КАК ДокументОснование,
		|	ТранспортнаяНакладнаяДокументыОснования.Ссылка.Дата КАК Дата,
		|	ЗаданиеНаПеревозкуМаршрут.НомерСтроки КАК НомерСтроки
		|ПОМЕСТИТЬ ТранспортныеНакладныеИОснования
		|ИЗ
		|	Документ.ЗаданиеНаПеревозку.Распоряжения КАК ЗаданиеНаПеревозкуРаспоряжения
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ТранспортнаяНакладная.ДокументыОснования КАК ТранспортнаяНакладнаяДокументыОснования
		|		ПО ЗаданиеНаПеревозкуРаспоряжения.Распоряжение = ТранспортнаяНакладнаяДокументыОснования.ДокументОснование
		|			И ЗаданиеНаПеревозкуРаспоряжения.Ссылка = ТранспортнаяНакладнаяДокументыОснования.Ссылка.ЗаданиеНаПеревозку
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.ЗаданиеНаПеревозку.Маршрут КАК ЗаданиеНаПеревозкуМаршрут
		|		ПО ЗаданиеНаПеревозкуРаспоряжения.КлючСвязи = ЗаданиеНаПеревозкуМаршрут.КлючСвязи
		|ГДЕ
		|	ЗаданиеНаПеревозкуРаспоряжения.Ссылка В(&ОбъектыПечати)
		|	И НЕ ТранспортнаяНакладнаяДокументыОснования.Ссылка.ПометкаУдаления
		|	И ТранспортнаяНакладнаяДокументыОснования.Ссылка.Проведен
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ТранспортныеНакладныеИОснования.ТранспортнаяНакладная КАК ТранспортнаяНакладная,
		|	ТранспортныеНакладныеИОснования.НомерСтроки КАК НомерСтроки,
		|	ТранспортныеНакладныеИОснования.ДокументОснование КАК ДокументОснование,
		|	ТранспортныеНакладныеИОснования.Дата
		|ИЗ
		|	ТранспортныеНакладныеИОснования КАК ТранспортныеНакладныеИОснования
		|
		|УПОРЯДОЧИТЬ ПО
		|	ТранспортныеНакладныеИОснования.ДокументОснование,
		|	ТранспортныеНакладныеИОснования.НомерСтроки,
		|	ТранспортныеНакладныеИОснования.Дата
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ТранспортныеНакладныеИОснования.ДокументОснование
		|ИЗ
		|	ТранспортныеНакладныеИОснования КАК ТранспортныеНакладныеИОснования";
		
		
	Иначе
		
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	ТранспортнаяНакладнаяДокументыОснования.Ссылка КАК ТранспортнаяНакладная,
		|	ТранспортнаяНакладнаяДокументыОснования.Ссылка.Дата КАК Дата,
		|	ТранспортнаяНакладнаяДокументыОснования.ДокументОснование
		|ПОМЕСТИТЬ ТранспортныеНакладныеИОснования
		|ИЗ
		|	Документ.ТранспортнаяНакладная.ДокументыОснования КАК ТранспортнаяНакладнаяДокументыОснования
		|ГДЕ
		|	ТранспортнаяНакладнаяДокументыОснования.ДокументОснование В(&ОбъектыПечати)
		|	И НЕ ТранспортнаяНакладнаяДокументыОснования.Ссылка.ПометкаУдаления
		|	И ТранспортнаяНакладнаяДокументыОснования.Ссылка.Проведен
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ТранспортныеНакладныеИОснования.ТранспортнаяНакладная КАК ТранспортнаяНакладная,
		|	ТранспортныеНакладныеИОснования.Дата
		|ИЗ
		|	ТранспортныеНакладныеИОснования КАК ТранспортныеНакладныеИОснования
		|
		|УПОРЯДОЧИТЬ ПО
		|	ТранспортныеНакладныеИОснования.Дата
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ТранспортныеНакладныеИОснования.ДокументОснование
		|ИЗ
		|	ТранспортныеНакладныеИОснования КАК ТранспортныеНакладныеИОснования";
				
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	УстановитьПривилегированныйРежим(Ложь);
	ДокументыОснования = РезультатЗапроса[2].Выгрузить().ВыгрузитьКолонку("ДокументОснование");
	ТаблицаТранспортныеНакладныеНаПечать = РезультатЗапроса[1].Выгрузить();
	ТаблицаТранспортныеНакладныеНаПечать.Свернуть("ТранспортнаяНакладная");
	ТранспортныеНакладныеНаПечать = ТаблицаТранспортныеНакладныеНаПечать.ВыгрузитьКолонку("ТранспортнаяНакладная");
	
	Для	Каждого ОбъектПечати Из ОбъектыПечати Цикл
		
		Если ДокументыОснования.Найти(ОбъектПечати) = Неопределено Тогда
			МассивДокументовБезНакладных.Добавить(ОбъектПечати);	
		КонецЕсли;
		
	КонецЦикла;	 
	
	Структура = Новый Структура;
	Структура.Вставить("МассивДокументовБезНакладных", МассивДокументовБезНакладных);
	Структура.Вставить("ТранспортныеНакладныеНаПечать", ТранспортныеНакладныеНаПечать);
	
	Возврат Структура;	
	
КонецФункции

#КонецОбласти

#КонецОбласти
