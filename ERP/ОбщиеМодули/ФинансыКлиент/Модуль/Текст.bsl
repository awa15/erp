﻿
#Область ПрограммныйИнтерфейс

// Процедура получает банк по указанному БИК или корреспондентскому счету.
//
// Параметры:
//	Форма - УправляемаяФорма - Текущая форма
//	Элемент - ПолеУправляемойФормы - Поле, в котором произведен выбор значения.
//	Значение - Строка - Значение, выбранное в поле.
//	СписокБанков - СписокЗначений - Список найденных банков
//	Банк - СправочникСсылка.КлассификаторБанковРФ - Значение поля для указания банка
//
Процедура ПолучитьБанкПоРеквизитам(Форма, Элемент, Значение, СписокБанков, Банк) Экспорт

	// Если возвращен список банков, произведем выбор банка из списка.
	Если СписокБанков.Количество() > 1 Тогда
	
		ВыбранныйЭлемент = Форма.ВыбратьИзСписка(СписокБанков, Элемент);
		Если ВыбранныйЭлемент <> Неопределено Тогда
			Банк = ВыбранныйЭлемент.Значение;
		КонецЕсли;
		
	КонецЕсли;
	
	Если СписокБанков.Количество() = 0 Тогда
		
		Если Не ПустаяСтрока(Значение) Тогда
			
			СписокВариантовОтветовНаВопрос = Новый СписокЗначений;
			СписокВариантовОтветовНаВопрос.Добавить("ВыбратьИзСписка", НСтр("ru='Выбрать из списка'"));
			СписокВариантовОтветовНаВопрос.Добавить("ОтменитьВвод", НСтр("ru='Отменить ввод'"));
			
			ТекстВопроса = НСтр("ru = 'Банк с %Поле%  %Значение% не найден в классификаторе банков.'");
			ТекстВопроса = СтрЗаменить(ТекстВопроса,"%Поле%", Элемент.Имя);
			ТекстВопроса = СтрЗаменить(ТекстВопроса,"%Значение%", Значение);
			
			ОписаниеОповещения = Новый ОписаниеОповещения(
				"ПолучитьБанкПоРеквизитамЗавершение",
				ЭтотОбъект,
				Новый Структура("Элемент, Форма", Элемент, Форма)
				);
			ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, СписокВариантовОтветовНаВопрос, 0, , НСтр("ru = 'Выбор банка из классификатора'"));
			
		Иначе
			Результат = "ВыбратьИзСписка";
			ПолучитьБанкПоРеквизитамЗавершение(Результат, Новый Структура("Элемент, Форма", Элемент, Форма));
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура ПолучитьБанкПоРеквизитамЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт

	Элемент = ДополнительныеПараметры.Элемент;
	Форма = ДополнительныеПараметры.Форма;
	
	Ответ = РезультатВопроса;
	
	Если Ответ = "ОтменитьВвод" Тогда
		Форма.БИКБанка = "";
	ИначеЕсли Ответ = "ВыбратьИзСписка" Тогда
		СтруктураПараметров = Новый Структура;
		ОткрытьФорму("Справочник.КлассификаторБанковРФ.Форма.ФормаВыбора", , Элемент);
	КонецЕсли;

КонецПроцедуры

// Процедура выводит сообщения пользователю, если заполнение на основании
// не было выполнено.
//
// Параметры:
//	Объект - ДанныеФорма - Текущий объект
//	Параметры - Структура - Коллекция параметров формы
//
Процедура ПроверитьЗаполнениеДокументаНаОсновании(Объект, Основание) Экспорт
	
	Если ЗначениеЗаполнено(Основание)
	   И Объект.СуммаДокумента = 0 Тогда
	   
		Если ТипЗнч(Основание) = Тип("ДокументСсылка.СчетНаОплатуКлиенту") Тогда
			Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Остаток задолженности по счету ""%1"" равен 0. Укажите сумму документа вручную'"),
				Основание);
		ИначеЕсли ТипЗнч(Основание) = Тип("ДокументСсылка.ЗаявкаНаРасходованиеДенежныхСредств") Тогда
			Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Остаток по заявке ""%1"" равен 0. Выберите неоплаченную заявку'"),
				Основание);
		ИначеЕсли ТипЗнч(Основание) = Тип("ДокументСсылка.РаспоряжениеНаПеремещениеДенежныхСредств") Тогда
			Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Остаток по распоряжению ""%1"" равен 0. Выберите неоплаченное распоряжение'"),
				Основание);
		ИначеЕсли ТипЗнч(Основание) = Тип("ДокументСсылка.РасходныйКассовыйОрдер") Тогда
			Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Остаток денежных средств к поступлению по документу ""%1"" равен 0. Укажите сумму документа вручную'"),
				Основание);
		Иначе
			Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Остаток задолженности по документу ""%1"" равен 0. Укажите сумму документа вручную'"),
				Основание);
		КонецЕсли;
				
		Если Не ПустаяСтрока(Текст) Тогда
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				Текст,
				,
				"Объект.СуммаДокумента",
				// Отказ
			);
		КонецЕсли;
	   
	КонецЕсли;
	
КонецПроцедуры

// Проверяет необходимость пересчета сумм документа из валюты в валюту
//
// Параметры:
//	Объект - ДанныеФормыСтруктура - Текущий документ
//	ТекущаяВалюта - СправочникСсылка.Валюты - Текущая валюта
//	НоваяВалюта - СправочникСсылка.Валюты - Новая валюта
//
// Возвращаемое значение:
//	Булево - Истина, если требуется пересчет сумм
//
Функция НеобходимПересчетВВалюту(Объект, ТекущаяВалюта, НоваяВалюта) Экспорт
	
	НеобходимПересчет = Ложь;
	
	Если ЗначениеЗаполнено(ТекущаяВалюта)
	 И ЗначениеЗаполнено(НоваяВалюта)
	 И ТекущаяВалюта <> НоваяВалюта Тогда
	
		МассивТабличныйЧастей = Новый Массив;
		МассивТабличныйЧастей.Добавить("РасшифровкаПлатежа");
		МассивТабличныйЧастей.Добавить("ДебиторскаяЗадолженность");
		МассивТабличныйЧастей.Добавить("КредиторскаяЗадолженность");
		
		Если Объект.СуммаДокумента <> 0 Тогда
			НеобходимПересчет = Истина;
		Иначе
			Для Каждого ТабличнаяЧасть Из МассивТабличныйЧастей Цикл
				
				Если Объект.Свойство(ТабличнаяЧасть)
				 И Объект[ТабличнаяЧасть].Итог("Сумма") <> 0 Тогда
					НеобходимПересчет = Истина;
					Прервать;
				КонецЕсли;
				
			КонецЦикла;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат НеобходимПересчет;
	
КонецФункции

// Процедура при необходимости очищает сумму взаиморасчетов в табличной части "Расшифровка платежа".
//
// Параметры:
//	Объект - ДанныеФормыСтруктура - Текущий документ
//
Процедура ОчиститьСуммуВзаиморасчетовРасшифровкиПлатежа(Объект) Экспорт
	
	Для Каждого СтрокаТаблицы Из Объект.РасшифровкаПлатежа Цикл
		
		Если СтрокаТаблицы.СуммаВзаиморасчетов > 0
			И СтрокаТаблицы.ВалютаВзаиморасчетов <> Объект.Валюта Тогда
		
			СтрокаТаблицы.СуммаВзаиморасчетов = 0;
			
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

// Процедура очищает сумму взаиморасчетов и валюту взаиморасчетов в табличной части "Расшифровка платежа".
//
// Параметры:
//	Объект - ДанныеФормыСтруктура - Текущий документ
//
Процедура ОчиститьСуммуИВалютуВзаиморасчетовРасшифровкиПлатежа(Объект) Экспорт
	
	Для Каждого СтрокаТаблицы Из Объект.РасшифровкаПлатежа Цикл
		
		Если ЗначениеЗаполнено(СтрокаТаблицы.Заказ) Тогда
			Если СтрокаТаблицы.ВалютаВзаиморасчетов = Объект.Валюта Тогда
				СтрокаТаблицы.СуммаВзаиморасчетов = 0;
			КонецЕсли;
		Иначе
			СтрокаТаблицы.ВалютаВзаиморасчетов = Неопределено;
			СтрокаТаблицы.СуммаВзаиморасчетов = 0;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

// Процедура пересчитывает сумму в строке табличной части "Расшифровка платежа" при изменении суммы в шапке документа.
//
// Параметры:
//	Объект - ДанныеФормыСтруктура - Текущий документ
//	СуммаДокумента - Число - Сумма документа
//
Процедура ПересчитатьСуммыВСтрокеРасшифровкиПлатежа(Объект, СуммаДокумента) Экспорт
	
	Если Объект.РасшифровкаПлатежа.Количество() = 1 Тогда
		Если СуммаДокумента <> 0 Тогда
			СтрокаТаблицы = Объект.РасшифровкаПлатежа[0];
			СтрокаТаблицы.Сумма = СуммаДокумента;
			СтрокаТаблицы.СуммаВзаиморасчетов = 0;
		Иначе
			Объект.РасшифровкаПлатежа.Удалить(0);
		КонецЕсли;
	ИначеЕсли Объект.РасшифровкаПлатежа.Количество() = 0 И СуммаДокумента > 0 Тогда
		СтрокаТаблицы = Объект.РасшифровкаПлатежа.Добавить();
		СтрокаТаблицы.Сумма = СуммаДокумента;
	ИначеЕсли СуммаДокумента = 0 Тогда
		Объект.РасшифровкаПлатежа.Очистить();
	ИначеЕсли Объект.РасшифровкаПлатежа.Итог("Сумма") > СуммаДокумента Тогда
		СуммаРазницы = Объект.РасшифровкаПлатежа.Итог("Сумма") - СуммаДокумента;
		НомерСтроки = Объект.РасшифровкаПлатежа.Количество() - 1;
		Пока НомерСтроки <> 0 Цикл
			СтрокаРасшифровки = Объект.РасшифровкаПлатежа[НомерСтроки];
			Если СтрокаРасшифровки.Сумма = СуммаРазницы Тогда
				Объект.РасшифровкаПлатежа.Удалить(НомерСтроки);
			ИначеЕсли СтрокаРасшифровки.Сумма > СуммаРазницы Тогда
				СтрокаРасшифровки.Сумма = СтрокаРасшифровки.Сумма - СуммаРазницы;
				СтрокаРасшифровки.СуммаВзаиморасчетов = 0;
			ИначеЕсли СтрокаРасшифровки.Сумма < СуммаРазницы Тогда
				СуммаРазницы = СуммаРазницы - СтрокаРасшифровки.Сумма;
				Объект.РасшифровкаПлатежа.Удалить(НомерСтроки);
			КонецЕсли;
			НомерСтроки = НомерСтроки - 1 ;
		КонецЦикла; 
	КонецЕсли;
	
КонецПроцедуры

// Открытие формы просмотра/редактирования видов запасов документа.
//
// Параметры:
//	Объект  - ДанныеФормыСтруктура - Текущий документ
//	Форма - Текущая форма
//	РедактироватьВидыЗапасов - Булево - Разрешено редактирование видов запасов в форме
//
Процедура ОткрытьВидыЗапасов(Объект, АдресТоваровВХранилище, АдресВидовЗапасовВХранилище, Форма, РедактироватьВидыЗапасов = Истина, ОтображатьДокументРеализации = Ложь, Склад = Неопределено) Экспорт
	ПараметрыВвода = Новый Структура("
		|АдресТоваровВХранилище,
		|АдресВидовЗапасовВХранилище,
		|Организация,
		|Склад,
		|ЦенаВключаетНДС,
		|РедактироватьВидыЗапасов,
		|ДокументМодифицирован,
		|ОтображатьДокументРеализации,
		|ВидыЗапасовУказаныВручную
		|");
	ЗаполнитьЗначенияСвойств(ПараметрыВвода, Объект); // Организация, Склад, ЦенаВключаетНДС, ВидыЗапасовУказаныВручную
	Если ЗначениеЗаполнено(Склад) Тогда
		ПараметрыВвода.Склад = Склад;
	КонецЕсли;
	
	ПараметрыВвода.АдресТоваровВХранилище = АдресТоваровВХранилище;
	ПараметрыВвода.АдресВидовЗапасовВХранилище = АдресВидовЗапасовВХранилище;
	ПараметрыВвода.ОтображатьДокументРеализации = ОтображатьДокументРеализации;
	ПараметрыВвода.ДокументМодифицирован = Форма.Модифицированность;
	
	ПараметрыВвода.РедактироватьВидыЗапасов =
		РедактироватьВидыЗапасов И (Не Форма.ТолькоПросмотр) И Форма.Доступность
		И ЗначениеЗаполнено(ПараметрыВвода.ВидыЗапасовУказаныВручную);
	
	ФормаВвода = ОткрытьФорму("Справочник.ВидыЗапасов.Форма.ФормаВводаВидовЗапасов", ПараметрыВвода, Форма);
	Если ПараметрыВвода.РедактироватьВидыЗапасов И ФормаВвода.РедактироватьВидыЗапасов Тогда
		Форма.ЗаблокироватьДанныеФормыДляРедактирования();
		Форма.Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

// Процедура - обработчик события "ПриИзменении" поля "СтатьяДоходов".
//
Процедура СтатьяДоходовПриИзменении(Объект, Элементы) Экспорт
	
	Элементы.АналитикаДоходов.ТолькоПросмотр = Не ЗначениеЗаполнено(Объект.СтатьяДоходов);
	
	Если Не ЗначениеЗаполнено(Объект.СтатьяДоходов) Тогда
		Объект.АналитикаДоходов = Неопределено;
	КонецЕсли;
	
КонецПроцедуры

// Процедура - обработчик события "ПриИзменении" поля "СтатьяРасходов".
//
Процедура СтатьяРасходовПриИзменении(Объект, Элементы) Экспорт
	
	Элементы.АналитикаРасходов.ТолькоПросмотр = Не ЗначениеЗаполнено(Объект.СтатьяРасходов);
	
	Если Не ЗначениеЗаполнено(Объект.СтатьяРасходов) Тогда
		Объект.АналитикаРасходов = Неопределено;
	КонецЕсли;
	
КонецПроцедуры

// Определяет относится ли хозяйственная операция документа к расчетами с клиентами.
//
// Параметры:
//	ХозяйственнаяОперация - ПеречислениеСсылка.ХозяйственныеОперации - Хозяйственная операция документа
//
// Возвращаемое значение:
//	Булево - Хозяйственная операция относится к расчетам с клиентами
//
Функция ЭтоРасчетыСКлиентами(ХозяйственнаяОперация) Экспорт
	
	Если ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента")
	 ИЛИ ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиенту") Тогда
		ЭтоРасчетыСКлиентами = Истина;
	Иначе
		ЭтоРасчетыСКлиентами = Ложь;
	КонецЕсли;
	
	Возврат ЭтоРасчетыСКлиентами;
	
КонецФункции

// Процедура выбора документа расчетов с клиентами или поставщиками.
//
// Параметры:
//	Организация - СправочникСсылка.Организации - Организация документа
//	Партнер - СправочникСсылка.Партнеры - Партнер документа
//	Контрагент - СправочникСсылка.Контрагенты - Контрагент документа
//	Соглашение - Соглашение с клиентом или соглашение с поставщиком
//	РедактируемыйДокумент - ДокументСсылка - Документ, из которого происходит выбор
//	ЭтоРасчетыСКлиентами - Булево - Признак расчетов с клиентами
//	Элемент - ПолеФормы - Поле для выбора документа расчетов
//	СтандартнаяОбработка - Булево - Признак выполнения стандартной (системной) обработки
//
Процедура ДокументРасчетовНачалоВыбора(
	Организация,
	Партнер,
	Контрагент,
	Соглашение,
	РедактируемыйДокумент,
	ЭтоРасчетыСКлиентами,
	ВыборОснованияПлатежа,
	Элемент,
	СтандартнаяОбработка,
	ХозяйственнаяОперация = Неопределено,
	ИсключитьХозяйственнуюОперацию = Ложь
	) Экспорт
		
	СтандартнаяОбработка = Ложь;
	
	Если ЗначениеЗаполнено(ХозяйственнаяОперация) Тогда
	
		СтруктураОтбор = Новый Структура("Организация, Контрагент, ЭтоРасчетыСКлиентами, ХозяйственнаяОперация",
		Организация,
		Контрагент,
		ЭтоРасчетыСКлиентами,
		ХозяйственнаяОперация);
	
	Иначе
	
		СтруктураОтбор = Новый Структура("Организация, Контрагент, ЭтоРасчетыСКлиентами",
		Организация,
		Контрагент,
		ЭтоРасчетыСКлиентами);
		
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(Партнер) Тогда
		СтруктураОтбор.Вставить("Партнер", Партнер);	
	КонецЕсли;
	Если ЗначениеЗаполнено(Соглашение) Тогда
		СтруктураОтбор.Вставить("Соглашение", Соглашение);	
	КонецЕсли;
	
	СтруктураПараметры = Новый Структура;
	СтруктураПараметры.Вставить("Отбор", СтруктураОтбор);
	СтруктураПараметры.Вставить("ВыборОснованияПлатежа", ВыборОснованияПлатежа);
	СтруктураПараметры.Вставить("ИсключитьХозяйственнуюОперацию", ИсключитьХозяйственнуюОперацию);
	СтруктураПараметры.Вставить("РедактируемыйДокумент", РедактируемыйДокумент);
	
	ОткрытьФорму("ОбщаяФорма.ВыборДокументаРасчетов", СтруктураПараметры, Элемент);
	
КонецПроцедуры

// Процедура обработки события "ПриНачалеРедактирования" табличной части "РасшифровкаПлатежа".
//
Процедура РасшифровкаПлатежаПриНачалеРедактирования(
	Объект,
	Партнер,
	ДоговорКонтрагента,
	СтрокаТаблицы,
	НоваяСтрока,
	Копирование,
	СтатьяДвиженияДенежныхСредств = Неопределено) Экспорт
	
	ЭтоРасчетыСКлиентами = ЭтоРасчетыСКлиентами(Объект.ХозяйственнаяОперация);
	
	УстановитьПустуюСсылкуНаЗаказ(
		СтрокаТаблицы.Заказ,
		ЭтоРасчетыСКлиентами);
	
	Если СтрокаТаблицы.Свойство("ОснованиеПлатежа") Тогда
		УстановитьПустуюСсылкуНаЗаказ(
			СтрокаТаблицы.ОснованиеПлатежа,
			ЭтоРасчетыСКлиентами);
	КонецЕсли;
	
	Если НоваяСтрока Тогда
		
		Если Копирование Тогда
			
			СуммаОстаток = Объект.СуммаДокумента - Объект.РасшифровкаПлатежа.Итог("Сумма")
				+ Объект.РасшифровкаПлатежа[Объект.РасшифровкаПлатежа.Количество()-1].Сумма;
			
		Иначе
			
			СуммаОстаток = Объект.СуммаДокумента - Объект.РасшифровкаПлатежа.Итог("Сумма");
			
			Если ЗначениеЗаполнено(Партнер) Тогда
				СтрокаТаблицы.Партнер = Партнер;
			КонецЕсли;
			
			Если ЗначениеЗаполнено(ДоговорКонтрагента) Тогда
				СтрокаТаблицы.Заказ = ДоговорКонтрагента;
				СтрокаТаблицы.ЭтоДоговор = Истина;
				Если ЗначениеЗаполнено(СтатьяДвиженияДенежныхСредств) Тогда
					СтрокаТаблицы.СтатьяДвиженияДенежныхСредств = СтатьяДвиженияДенежныхСредств;
				КонецЕсли;
			КонецЕсли;
			
		КонецЕсли;
		
		СтрокаТаблицы.Сумма = СуммаОстаток;
		
	КонецЕсли;
	
КонецПроцедуры

// Получает пустую ссылку на заказ клиента или на заказ поставщику.
//
// Параметры:
//	Заказ - ДокументСсылка
//	ЭтоРасчетыСКлиентами - Булево - Признак отражения расчетов с клиентами
//
Процедура УстановитьПустуюСсылкуНаЗаказ(Заказ, ЭтоРасчетыСКлиентами) Экспорт
	
	Если Заказ = Неопределено Тогда
		Если ЭтоРасчетыСКлиентами Тогда
			Заказ = ПредопределенноеЗначение("Документ.ЗаказКлиента.ПустаяСсылка");
		Иначе
			Заказ = ПредопределенноеЗначение("Документ.ЗаказПоставщику.ПустаяСсылка");
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

// Показывает оповещение пользователю о заполнении номеров ГТД в строках табличной части документа.
//
Процедура ОповеститьОЗаполненииНомеровГТДвТабличнойЧасти(НомерГТД, ЗаполненыНомераГТД) Экспорт
	
	Если ЗаполненыНомераГТД <> Неопределено И ЗаполненыНомераГТД Тогда
		Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru='В строках документа заполнен номер ГТД %1'"),
			Строка(НомерГТД));
		ПоказатьОповещениеПользователя(
			НСтр("ru = 'Номера ГТД заполнены'"),
			,
			Текст,
			БиблиотекаКартинок.Информация32);
	Иначе
		Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru='Ни в одной строке номер ГТД не заполнен'"),
			Строка(НомерГТД));
		ПоказатьОповещениеПользователя(
			НСтр("ru = 'Номера ГТД не заполнены'"),
			,
			Текст,
			БиблиотекаКартинок.Информация32);
	КонецЕсли;
	
КонецПроцедуры

// Создает документ оплаты по одной или нескольким заявкам на расход денежных средств
//
// Параметры:
//	Список - ТаблицаФормы - список заявок, по выделенным строкам которого будет создан документ оплаты
//	ИмяДокумента - Строка - имя документа в метеданных, который будет создан на основании заявок
//
Процедура СоздатьДокументОплатыНаОснованииЗаявокНаРасходДС(Список, ИмяДокумента) Экспорт
	
	ТекущиеДанные = Список.ТекущиеДанные;
	
	Если ТекущиеДанные = Неопределено Тогда
		ТекстПредупреждения = НСтр("ru = 'Команда не может быть выполнена для указанного объекта!'");
		ПоказатьПредупреждение(Неопределено, ТекстПредупреждения);
		Возврат;
	КонецЕсли;
	
	ИмяФормы = "Документ." + ИмяДокумента + ".Форма.ФормаДокумента";
	
	Если Список.ВыделенныеСтроки.Количество() = 1 Тогда
		
		Если ТипЗнч(Список.ТекущаяСтрока) = Тип("СтрокаГруппировкиДинамическогоСписка") Тогда
			ТекстПредупреждения = НСтр("ru = 'Команда не может быть выполнена для указанного объекта!'");
			ПоказатьПредупреждение(Неопределено, ТекстПредупреждения);
			Возврат;
		КонецЕсли;
		
		ПараметрыОснования = ТекущиеДанные.Ссылка;
		
		ОткрытьФорму(ИмяФормы, Новый Структура("Основание", ПараметрыОснования));
		
	Иначе
		
		МассивСсылок = Новый Массив();
		
		Для Каждого Заявка Из Список.ВыделенныеСтроки Цикл
			Если ТипЗнч(Заявка) = Тип("СтрокаГруппировкиДинамическогоСписка") Тогда
				Продолжить;
			КонецЕсли;
			МассивСсылок.Добавить(Список.ДанныеСтроки(Заявка).Ссылка);
		КонецЦикла;
		
		Если МассивСсылок.Количество() = 0 Тогда
			ТекстПредупреждения = НСтр("ru = 'Не выбрано ни одной заявки для ввода на основании!'");
			ПоказатьПредупреждение(Неопределено, ТекстПредупреждения);
			Возврат;
		КонецЕсли;
		
		ОчиститьСообщения();
		РеквизитыШапки = Новый Структура();
		
		Если ДенежныеСредстваВызовСервера.СформироватьДанныеЗаполненияОплаты(МассивСсылок, РеквизитыШапки) Тогда
			
			ПараметрыОснования = Новый Структура;
			ПараметрыОснования.Вставить("РеквизитыШапки",                        РеквизитыШапки);
			ПараметрыОснования.Вставить("ДокументОснование",                     МассивСсылок);
			ПараметрыОснования.Вставить("НесколькоЗаявокНаРасходованиеСредств",  Истина);
			
			ОткрытьФорму(ИмяФормы, Новый Структура("Основание", ПараметрыОснования));
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
