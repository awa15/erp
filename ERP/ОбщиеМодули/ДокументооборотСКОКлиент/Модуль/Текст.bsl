﻿
// Процедура получает Контекст ЭДО и возвращает его в Обработку оповещения, 
// переданную в параметрах к этой процедуре.
// 
//
// Параметры:
//	ВыполняемоеОповещение                  - ОписаниеОповещения - Описание оповещения, которое будет вызвано после получения Контекста ЭДО.
//                                                       В качестве результата описания оповещения передается структура с ключами: 
//                                                       * КонтекстЭДО    - Форма обработки, либо неопределено 
//                                                       * ТекстОшибки - Текст сообщения об ошибке, из-за которой не удалось получить контекст
//	ВызовИзМастераПодключенияК1СОтчетности - Булево - .
//
Процедура ПолучитьКонтекстЭДО(ВыполняемоеОповещение, ОбновитьСейчас = Ложь, ТихийРежим = Ложь) Экспорт
	
	ТекстСообщения = "";
	
	СтруктураПараметров = Новый Структура("КонтекстЭДО");
	Оповестить("Получение контекста ЭДО", СтруктураПараметров);
	
	Если СтруктураПараметров.КонтекстЭДО <> Неопределено И НЕ ОбновитьСейчас Тогда
		
		СтруктураРезультата = Новый Структура;
		СтруктураРезультата.Вставить("ТекстОшибки", ТекстСообщения);
		СтруктураРезультата.Вставить("КонтекстЭДО", СтруктураПараметров.КонтекстЭДО);
		
		ПараметрыПриложения.Вставить("РегламентированнаяОтчетность.КонтекстЭДО", СтруктураПараметров.КонтекстЭДО);

		ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, СтруктураРезультата);
		
	ИначеЕсли НЕ ЭлектронныйДокументооборотСКонтролирующимиОрганамиВызовСервераПовтИсп.РазделениеВключено() И ДокументооборотСКОВызовСервера.ПодключатьВнешнююОбработкуЭДО() Тогда
		Если ДокументооборотСКОВызовСервера.ЕстьПравоНаДОсКО(Истина) Тогда
			Попытка
				ФормаРезультат = ПолучитьФорму("ВнешняяОбработка.Обработка_ДокументооборотСКО.Форма.КонтейнерКлиентскихМетодов");
				ФормаРезультат.ПутьКОбъекту = "ВнешняяОбработка.Обработка_ДокументооборотСКО";
			Исключение
				Состояние(НСтр("ru = 'Не удалось загрузить внешний модуль для документооборота с налоговыми органами.
					|Будет использован модуль, встроенный в конфигурацию.'"));
				ФормаРезультат = ПолучитьФорму("Обработка.ДокументооборотСКонтролирующимиОрганами.Форма.КонтейнерКлиентскихМетодов");
				ФормаРезультат.ПутьКОбъекту = "Обработка.ДокументооборотСКонтролирующимиОрганами";
			КонецПопытки;
			
			// Проверка обновления
			ДополнительныеПараметры = Новый Структура("ВыполняемоеОповещение, ФормаРезультат", ВыполняемоеОповещение, ФормаРезультат);
			ОписаниеОповещения = Новый ОписаниеОповещения("ПолучитьКонтекстЭДОЗавершение", ЭтотОбъект, ДополнительныеПараметры);
			
			ФормаРезультат.ОбновитьМодульДокументооборотаСФНСПриНеобходимости(ОбновитьСейчас, ОписаниеОповещения, ТихийРежим);
				
		Иначе
			ТекстСообщения = НСтр("ru = 'Недостаточно прав для использования методов электронного документооборота с контролирующими органами.'");
			
			СтруктураРезультата = Новый Структура;
			СтруктураРезультата.Вставить("ТекстОшибки", ТекстСообщения);
			СтруктураРезультата.Вставить("КонтекстЭДО", Неопределено);
			
			ПараметрыПриложения.Вставить("РегламентированнаяОтчетность.КонтекстЭДО", Неопределено);
			
			ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, СтруктураРезультата);
		КонецЕсли;
		
	Иначе
		Если ДокументооборотСКОВызовСервера.ЕстьПравоНаДОсКО(Ложь) Тогда
			ФормаРезультат = ПолучитьФорму("Обработка.ДокументооборотСКонтролирующимиОрганами.Форма.КонтейнерКлиентскихМетодов");
			ФормаРезультат.ПутьКОбъекту = "Обработка.ДокументооборотСКонтролирующимиОрганами";
			
			// Проверка обновления
			ДополнительныеПараметры = Новый Структура("ВыполняемоеОповещение, ФормаРезультат", ВыполняемоеОповещение, ФормаРезультат);
			ОписаниеОповещения = Новый ОписаниеОповещения("ПолучитьКонтекстЭДОЗавершение", ЭтотОбъект, ДополнительныеПараметры);
			
			ФормаРезультат.ОбновитьМодульДокументооборотаСФНСПриНеобходимости(ОбновитьСейчас, ОписаниеОповещения, ТихийРежим);
			
		Иначе
			ТекстСообщения = НСтр("ru = 'Недостаточно прав для использования методов электронного документооборота с контролирующими органами.'");
			
			СтруктураРезультата = Новый Структура;
			СтруктураРезультата.Вставить("ТекстОшибки", ТекстСообщения);
			СтруктураРезультата.Вставить("КонтекстЭДО", Неопределено);
			
			ПараметрыПриложения.Вставить("РегламентированнаяОтчетность.КонтекстЭДО", Неопределено);

			ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, СтруктураРезультата);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры //ПолучитьКонтекстЭДО()

Процедура ОбновитьИПолучитьКонтекстЭДО(ВыполняемоеОповещение)
	
	Попытка
		КонтекстИнициализирован = ДокументооборотСКОВызовСервера.ИнициализироватьКонтекстДокументооборотаСНалоговымиОрганами();
	Исключение
		КонтекстИнициализирован = Ложь;
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Не удалось загрузить внешний модуль для документооборота с налоговыми органами.
			|%1
			|Будет продолжено использование текущего модуля конфигурации.'"), ИнформацияОбОшибке().Описание);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки);
	КонецПопытки;
	
	Если КонтекстИнициализирован Тогда
		Попытка
			ФормаРезультат = ПолучитьФорму("ВнешняяОбработка.Обработка_ДокументооборотСКО.Форма.КонтейнерКлиентскихМетодов");
			ФормаРезультат.ПутьКОбъекту = "ВнешняяОбработка.Обработка_ДокументооборотСКО";
		Исключение
			Состояние(НСтр("ru = 'Не удалось загрузить внешний модуль для документооборота с налоговыми органами.
				|Будет использован модуль, встроенный в конфигурацию.'"));
			ФормаРезультат = ПолучитьФорму("Обработка.ДокументооборотСКонтролирующимиОрганами.Форма.КонтейнерКлиентскихМетодов");
			ФормаРезультат.ПутьКОбъекту = "Обработка.ДокументооборотСКонтролирующимиОрганами";
		КонецПопытки;
	КонецЕсли;
	
	СтруктураРезультата = Новый Структура;
	СтруктураРезультата.Вставить("ТекстОшибки", "");
	СтруктураРезультата.Вставить("КонтекстЭДО", ФормаРезультат);
	
	ПараметрыПриложения.Вставить("РегламентированнаяОтчетность.КонтекстЭДО", ФормаРезультат);
	
	ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, СтруктураРезультата);
	
КонецПроцедуры

Процедура ПолучитьКонтекстЭДОЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	ВыполняемоеОповещение = ДополнительныеПараметры.ВыполняемоеОповещение;
	ФормаРезультат = ДополнительныеПараметры.ФормаРезультат;
	
	Если Результат Тогда
		
		ОбновитьИПолучитьКонтекстЭДО(ВыполняемоеОповещение);
		
	Иначе
		
		СтруктураРезультата = Новый Структура;
		СтруктураРезультата.Вставить("ТекстОшибки", "");
		СтруктураРезультата.Вставить("КонтекстЭДО", ФормаРезультат);
		ПараметрыПриложения.Вставить("РегламентированнаяОтчетность.КонтекстЭДО", ФормаРезультат);
		
		ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, СтруктураРезультата);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриНажатииНаКнопкуОтправкиВКонтролирующийОрган(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность = Ложь, СсылкаНаОтчет = Неопределено, ОрганизацияОтчета = Неопределено) Экспорт
	
	Если НЕ ЭтоОтправкаИзФормыОтчетность Тогда
		СсылкаНаОтчет = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.СсылкаНаОтчетПоФорме(Форма);
	КонецЕсли;
	
	ТипЗнчСсылкаНаОтчет = ТипЗнч(СсылкаНаОтчет);
	ИмяДокументаУведомлениеОКонтролируемыхСделках					= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("УведомлениеОКонтролируемыхСделках");
	ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде	= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде");
	ИмяДокументаИсходящееУведомлениеФНС					 			= "УведомлениеОСпецрежимахНалогообложения";
	
	ИмяДокументаЗаявлениеОВвозеТоваров 	= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("ЗаявлениеОВвозеТоваров");
	
	Если ИмяДокументаЗаявлениеОВвозеТоваров <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаЗаявлениеОВвозеТоваров) Тогда
		ЭтоЗаявлениеОВвозе = Истина;
	Иначе
		ЭтоЗаявлениеОВвозе = Ложь;
	КонецЕсли;
	
	Если (ИмяДокументаУведомлениеОКонтролируемыхСделках <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаУведомлениеОКонтролируемыхСделках))
		ИЛИ (ИмяДокументаИсходящееУведомлениеФНС <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаИсходящееУведомлениеФНС)) Тогда
		ЭтоУведомлениеФНС = Истина;
	Иначе
		ЭтоУведомлениеФНС = Ложь;
	КонецЕсли;
	
	Если ТипЗнч(СсылкаНаОтчет) = Тип("СправочникСсылка.ЭлектронныеПредставленияРегламентированныхОтчетов") Тогда
		ЭтоЖурналСчетовФактурФНС = 
			ЭлектронныйДокументооборотСКонтролирующимиОрганамиВызовСервера.ЗначениеРеквизитаОбъекта(СсылкаНаОтчет, "ВидОтчета") =
				ПредопределенноеЗначение("Справочник.ВидыОтправляемыхДокументов.ЖурналУчетаСчетовФактур");	
	Иначе	
		Если ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде) Тогда
			ЭтоЖурналСчетовФактурФНС = Истина;
		Иначе
			ЭтоЖурналСчетовФактурФНС = Ложь;
		КонецЕсли;
	КонецЕсли;
	
	
	Если ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка.РегламентированныйОтчет")
		ИЛИ ТипЗнчСсылкаНаОтчет = Тип("Неопределено") Тогда
		
		Если НЕ ЭтоОтправкаИзФормыОтчетность Тогда
			// отправляем только из записанной формы
			Если Форма.Модифицированность Тогда
				ДополнительныеПараметры = Новый Структура;
				ДополнительныеПараметры.Вставить("СсылкаНаОтчет",СсылкаНаОтчет);
				ДополнительныеПараметры.Вставить("Форма",Форма);
				ДополнительныеПараметры.Вставить("КонтролирующийОрган",КонтролирующийОрган);
				ДополнительныеПараметры.Вставить("ЭтоОтправкаИзФормыОтчетность",ЭтоОтправкаИзФормыОтчетность);
				ДополнительныеПараметры.Вставить("СсылкаНаОтчет",СсылкаНаОтчет);
				ДополнительныеПараметры.Вставить("ОрганизацияОтчета",ОрганизацияОтчета);
				ДополнительныеПараметры.Вставить("ЭтоУведомлениеФНС",ЭтоУведомлениеФНС);
				ДополнительныеПараметры.Вставить("ЭтоЖурналСчетовФактурФНС",ЭтоЖурналСчетовФактурФНС);
				ОписаниеОповещения = Новый ОписаниеОповещения("ПриНажатииНаКнопкуОтправкиВКонтролирующийОрганЗавершение", ЭтотОбъект, ДополнительныеПараметры);
				Форма.СохранитьНаКлиенте(,ОписаниеОповещения);
			Иначе
				ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
			КонецЕсли;
		Иначе
			ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
		КонецЕсли;
			
	ИначеЕсли ЭтоУведомлениеФНС Тогда
		
		Если ЭтоОтправкаИзФормыОтчетность Тогда
			// Уведомление будет записано
		ИначеЕсли Форма.Модифицированность ИЛИ НЕ ЗначениеЗаполнено(СсылкаНаОтчет) Тогда
			
			// Записываем
			Если ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаУведомлениеОКонтролируемыхСделках) Тогда
				Форма.Записать();
			Иначе
				Форма.СохранитьДанные();
			КонецЕсли;
			
			// Получаем ссылку еще раз после записи
			СсылкаНаОтчет = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.СсылкаНаОтчетПоФорме(Форма);
			
		КонецЕсли;
			
		Если НЕ ЗначениеЗаполнено(СсылкаНаОтчет) Тогда
			Возврат;
		КонецЕсли;
		
		// Отправка
		ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
		
	ИначеЕсли ЭтоЖурналСчетовФактурФНС Тогда
		
		Если ЭтоОтправкаИзФормыОтчетность Тогда
			// Уведомление будет записано
		ИначеЕсли Форма.Модифицированность ИЛИ НЕ ЗначениеЗаполнено(СсылкаНаОтчет) Тогда
			
			// Записываем
			Форма.Записать();
			
			// Получаем ссылку еще раз после записи
			СсылкаНаОтчет = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.СсылкаНаОтчетПоФорме(Форма);
			
		КонецЕсли;
			
		Если НЕ ЗначениеЗаполнено(СсылкаНаОтчет) Тогда
			Возврат;
		КонецЕсли;
		
		// Отправка
		ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
		
	ИначеЕсли ЭтоЗаявлениеОВвозе Тогда
		
		Если НЕ ЗначениеЗаполнено(СсылкаНаОтчет) Тогда
			ПоказатьПредупреждение(,"Перед отправкой необходимо записать заявление.");
			Возврат;
		КонецЕсли;
		
		Если НЕ ЭтоОтправкаИзФормыОтчетность И Форма.Модифицированность Тогда
			ПоказатьПредупреждение(, "Перед отправкой необходимо записать заявление.");
			Возврат;
		КонецЕсли;
		ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
		
	Иначе
		
		ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
		
	КонецЕсли;

КонецПроцедуры

Процедура ПриНажатииНаКнопкуОтправкиВКонтролирующийОрганЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	СсылкаНаОтчет = ДополнительныеПараметры.СсылкаНаОтчет;
	Форма = ДополнительныеПараметры.Форма;
	КонтролирующийОрган = ДополнительныеПараметры.КонтролирующийОрган;
	ЭтоОтправкаИзФормыОтчетность = ДополнительныеПараметры.ЭтоОтправкаИзФормыОтчетность;
	СсылкаНаОтчет = ДополнительныеПараметры.СсылкаНаОтчет;
	ОрганизацияОтчета = ДополнительныеПараметры.ОрганизацияОтчета;
	ЭтоУведомлениеФНС = ДополнительныеПараметры.ЭтоУведомлениеФНС;
	ЭтоЖурналСчетовФактурФНС = ДополнительныеПараметры.ЭтоЖурналСчетовФактурФНС;
	
	Если СсылкаНаОтчет = Неопределено Тогда
		СсылкаНаОтчет = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.СсылкаНаОтчетПоФорме(Форма);
	КонецЕсли;
	
	ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
	
КонецПроцедуры

Процедура ОтправкаВКонтролирующийОрганПослеСохранения(Форма, КонтролирующийОрган, ЭтоОтправкаИзФормыОтчетность, СсылкаНаОтчет, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС)
	
	Если НЕ ЭтоОтправкаИзФормыОтчетность Тогда
		ОрганизацияОтчета = ДокументооборотСКОКлиентСервер.ПолучитьОрганизациюПоФорме(Форма);
	КонецЕсли;
	
	НастроенОбменВУниверсальномФормате = Ложь;
	УчетнаяЗаписьПредназначенаДляДокументооборотаСКО = Ложь;
	
	ДокументооборотСКОВызовСервера.ПриНажатииНаКнопкуОтправкиВКонтролирующийОрган(ОрганизацияОтчета, КонтролирующийОрган, НастроенОбменВУниверсальномФормате, УчетнаяЗаписьПредназначенаДляДокументооборотаСКО);
	
	Если НЕ НастроенОбменВУниверсальномФормате Тогда
		ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиент.ПоказатьФормуПредложениеОформитьЗаявлениеНаПодключение(ОрганизацияОтчета);
		Возврат;
	Иначе
		Если УчетнаяЗаписьПредназначенаДляДокументооборотаСКО = Неопределено Тогда
			ПоказатьПредупреждение(, "Недостаточно прав для использования модуля документооборота!");
			Возврат;
			
		ИначеЕсли УчетнаяЗаписьПредназначенаДляДокументооборотаСКО = Ложь Тогда
			
			Если КонтролирующийОрган = "ФНС" Тогда
				ПоказатьПредупреждение(, "Учетная запись документооборота, сопоставленная организации, не предназначена для взаимодействия с ФНС.");
			ИначеЕсли КонтролирующийОрган = "ПФР" Тогда
				ПоказатьПредупреждение(, "Учетная запись документооборота, сопоставленная организации, не предназначена для взаимодействия с ПФР.");
			ИначеЕсли КонтролирующийОрган = "ФСГС" Тогда
				ПоказатьПредупреждение(, "Учетная запись документооборота, сопоставленная организации, не предназначена для взаимодействия с Росстатом.");
			КонецЕсли;
			
			Возврат;
			
		КонецЕсли;
	КонецЕсли;
	
	ДополнительныеПараметры = Новый Структура("Форма, КонтролирующийОрган, СсылкаНаОтчет, ЭтоОтправкаИзФормыОтчетность, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС", Форма, КонтролирующийОрган, СсылкаНаОтчет, ЭтоОтправкаИзФормыОтчетность, ОрганизацияОтчета, ЭтоУведомлениеФНС, ЭтоЖурналСчетовФактурФНС);
	ОписаниеОповещения = Новый ОписаниеОповещения("ОтправкаВКонтролирующийОрганПослеСохраненияЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
	
КонецПроцедуры

Процедура ПослеОтправкиУведомленияФНСЗавершение(РезультатОтправки, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент 				= ДополнительныеПараметры.КонтекстЭДОКлиент;
	ЭтоОтправкаИзФормыОтчетность 	= ДополнительныеПараметры.ЭтоОтправкаИзФормыОтчетность;
	Форма 							= ДополнительныеПараметры.Форма;
	КонтролирующийОрган 			= ДополнительныеПараметры.КонтролирующийОрган;
	СсылкаНаОтчет 					= ДополнительныеПараметры.СсылкаНаОтчет;
	ОрганизацияОтчета 				= ДополнительныеПараметры.ОрганизацияОтчета;
	
	КонтекстЭДОКлиент.ПредупредитьЕслиСтатусОтправкиВКонверте(СсылкаНаОтчет, "уведомление");
	
	Если НЕ ЭтоОтправкаИзФормыОтчетность Тогда
		Если РезультатОтправки <> Неопределено Тогда
			ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.ОбновитьПанельСостоянияОтправкиВРегламентированномОтчете(Форма, КонтролирующийОрган);
		КонецЕсли;
	КонецЕсли;
	
	ПараметрыОповещения = Новый Структура(); 
	ПараметрыОповещения.Вставить("Ссылка", СсылкаНаОтчет);
	ПараметрыОповещения.Вставить("Организация", ОрганизацияОтчета);
	Оповестить("Завершение отправки в контролирующий орган", ПараметрыОповещения, );
	
КонецПроцедуры

Процедура ПослеОтправкиЖурналаСчетовФактурВФНСЗавершение(РезультатОтправки, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент 				= ДополнительныеПараметры.КонтекстЭДОКлиент;
	ЭтоОтправкаИзФормыОтчетность 	= ДополнительныеПараметры.ЭтоОтправкаИзФормыОтчетность;
	Форма 							= ДополнительныеПараметры.Форма;
	КонтролирующийОрган 			= ДополнительныеПараметры.КонтролирующийОрган;
	СсылкаНаОтчет 					= ДополнительныеПараметры.СсылкаНаОтчет;
	ОрганизацияОтчета 				= ДополнительныеПараметры.ОрганизацияОтчета;
	
	КонтекстЭДОКлиент.ПредупредитьЕслиСтатусОтправкиВКонверте(СсылкаНаОтчет, "журнал");
	
	Если НЕ ЭтоОтправкаИзФормыОтчетность Тогда
		Если РезультатОтправки <> Неопределено Тогда
			ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.ОбновитьПанельСостоянияОтправкиВРегламентированномОтчете(Форма, КонтролирующийОрган);
		КонецЕсли;
	КонецЕсли;
	
	ПараметрыОповещения = Новый Структура(); 
	ПараметрыОповещения.Вставить("Ссылка", СсылкаНаОтчет);
	ПараметрыОповещения.Вставить("Организация", ОрганизацияОтчета);
	Оповестить("Завершение отправки в контролирующий орган", ПараметрыОповещения, );
	
КонецПроцедуры

Процедура ОтправкаВКонтролирующийОрганПослеСохраненияЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	СсылкаНаОтчет = ДополнительныеПараметры.СсылкаНаОтчет;
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	ДополнительныеПараметры.Вставить("КонтекстЭДОКлиент", КонтекстЭДОКлиент);
	
	ПараметрыФормы = Новый Структура("СсылкаНаОтчет", СсылкаНаОтчет);
	ОписаниеОповещения = Новый ОписаниеОповещения("ОтправкаВКонтролирующийОрганПодтверждениеОтправкиОтчетаЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	// при отправке из списка подтверждение запрашивается перед запуском отправки для предотвращения попадания сообщений в диалог подтверждения
	Если ДополнительныеПараметры.Форма = Неопределено ИЛИ ДополнительныеПараметры.Форма.ИмяФормы <> "ОбщаяФорма.РегламентированнаяОтчетность" Тогда
		ОткрытьФорму(КонтекстЭДОКлиент.ПутьКОбъекту + ".Форма.ПодтверждениеОтправкиОтчета", ПараметрыФормы,,,,, ОписаниеОповещения, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	Иначе
		ВыполнитьОбработкуОповещения(ОписаниеОповещения, КодВозвратаДиалога.ОК);
	КонецЕсли;
	
КонецПроцедуры

Процедура ОтправкаВКонтролирующийОрганПодтверждениеОтправкиОтчетаЗавершение(Результат, ВходящийКонтекст) Экспорт
	
	Если Результат <> КодВозвратаДиалога.ОК Тогда
		Возврат;
	КонецЕсли;
	
	ОрганизацияОтчета        = ВходящийКонтекст.ОрганизацияОтчета;
	ЭтоУведомлениеФНС        = ВходящийКонтекст.ЭтоУведомлениеФНС;
	ЭтоЖурналСчетовФактурФНС = ВходящийКонтекст.ЭтоЖурналСчетовФактурФНС;
	КонтекстЭДОКлиент        = ВходящийКонтекст.КонтекстЭДОКлиент;
	КонтролирующийОрган      = ВходящийКонтекст.КонтролирующийОрган;
	СсылкаНаОтчет = ВходящийКонтекст.СсылкаНаОтчет;
	
	Если ЭтоУведомлениеФНС Тогда
		Форма = ВходящийКонтекст.Форма;
		СсылкаНаОтчет = ВходящийКонтекст.СсылкаНаОтчет;
		КонтекстЭДОКлиент = ВходящийКонтекст.КонтекстЭДОКлиент;
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ПослеОтправкиУведомленияФНСЗавершение", ЭтотОбъект, ВходящийКонтекст);
		КонтекстЭДОКлиент.ОтправкаУведомлениеФНС(СсылкаНаОтчет, Форма.УникальныйИдентификатор, ОписаниеОповещения);
	ИначеЕсли ЭтоЖурналСчетовФактурФНС Тогда
		Форма = ВходящийКонтекст.Форма;
		СсылкаНаОтчет = ВходящийКонтекст.СсылкаНаОтчет;
		КонтекстЭДОКлиент = ВходящийКонтекст.КонтекстЭДОКлиент;
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ПослеОтправкиЖурналаСчетовФактурВФНСЗавершение", ЭтотОбъект, ВходящийКонтекст);
		КонтекстЭДОКлиент.ОтправкаЖурналаСчетовФактурВФНС(СсылкаНаОтчет, ОрганизацияОтчета, Форма.УникальныйИдентификатор, , ОписаниеОповещения);
	Иначе
		ОписаниеОповещения = Новый ОписаниеОповещения("ОтправкаВКонтролирующийОрганПослеОтправки", ЭтотОбъект, ВходящийКонтекст);
		
		// регистрируем заявку на отправку
		Если КонтролирующийОрган = "ФНС" Тогда
			КонтекстЭДОКлиент.ОтправкаРегламентированногоОтчетаВФНС(СсылкаНаОтчет, ОписаниеОповещения);
		ИначеЕсли КонтролирующийОрган = "ФСГС" Тогда
			КонтекстЭДОКлиент.ОтправкаРегламентированногоОтчетаВФСГС(СсылкаНаОтчет, ОписаниеОповещения);
		Иначе
			КонтекстЭДОКлиент.ОтправкаРегламентированногоОтчетаВПФР(СсылкаНаОтчет, ОписаниеОповещения);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОтправкаВКонтролирующийОрганПослеОтправки(РезультатОтправки, ДополнительныеПараметры) Экспорт
	
	ЭтоОтправкаИзФормыОтчетность = ДополнительныеПараметры.ЭтоОтправкаИзФормыОтчетность;
	Форма = ДополнительныеПараметры.Форма;
	КонтролирующийОрган = ДополнительныеПараметры.КонтролирующийОрган;
	СсылкаНаОтчет = ДополнительныеПараметры.СсылкаНаОтчет;
	ОрганизацияОтчета = ДополнительныеПараметры.ОрганизацияОтчета;
	КонтекстЭДОКлиент = ДополнительныеПараметры.КонтекстЭДОКлиент;
	
	КонтекстЭДОКлиент.ПредупредитьЕслиСтатусОтправкиВКонверте(СсылкаНаОтчет, "отчет");
	
	Если НЕ ЭтоОтправкаИзФормыОтчетность Тогда
		Если РезультатОтправки Тогда
			ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.ОбновитьПанельСостоянияОтправкиВРегламентированномОтчете(Форма, КонтролирующийОрган);
		КонецЕсли;
	КонецЕсли;
	
	ПараметрыОповещения = Новый Структура(); 
	ПараметрыОповещения.Вставить("Ссылка", СсылкаНаОтчет);
	ПараметрыОповещения.Вставить("Организация", ОрганизацияОтчета);
	Оповестить("Завершение отправки в контролирующий орган", ПараметрыОповещения, );
	
КонецПроцедуры

Процедура ПроверитьВИнтернете(Форма, КонтролирующийОрган = "ФНС") Экспорт
	
	СсылкаНаОтчет = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.СсылкаНаОтчетПоФорме(Форма);
	ТипЗнчСсылкаНаОтчет = ТипЗнч(СсылкаНаОтчет);
	ИмяДокументаУведомлениеОКонтролируемыхСделках 					= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("УведомлениеОКонтролируемыхСделках");
	ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде	= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде");
	ИмяДокументаИсходящееУведомлениеФНС					 			= "УведомлениеОСпецрежимахНалогообложения";
	
	ИмяДокументаЗаявлениеОВвозеТоваров 	= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("ЗаявлениеОВвозеТоваров");
	
	Если ИмяДокументаЗаявлениеОВвозеТоваров <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаЗаявлениеОВвозеТоваров) Тогда
		ЭтоЗаявлениеОВвозе = Истина;
	Иначе
		ЭтоЗаявлениеОВвозе = Ложь;
	КонецЕсли;
	
	Если (ИмяДокументаУведомлениеОКонтролируемыхСделках <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаУведомлениеОКонтролируемыхСделках))
		ИЛИ (ИмяДокументаИсходящееУведомлениеФНС <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаИсходящееУведомлениеФНС)) Тогда
		ЭтоУведомлениеФНС = Истина;
	Иначе
		ЭтоУведомлениеФНС = Ложь;
	КонецЕсли;
	
	Если ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде <> Неопределено И ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка." + ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде) Тогда
		ЭтоЖурналСчетовФактурФНС = Истина;
	Иначе
		ЭтоЖурналСчетовФактурФНС = Ложь;
	КонецЕсли;
	
	Если ТипЗнчСсылкаНаОтчет = Тип("ДокументСсылка.РегламентированныйОтчет")
		ИЛИ ТипЗнчСсылкаНаОтчет = Тип("Неопределено") Тогда
		
		// отправляем только из записанной формы
		Если Форма.Модифицированность Тогда
			
			ДополнительныеПараметры = Новый Структура("Форма, КонтролирующийОрган", Форма, КонтролирующийОрган);
			ОписаниеОповещения = Новый ОписаниеОповещения("ПроверитьВИнтернетеЗавершение", ЭтотОбъект, ДополнительныеПараметры);
			
			Форма.СохранитьНаКлиенте(, ОписаниеОповещения);
			
		Иначе
			ПроверитьВИнтернетеПослеСохранения(Форма, КонтролирующийОрган, СсылкаНаОтчет)
		КонецЕсли;
		
	ИначеЕсли ЭтоУведомлениеФНС Тогда
		
		Если НЕ ЗначениеЗаполнено(СсылкаНаОтчет) ИЛИ Форма.Модифицированность Тогда
			ПоказатьПредупреждение(,НСтр("ru = 'Перед проверкой необходимо записать уведомление.'"));
			Возврат;
		КонецЕсли;
		
		ПроверитьВИнтернетеПослеСохранения(Форма, КонтролирующийОрган, СсылкаНаОтчет)
		
	ИначеЕсли ЭтоЖурналСчетовФактурФНС Тогда
		
		Если НЕ ЗначениеЗаполнено(СсылкаНаОтчет) ИЛИ Форма.Модифицированность Тогда
			ПоказатьПредупреждение(,НСтр("ru = 'Перед проверкой необходимо записать журнал.'"));
			Возврат;
		КонецЕсли;
		
		ПроверитьВИнтернетеПослеСохранения(Форма, КонтролирующийОрган, СсылкаНаОтчет)
		
	ИначеЕсли ЭтоЗаявлениеОВвозе Тогда
		
		Если НЕ ЗначениеЗаполнено(СсылкаНаОтчет) ИЛИ Форма.Модифицированность Тогда
			ПоказатьПредупреждение(,НСтр("ru = 'Перед проверкой необходимо записать заявление.'"));
			Возврат;
		КонецЕсли;
		
		ПроверитьВИнтернетеПослеСохранения(Форма, КонтролирующийОрган, СсылкаНаОтчет)
		
	Иначе // все, кроме документа РегламентированныйОтчет
		
		ПроверитьВИнтернетеПослеСохранения(Форма, КонтролирующийОрган, СсылкаНаОтчет)
	
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьВИнтернетеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Форма = ДополнительныеПараметры.Форма;
	КонтролирующийОрган = ДополнительныеПараметры.КонтролирующийОрган;
	СсылкаНаОтчет = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСервер.СсылкаНаОтчетПоФорме(Форма);
	
	ПроверитьВИнтернетеПослеСохранения(Форма, КонтролирующийОрган, СсылкаНаОтчет)
	
КонецПроцедуры

Процедура ПроверитьВИнтернетеПослеСохранения(Форма, КонтролирующийОрган, СсылкаНаОтчет)
		
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("Форма", Форма);
	ДополнительныеПараметры.Вставить("КонтролирующийОрган", КонтролирующийОрган);
	ДополнительныеПараметры.Вставить("СсылкаНаОтчет", СсылкаНаОтчет);
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПроверитьВИнтернетеПослеПолученияКонтекста", ЭтотОбъект, ДополнительныеПараметры);
	ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
	
КонецПроцедуры

Процедура ПроверитьВИнтернетеПослеПолученияКонтекста(Результат, ДополнительныеПараметры) Экспорт
	
	СсылкаНаОтчет = ДополнительныеПараметры.СсылкаНаОтчет;
	КонтролирующийОрган = ДополнительныеПараметры.КонтролирующийОрган;
	Форма = ДополнительныеПараметры.Форма;
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	Если КонтекстЭДОКлиент <> Неопределено Тогда
		
		// Проверяем наличие учетной записи
		ОрганизацияОтчета = КонтекстЭДОКлиент.ОрганизацияОтчетаДляОнлайнПроверки(СсылкаНаОтчет);
		УчетнаяЗаписьОрганизации = КонтекстЭДОКлиент.УчетнаяЗаписьОрганизации(ОрганизацияОтчета);
		
		Если НЕ ЗначениеЗаполнено(УчетнаяЗаписьОрганизации) Тогда
			ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиент.ПоказатьФормуПредложениеОформитьЗаявлениеНаПодключение(ОрганизацияОтчета);
			Возврат;
		КонецЕсли;
		
		КонтекстЭДОКлиент.ПроверитьОтчетСИспользованиемСервисаОнлайнПроверки(СсылкаНаОтчет, КонтролирующийОрган, Истина, Форма);
	Иначе
		ПоказатьПредупреждение(,НСтр("ru = 'Недостаточно прав для использования методов электронного документооборота с контролирующими органами.'"));
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьВИнтернетеПоСсылке(Ссылка, КонтролирующийОрган = "ФНС") Экспорт
	
	ТипЗнчСсылка = ТипЗнч(Ссылка);
	ИмяДокументаУведомлениеОКонтролируемыхСделках 					= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("УведомлениеОКонтролируемыхСделках");
	ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде	= ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентСерверПереопределяемый.ИмяОбъектаМетаданных("ЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде");
	ИмяДокументаИсходящееУведомлениеФНС 							= "УведомлениеОСпецрежимахНалогообложения";
	
	Если (ИмяДокументаУведомлениеОКонтролируемыхСделках <> Неопределено И ТипЗнчСсылка = Тип("ДокументСсылка." + ИмяДокументаУведомлениеОКонтролируемыхСделках))
	ИЛИ (ИмяДокументаИсходящееУведомлениеФНС <> Неопределено И ТипЗнчСсылка = Тип("ДокументСсылка." + ИмяДокументаИсходящееУведомлениеФНС)) Тогда
		ЭтоУведомлениеФНС = Истина;
	Иначе
		ЭтоУведомлениеФНС = Ложь;
	КонецЕсли;
	
	Если ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде <> Неопределено И ТипЗнчСсылка = Тип("ДокументСсылка." + ИмяДокументаЖурналУчетаСчетовФактурДляПередачиВЭлектронномВиде) Тогда
		ЭтоЖурналСчетовФактурФНС = Истина;
	Иначе
		ЭтоЖурналСчетовФактурФНС = Ложь;
	КонецЕсли;
	
	ДополнительныеПараметры = Новый Структура("Ссылка, КонтролирующийОрган", Ссылка, КонтролирующийОрган);
	ОписаниеОповещения = Новый ОписаниеОповещения("ПроверитьВИнтернетеПоСсылкеЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
	
КонецПроцедуры

Процедура ПроверитьВИнтернетеПоСсылкеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Ссылка = ДополнительныеПараметры.Ссылка;
	КонтролирующийОрган = ДополнительныеПараметры.КонтролирующийОрган;
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	Если КонтекстЭДОКлиент <> Неопределено Тогда
		КонтекстЭДОКлиент.ПроверитьОтчетСИспользованиемСервисаОнлайнПроверки(Ссылка, КонтролирующийОрган);
	Иначе
		ПоказатьПредупреждение(,НСтр("ru = 'Недостаточно прав для использования методов электронного документооборота с контролирующими органами.'"));
	КонецЕсли;
	
КонецПроцедуры

Процедура ПослеЗапускаСистемы() Экспорт
	
	Если ПользователиКлиентСервер.ЭтоСеансВнешнегоПользователя() Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыРаботыКлиентаПриЗапуске = СтандартныеПодсистемыКлиентПовтИсп.ПараметрыРаботыКлиентаПриЗапуске();
	ТекущемуПользователюЭДОДоступен = ПараметрыРаботыКлиентаПриЗапуске.ДокументооборотСКонтролирующимиОрганами_ТекущемуПользователюЭДОДоступен;
	ТекущемуПользователюАОДоступен = ПараметрыРаботыКлиентаПриЗапуске.ДокументооборотСКонтролирующимиОрганами_ТекущемуПользователюАОДоступен;
	ВыбранныйCSPИзВременныхНастроек = ПараметрыРаботыКлиентаПриЗапуске.ДокументооборотСКонтролирующимиОрганами_ВыбранныйCSPИзВременныхНастроек;
	ИспользованиеЭлектроннойПодписиВМоделиСервисаВозможно = ПараметрыРаботыКлиентаПриЗапуске.ДокументооборотСКонтролирующимиОрганами_ИспользованиеЭлектроннойПодписиВМоделиСервисаВозможно;
	
	Если ТекущемуПользователюЭДОДоступен Тогда
		ПодключитьОбработчикОжидания("ПолучитьИнформациюОВходящихСообщенияхДляПользователяЭДО", 300, Истина);
	КонецЕсли;
	ПодключениеОбработчикаОжиданияАвтообмена(Истина, ТекущемуПользователюАОДоступен);
	ОткрытьМастерПодключенияК1СОтчетности(ВыбранныйCSPИзВременныхНастроек);
	Если ТекущемуПользователюЭДОДоступен Тогда
		ПодключитьОбработчикОжидания("ПредупредитьОбИстеченииСертификатов", 900, Истина);
		ПодключитьОбработчикОжидания("ПредупредитьОНекорректныхСтатусахОтправки2НДФЛ", 5 * 60, Истина);
	КонецЕсли;
	
	// Подключение обработчиков ожидания Электронной подписи в модели сервиса
	// при условии, что использование подсистемы возможно.
	Если ИспользованиеЭлектроннойПодписиВМоделиСервисаВозможно Тогда
		МодульЭлектроннаяПодписьВМоделиСервисаКлиент = ОбщегоНазначенияКлиентСервер.ОбщийМодуль("ЭлектроннаяПодписьВМоделиСервисаКлиент");
		МодульЭлектроннаяПодписьВМоделиСервисаКлиент.ПодключитьОбработчикПроверкиЗаявлений(10, Истина);
	КонецЕсли;
	
	ПараметрыРаботыКлиента = СтандартныеПодсистемыКлиентПовтИсп.ПараметрыРаботыКлиентаПриЗапуске();
	Если ПараметрыРаботыКлиента.РазделениеВключено Тогда
		ПроверитьКорректностьИспользованияАвтообменаСКО();
	КонецЕсли;
	
КонецПроцедуры

Процедура ПодключениеОбработчикаОжиданияАвтообмена(Подключать = Истина, ТекущемуПользователюАОДоступен = Неопределено) Экспорт
	
	Если Подключать Тогда
		// проверяем, является ли текущий пользователь пользователем ДО
		// проверяем отключение автообмена на уровне учетной записи документооборота
		Если ТекущемуПользователюАОДоступен = Истина ИЛИ (ТекущемуПользователюАОДоступен = Неопределено
			И ДокументооборотСКОВызовСервера.ТекущемуПользователюАОДоступен()) Тогда
			
			// если проверки пройдены, определяем интервал выполнения
			Интервал = ПолучитьИнтервалВыполнения();
			
			Если Интервал = Неопределено Тогда
				Возврат;
			КонецЕсли;
			
			ПодключитьОбработчикОжидания("ПолучитьИнформациюОВходящихСообщениях", Интервал);
			
		КонецЕсли;
		
	Иначе
		ОтключитьОбработчикОжидания("ПолучитьИнформациюОВходящихСообщениях");
	КонецЕсли;
	
КонецПроцедуры

Функция ПолучитьИнтервалВыполнения()
	
	// в разделенном режиме получить РЗ вне Области данных.
	Результат = 60*60*3; // 3(три) часа
	Возврат Результат;
	
КонецФункции

Процедура ОткрытьМастерПодключенияК1СОтчетности(СохраненныеНастройки)
	
	// Если пользователь в мастере подключения остановился на шаге установки криптопровайдеров (шаг 2)
	// и после установки криптопровайдера перезагрузил компьютер, то необходимо открыть мастер для продолжения
	// подключения к 1С-Отчетности
	Если СохраненныеНастройки <> Неопределено Тогда
		
		ДополнительныеПараметры = Новый Структура("СохраненныеНастройки", СохраненныеНастройки);
		ОписаниеОповещения = Новый ОписаниеОповещения("ОткрытьМастерПодключенияК1СОтчетностиЗавершение", ЭтотОбъект, ДополнительныеПараметры);
		ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОткрытьМастерПодключенияК1СОтчетностиЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	СохраненныеНастройки = ДополнительныеПараметры.СохраненныеНастройки;
	
	Если КонтекстЭДОКлиент <> Неопределено Тогда
		
		Если ТипЗнч(СохраненныеНастройки) = Тип("СправочникСсылка.Организации") Тогда
			ПараметрыФормы = Новый Структура();
			ПараметрыФормы.Вставить("Организация", СохраненныеНастройки);
			ОткрытьФорму(КонтекстЭДОКлиент.ПутьКОбъекту + ".Форма.МастерФормированияЗаявкиНаПодключение", ПараметрыФормы);
		Иначе
			ОткрытьФорму(КонтекстЭДОКлиент.ПутьКОбъекту + ".Форма.МастерФормированияЗаявкиНаПодключение");
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПолучитьИнформациюОВходящихСообщенияхЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	Если КонтекстЭДОКлиент = Неопределено Тогда
		Возврат;
	КонецЕсли;
	КонтекстЭДОКлиент.ПолучитьИОбработатьВходящиеКлиент();
	
КонецПроцедуры

Процедура ПолучитьИнформациюОВходящихСообщенияхДляПользователяЭДОЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	Если КонтекстЭДОКлиент = Неопределено Тогда
		Возврат;
	КонецЕсли;
	КонтекстЭДОКлиент.ПолучитьИОбработатьВходящиеКлиент();
	
КонецПроцедуры

Процедура ПредупредитьОбИстеченииСертификатовЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	Если КонтекстЭДОКлиент = Неопределено Тогда
		Возврат;
	КонецЕсли;
	КонтекстЭДОКлиент.ПредупредитьОбИстеченииСертификатовКлиент();
	
КонецПроцедуры

Процедура ПредупредитьОНекорректныхСтатусахОтправки2НДФЛПослеПолученияКонтекста(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	Если КонтекстЭДОКлиент = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	КонтекстЭДОКлиент.ПредупредитьОбОшибкеВСтатусахОтправки2НДФЛ();
	
КонецПроцедуры

Процедура СоздатьЭлектронноеПредставлениеРегламентированныхОтчетовИзФайлов(Файлы, Адрес) Экспорт
	
	МассивФайлов = Новый Массив;
	Если ТипЗнч(Файлы) = Тип("Файл") Тогда
		МассивФайлов.Добавить(Файлы.ПолноеИмя);
	ИначеЕсли ТипЗнч(Файлы) = Тип("Массив") Тогда
		Для Каждого Значение Из Файлы Цикл
			Если ТипЗнч(Значение) = Тип("Файл") Тогда
				МассивФайлов.Добавить(Значение.ПолноеИмя);
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Если МассивФайлов.Количество() > 0 Тогда
		ФайлыИмпорта = СтроковыеФункцииКлиентСервер.СтрокаИзМассиваПодстрок(МассивФайлов, Символы.ПС);
				
		ДополнительныеПараметры = Новый Структура("Адрес, ФайлыИмпорта", Адрес, ФайлыИмпорта);
		ОписаниеОповещения = Новый ОписаниеОповещения("СоздатьЭлектронноеПредставлениеРегламентированныхОтчетовИзФайловЗавершение", ЭтотОбъект, ДополнительныеПараметры);
		ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
	КонецЕсли;
	
КонецПроцедуры

Процедура СоздатьЭлектронноеПредставлениеРегламентированныхОтчетовИзФайловЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	КонтекстЭДОКлиент.ПолучениеФайловДляИмпортаНачало(ДополнительныеПараметры.Адрес, ДополнительныеПараметры.ФайлыИмпорта);
		
КонецПроцедуры

#Область АвтоматическийОбменСКонтролирующимиОрганами

Процедура ВопросЗавершение(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ = "Включить" Тогда
		ДокументооборотСКОВызовСервера.ВключитьАвтообменПоУчетнымЗаписяхДО(ДополнительныеПараметры.УчетныеЗаписи);
	Иначе
		ДокументооборотСКОВызовСервера.ОтключитьАвтообменПоУчетнымЗаписяхДО(ДополнительныеПараметры.УчетныеЗаписи);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьКорректностьИспользованияАвтообменаСКО()
	
	ОбластьДанных = Неопределено;
	УчетныеЗаписи = Неопределено;
	Если Не ДокументооборотСКОВызовСервера.ИспользованиеАвтообменаСКОНастроеноКорректно(ОбластьДанных, УчетныеЗаписи) Тогда
		СписокКнопок = Новый СписокЗначений;
		СписокКнопок.Добавить("Отключить");
		СписокКнопок.Добавить("Включить", НСтр("ru = 'Включить'"));
		
		МассивСтрок = Новый Массив;
		Для Каждого УчетнаяЗапись Из УчетныеЗаписи Цикл
			ФСтрока = Новый ФорматированнаяСтрока(Строка(УчетнаяЗапись),,,, ПолучитьНавигационнуюСсылку(УчетнаяЗапись));
			МассивСтрок.Добавить(ФСтрока);
			МассивСтрок.Добавить(Символы.ПС);
			МассивСтрок.Добавить(Символы.ПС);
		КонецЦикла;
		
		МассивСтрок.Добавить(Символы.ПС);
		
		ТекстВопроса1 = НСтр("ru = 'Для учетных записей:
                              |'");
							
		ТекстВопроса2 = НСтр("ru = 'уже настроено использование автообмена с контролирующими органами в области данных %1.
                              |Корректная работа гарантируется только при использовании автообмена одновременно в одной области данных.
                              |
                              |Автообмен в этой области данных будет отключен?'");
		ТекстВопроса2 = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстВопроса2, ОбластьДанных);
		
		ТекстВопроса = Новый ФорматированнаяСтрока(ТекстВопроса1, МассивСтрок, ТекстВопроса2);
		
		ОписаниеЗавершения = Новый ОписаниеОповещения("ВопросЗавершение", ДокументооборотСКОКлиент, Новый Структура("УчетныеЗаписи", УчетныеЗаписи));
		
		ПоказатьВопрос(
			ОписаниеЗавершения, 
			ТекстВопроса, 
			СписокКнопок, 
			, 
			"Отключить", 
			НСтр("ru = 'Обнаружена некорректная настройка автообмена с контролирующими органами'"));
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти