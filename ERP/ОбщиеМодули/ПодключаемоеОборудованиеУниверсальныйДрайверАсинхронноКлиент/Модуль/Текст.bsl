﻿                                            
#Область ПрограммныйИнтерфейс

Процедура НачатьПодключенияУстройствоПолучитьОшибкуЗавершение(РезультатВыполнения, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	ВыходныеПараметры = ДополнительныеПараметры.ВыходныеПараметры;
	ВыходныеПараметры.Очистить();
	ВыходныеПараметры.Добавить(999);
	ВыходныеПараметры.Добавить(ПараметрыВызова[0]);
	
	РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Ложь, ВыходныеПараметры);
	Если ДополнительныеПараметры.ОповещениеПриЗавершении <> Неопределено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриЗавершении, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

Процедура НачатьПодключениеУстройстваПодключитьЗавершение(РезультатВыполнения, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	Если НЕ РезультатВыполнения Тогда
		ОповещениеПодключитьЗавершение = Новый ОписаниеОповещения("НачатьПодключенияУстройствоПолучитьОшибкуЗавершение", ЭтотОбъект, ДополнительныеПараметры);
		Попытка
			ДополнительныеПараметры.ОбъектДрайвера.НачатьВызовПолучитьОшибку(ОповещениеПодключитьЗавершение, ПараметрыВызова[0]) 
		Исключение
			ВыходныеПараметры = ДополнительныеПараметры.ВыходныеПараметры;
			ВыходныеПараметры.Очистить();
			ВыходныеПараметры.Добавить(999);
			ВыходныеПараметры.Добавить(НСтр("ru='Ошибка вызова метода <ОбъектДрайвера.ПолучитьОшибку>.'") + Символы.ПС + ОписаниеОшибки());
			РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Ложь, ВыходныеПараметры);
			Если ДополнительныеПараметры.ОповещениеПриЗавершении <> Неопределено Тогда
				ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриЗавершении, РезультатВыполнения);
			КонецЕсли;
		КонецПопытки;
	Иначе
		ДополнительныеПараметры.ПараметрыПодключения.ИДУстройства = ПараметрыВызова[0];
		ВыходныеПараметры = ДополнительныеПараметры.ВыходныеПараметры;
		ПараметрыПодключения = ДополнительныеПараметры.ПараметрыПодключения;
		
		Если ПараметрыПодключения.ТипОборудования = "ЭквайринговыйТерминал" Тогда
			ПараметрыПодключения.Вставить("КодОригинальнойТранзакции", Неопределено);
			ПараметрыПодключения.Вставить("ТипТранзакции", "");
		ИначеЕсли ПараметрыПодключения.ТипОборудования = "СканерШтрихкода" Тогда
			ВыходныеПараметры.Добавить(Строка(ПараметрыВызова[0]));
			ВыходныеПараметры.Добавить(Новый Массив());
			ВыходныеПараметры[1].Добавить("Штрихкод");
			ВыходныеПараметры[1].Добавить("Barcode");
		ИначеЕсли ПараметрыПодключения.ТипОборудования = "СчитывательМагнитныхКарт" Тогда
			ВыходныеПараметры.Добавить(Строка(ПараметрыВызова[0]));
			ВыходныеПараметры.Добавить(Новый Массив());
			ВыходныеПараметры[1].Добавить("ДанныеКарты");
			ВыходныеПараметры[1].Добавить("TracksData");
		КонецЕсли;  
		
		РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Истина, ВыходныеПараметры);
		Если ДополнительныеПараметры.ОповещениеПриЗавершении <> Неопределено Тогда
			ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриЗавершении, РезультатВыполнения);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура НачатьПодключенияУстройствоУстановкаПараметровЗавершение(Результат, Параметры) Экспорт

	ОповещениеПодключитьЗавершение = Новый ОписаниеОповещения("НачатьПодключениеУстройстваПодключитьЗавершение", ЭтотОбъект, Параметры);
	Попытка
		Параметры.ОбъектДрайвера.НачатьВызовПодключить(ОповещениеПодключитьЗавершение, Параметры.ПараметрыПодключения.ИДУстройства) 
	Исключение
		ВыходныеПараметры = Параметры.ВыходныеПараметры;
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(999);
		ВыходныеПараметры.Добавить(НСтр("ru='Ошибка вызова метода <ОбъектДрайвера.Подключить>.'") + Символы.ПС + ОписаниеОшибки());
		РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Ложь, ВыходныеПараметры);
		Если Параметры.ОповещениеПриЗавершении <> Неопределено Тогда
			ВыполнитьОбработкуОповещения(Параметры.ОповещениеПриЗавершении, РезультатВыполнения);
		КонецЕсли;
	КонецПопытки;
	
КонецПроцедуры

// Функция начинает подключение устройства.
//
Процедура НачатьПодключениеУстройства(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ДополнительныеПараметры) Экспорт
	
	ПараметрыПодключения.Вставить("ИДУстройства", Неопределено);
	ВыходныеПараметры = Новый Массив();
	
	ДополнительныеПараметры.Вставить("ОбъектДрайвера"         , ОбъектДрайвера);
	ДополнительныеПараметры.Вставить("Параметры"              , Параметры);
	ДополнительныеПараметры.Вставить("ПараметрыПодключения"   , ПараметрыПодключения);
	ДополнительныеПараметры.Вставить("ВыходныеПараметры"      , ВыходныеПараметры);
	ДополнительныеПараметры.Вставить("ОповещениеПриЗавершении", ОповещениеПриЗавершении);
	
	ОповещениеПриУстановкеПараметров = Новый ОписаниеОповещения("НачатьПодключенияУстройствоУстановкаПараметровЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	НачатьУстановкуПараметров(ОповещениеПриУстановкеПараметров, ДополнительныеПараметры);
	
КонецПроцедуры

Процедура НачатьОтключенияУстройстваЗавершение(РезультатВыполнения, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	ВыходныеПараметры = Новый Массив();
	
	Если РезультатВыполнения Тогда
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(0);
	Иначе
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(999);
	КонецЕсли;
	
	РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", РезультатВыполнения, ВыходныеПараметры);
	Если ДополнительныеПараметры.ОповещениеПриЗавершении <> Неопределено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриЗавершении, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Функция начинает отключение устройства.
//
Процедура НачатьОтключенияУстройства(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры) Экспорт
	
	ПараметрыКоманды = Новый Структура();
	ПараметрыКоманды.Вставить("ОповещениеПриЗавершении", ОповещениеПриЗавершении);
	ПараметрыКоманды.Вставить("ОбъектДрайвера"         , ОбъектДрайвера);
	ПараметрыКоманды.Вставить("Параметры"              , Параметры);
	ПараметрыКоманды.Вставить("ПараметрыПодключения"   , ПараметрыПодключения);
	ПараметрыКоманды.Вставить("ВыходныеПараметры"      , ВыходныеПараметры);
	ОповещениеМетода = Новый ОписаниеОповещения("НачатьОтключенияУстройстваЗавершение", ЭтотОбъект, ПараметрыКоманды);
	
	Попытка
		ОбъектДрайвера.НачатьВызовОтключить(ОповещениеМетода, ПараметрыПодключения.ИДУстройства);
	Исключение
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(999);
		ВыходныеПараметры.Добавить(НСтр("ru='Ошибка вызова метода <ОбъектДрайвера.Отключить>.'") + Символы.ПС + ОписаниеОшибки());
		РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Ложь, ВыходныеПараметры);
		Если ОповещениеПриЗавершении <> Неопределено Тогда
			ВыполнитьОбработкуОповещения(ОповещениеПриЗавершении, РезультатВыполнения);
		КонецЕсли;
	КонецПопытки;
	
КонецПроцедуры

// Процедура начинает выполнение команды, обрабатывает и перенаправляет на исполнение команду к драйверу.
//
Процедура НачатьВыполнениеКоманды(ОповещениеПриЗавершении, Команда, ВходныеПараметры = Неопределено, ОбъектДрайвера, Параметры, ПараметрыПодключения) Экспорт
	
	ВыходныеПараметры = Новый Массив();
	
	// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩИЕ ДЛЯ ВСЕХ ТИПОВ ДРАЙВЕРОВ
	
	// Тестирование устройства
	Если Команда = "ТестУстройства" ИЛИ Команда = "CheckHealth" Тогда
		НачатьТестУстройства(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
		
	ИначеЕсли Команда = "ВыполнитьДополнительноеДействие" ИЛИ Команда = "DoAdditionalAction" Тогда
		ИмяДействия = ВходныеПараметры[0];
		НачатьВыполнитьДополнительноеДействие(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ИмяДействия, ВыходныеПараметры);
		
	// Получение версии драйвера
	ИначеЕсли Команда = "ПолучитьВерсиюДрайвера" ИЛИ Команда = "GetVersion" Тогда
		НачатьПолучениеВерсииДрайвера(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
		
	// Получение описание драйвера.
	ИначеЕсли Команда = "ПолучитьОписаниеДрайвера" ИЛИ Команда = "GetDescription" Тогда
		НачатьПолучениеОписаниеДрайвера(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
		
	// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩИЕ ДЛЯ РАБОТЫ С УСТРОЙСВАМИ ВВОДА ДАННЫХ
	
	// Обработка события от устройства.
	ИначеЕсли Команда = "ОбработатьСобытие" Тогда
		Событие = ВходныеПараметры[0];
		Данные  = ВходныеПараметры[1];
		Результат = ОбработатьСобытие(ОбъектДрайвера, Параметры, ПараметрыПодключения, Событие, Данные, ВыходныеПараметры);
		
	Иначе
		ВыходныеПараметры.Добавить(999);
		ВыходныеПараметры.Добавить(НСтр("ru='Команда ""%Команда%"" не поддерживается данным драйвером.'"));
		ВыходныеПараметры[1] = СтрЗаменить(ВыходныеПараметры[1], "%Команда%", Команда);
		РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Ложь, ВыходныеПараметры);
		Если ОповещениеПриЗавершении <> Неопределено Тогда
			ВыполнитьОбработкуОповещения(ОповещениеПриЗавершении, РезультатВыполнения);
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область ПроцедурыИФункцииОбщиеДляУстройствВводаДанных

// Функция осуществляет обработку внешних событий подключаемого оборудования.
//
Функция ОбработатьСобытие(ОбъектДрайвера, Параметры, ПараметрыПодключения, Событие, Данные, ВыходныеПараметры) Экспорт
	
	Результат = Истина;
	
	Если Событие = "Штрихкод" Или Событие = "Barcode" Тогда
		
		Штрихкод = СокрЛП(Данные);
		ВыходныеПараметры.Добавить("ScanData");
		ВыходныеПараметры.Добавить(Новый Массив());
		ВыходныеПараметры[1].Добавить(Штрихкод);
		ВыходныеПараметры[1].Добавить(Новый Массив());
		ВыходныеПараметры[1][1].Добавить(Данные);
		ВыходныеПараметры[1][1].Добавить(Штрихкод);
		ВыходныеПараметры[1][1].Добавить(0);
		Результат = Истина;
		
	ИначеЕсли Событие = "ДанныеКарты" Или Событие = "TracksData" Тогда
		
		ДанныеКарты = СокрЛП(Данные);
		ВыходныеПараметры.Добавить("TracksData");
		ВыходныеПараметры.Добавить(Новый Массив());
		ВыходныеПараметры[1].Добавить(ДанныеКарты);
		ВыходныеПараметры[1].Добавить(Новый Массив());
		ВыходныеПараметры[1][1].Добавить(Данные);
		ВыходныеПараметры[1][1].Добавить(ДанныеКарты);
		ВыходныеПараметры[1][1].Добавить(0);
		Результат = Истина;
		
	КонецЕсли;
	
	Возврат Результат;

КонецФункции

#КонецОбласти

#Область ПроцедурыИФункцииОбщиеДляВсехТиповДрайверов

// Процедура возвращает версию установленного драйвера.
//
Процедура НачатьПолучениеВерсииДрайвераЗавершение(РезультатВызова, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	ДополнительныеПараметры.ВыходныеПараметры[1] = РезультатВызова;
	РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Истина, ДополнительныеПараметры.ВыходныеПараметры);
	
	Если ДополнительныеПараметры.ОповещениеПриЗавершении <> Неопределено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриЗавершении, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Процедура возвращает версию установленного драйвера.
//
Процедура НачатьПолучениеВерсииДрайвера(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры) Экспорт

	ВыходныеПараметры.Очистить();
	ВыходныеПараметры.Добавить(НСтр("ru='Установлен'"));
	ВыходныеПараметры.Добавить(НСтр("ru='Не определена'"));
	
	ПараметрыКоманды = Новый Структура();
	ПараметрыКоманды.Вставить("ОповещениеПриЗавершении", ОповещениеПриЗавершении);
	ПараметрыКоманды.Вставить("ОбъектДрайвера"         , ОбъектДрайвера);
	ПараметрыКоманды.Вставить("Параметры"              , Параметры);
	ПараметрыКоманды.Вставить("ПараметрыПодключения"   , ПараметрыПодключения);
	ПараметрыКоманды.Вставить("ВыходныеПараметры"      , ВыходныеПараметры);
	ОповещениеМетода = Новый ОписаниеОповещения("НачатьПолучениеВерсииДрайвераЗавершение", ЭтотОбъект, ПараметрыКоманды);
	
	Попытка
		ОбъектДрайвера.НачатьВызовПолучитьНомерВерсии(ОповещениеМетода);
	Исключение
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(999);
		ВыходныеПараметры.Добавить(НСтр("ru='Ошибка вызова метода <ОбъектДрайвера.ПолучитьНомерВерсии>.'") + Символы.ПС + ОписаниеОшибки());
		РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Ложь, ВыходныеПараметры);
		Если ОповещениеПриЗавершении <> Неопределено Тогда
			ВыполнитьОбработкуОповещения(ОповещениеПриЗавершении, РезультатВыполнения);
		КонецЕсли;
	КонецПопытки;
	
КонецПроцедуры

// Процедура завершения установки параметров драйвера.
//
Процедура НачатьУстановкуПараметровЗавершение(РезультатВыполнения, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	Если Не ТипЗнч(ДополнительныеПараметры.ПараметрыДляУстановки) = Тип("Структура") Тогда
		Возврат;
	КонецЕсли;
	
	Если ДополнительныеПараметры.ПараметрыДляУстановки.Количество() > 0  Тогда
		Для Каждого Параметр Из ДополнительныеПараметры.ПараметрыДляУстановки Цикл
			ИмяТекПараметра = Параметр.Ключ;
			ЗначениеПараметра = Параметр.Значение;
			ДополнительныеПараметры.ПараметрыДляУстановки.Удалить(ИмяТекПараметра);
			ОповещениеМетода = Новый ОписаниеОповещения("НачатьУстановкуПараметровЗавершение", ЭтотОбъект, ДополнительныеПараметры);
			ДополнительныеПараметры.ОбъектДрайвера.НачатьВызовУстановитьПараметр(ОповещениеМетода, Сред(ИмяТекПараметра, 3), ЗначениеПараметра);
			Прервать;
		КонецЦикла;
	Иначе
		Если ДополнительныеПараметры.ОповещениеПриУстановкеПараметров <> Неопределено Тогда
			ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриУстановкеПараметров, ДополнительныеПараметры);
		КонецЕсли;   
	КонецЕсли;
	
КонецПроцедуры

// Процедура устанавливает параметры драйвера.
//
Процедура НачатьУстановкуПараметров(ОповещениеПриУстановкеПараметров, ДополнительныеПараметры) Экспорт
	
	ВремПараметры = Новый Структура();
	Если ДополнительныеПараметры.ПараметрыПодключения.Свойство("ТипОборудования") Тогда
		ТипОборудования = ДополнительныеПараметры.ПараметрыПодключения.ТипОборудования;
		// Предопределенный параметр с указанием типа драйвера.
		ВремПараметры.Вставить("P_EquipmentType", ТипОборудования) 
	КонецЕсли;
	
	Для Каждого Параметр Из ДополнительныеПараметры.Параметры Цикл
		Если Лев(Параметр.Ключ, 2) = "P_" Тогда
			ВремПараметры.Вставить(Параметр.Ключ, Параметр.Значение);
		КонецЕсли;
	КонецЦикла;
	
	ДополнительныеПараметры.Вставить("ПараметрыДляУстановки", ВремПараметры);
	ДополнительныеПараметры.Вставить("ОповещениеПриУстановкеПараметров", ОповещениеПриУстановкеПараметров);
	НачатьУстановкуПараметровЗавершение(Истина, Неопределено, ДополнительныеПараметры);
	
КонецПроцедуры

// Процедура завершения тестирование устройства.
//
Процедура НачатьТестУстройстваУстановкаПараметровЗавершение(Результат, Параметры) Экспорт
	
	РезультатТеста       = "";
	АктивированДемоРежим = "";  
	
	Попытка
		ОповещениеПриЗавершении = Новый ОписаниеОповещения("НачатьТестУстройстваЗавершение", ЭтотОбъект, Параметры);
		Параметры.ОбъектДрайвера.НачатьВызовТестУстройства(ОповещениеПриЗавершении, РезультатТеста, АктивированДемоРежим);
	Исключение
		ВыходныеПараметры = Параметры.ВыходныеПараметры;
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(999);
		ВыходныеПараметры.Добавить(НСтр("ru='Ошибка вызова метода <ОбъектДрайвера.ТестУстройства>.'") + Символы.ПС + ОписаниеОшибки());
		РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Ложь, ВыходныеПараметры);
		Если Параметры.ОповещениеПриЗавершении <> Неопределено Тогда
			ВыполнитьОбработкуОповещения(Параметры.ОповещениеПриЗавершении, РезультатВыполнения);
		КонецЕсли;
	КонецПопытки;
	
КонецПроцедуры

// Процедура осуществляет тестирование устройства.
//
Процедура НачатьТестУстройстваЗавершение(РезультатВыполнения, Параметры, ДополнительныеПараметры) Экспорт
	
	ВыходныеПараметры = ДополнительныеПараметры.ВыходныеПараметры;
	
	Если РезультатВыполнения Тогда
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(0);
	Иначе
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(999);
	КонецЕсли;
	ВыходныеПараметры.Добавить(Параметры[0]);
	ВыходныеПараметры.Добавить(Параметры[1]);
	
	РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", РезультатВыполнения, ВыходныеПараметры);
	Если ДополнительныеПараметры.ОповещениеПриЗавершении <> Неопределено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриЗавершении, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Процедура осуществляет тестирование устройства.
//
Процедура НачатьТестУстройства(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры)

	ДополнительныеПараметры = Новый Структура();
	ДополнительныеПараметры.Вставить("ОбъектДрайвера"         , ОбъектДрайвера);
	ДополнительныеПараметры.Вставить("Параметры"              , Параметры);
	ДополнительныеПараметры.Вставить("ПараметрыПодключения"   , ПараметрыПодключения);
	ДополнительныеПараметры.Вставить("ВыходныеПараметры"      , ВыходныеПараметры);
	ДополнительныеПараметры.Вставить("ОповещениеПриЗавершении", ОповещениеПриЗавершении);
	
	ОповещениеПриУстановкеПараметров = Новый ОписаниеОповещения("НачатьТестУстройстваУстановкаПараметровЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	НачатьУстановкуПараметров(ОповещениеПриУстановкеПараметров, ДополнительныеПараметры);
	
КонецПроцедуры

// Функция осуществляет выполнение дополнительного действия для устройства.
//
Процедура НачатьВыполнитьДополнительноеДействие(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ИмяДействия, ВыходныеПараметры);
	
	Для Каждого Параметр Из Параметры Цикл
		Если Лев(Параметр.Ключ, 2) = "P_" Тогда
			ЗначениеПараметра = Параметр.Значение;
			ИмяПараметра = Сред(Параметр.Ключ, 3);
			Ответ = ОбъектДрайвера.УстановитьПараметр(ИмяПараметра, ЗначениеПараметра) 
		КонецЕсли;
	КонецЦикла;
	
	Попытка
		Ответ = ОбъектДрайвера.ВыполнитьДополнительноеДействие(ИмяДействия);
		Если НЕ Ответ Тогда
			Результат = Ложь;
			ВыходныеПараметры.Очистить();
			ВыходныеПараметры.Добавить(999);
			ВыходныеПараметры.Добавить("");
			ОбъектДрайвера.ПолучитьОшибку(ВыходныеПараметры[1])
		Иначе
			ВыходныеПараметры.Очистить();  
		КонецЕсли;
	Исключение
		ВыходныеПараметры.Очистить();
		ВыходныеПараметры.Добавить(999);
		ВыходныеПараметры.Добавить(НСтр("ru='Ошибка вызова метода <ОбъектДрайвера.ВыполнитьДополнительноеДействие>.'") + Символы.ПС + ОписаниеОшибки());
	КонецПопытки;
	
КонецПроцедуры

// Процедура возвращает описание установленного драйвера.
//
Процедура НачатьПолучениеОписаниеДрайвера(ОповещениеПриЗавершении, ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры)
	
	ВыходныеПараметры.Очистить();
	ВыходныеПараметры.Добавить(НСтр("ru='Установлен'"));
	ВыходныеПараметры.Добавить(НСтр("ru='Не определена'"));
	ВыходныеПараметры.Добавить(НСтр("ru='Не определено'"));
	ВыходныеПараметры.Добавить(НСтр("ru='Не определено'"));
	ВыходныеПараметры.Добавить(НСтр("ru='Не определено'"));
	ВыходныеПараметры.Добавить(Неопределено);
	ВыходныеПараметры.Добавить(Неопределено);
	ВыходныеПараметры.Добавить(Неопределено);
	ВыходныеПараметры.Добавить(Неопределено);
	ВыходныеПараметры.Добавить(Неопределено);
	ВыходныеПараметры.Добавить(Неопределено);

	ПараметрыКоманды = Новый Структура();
	ПараметрыКоманды.Вставить("ОповещениеПриЗавершении", ОповещениеПриЗавершении);
	ПараметрыКоманды.Вставить("ОбъектДрайвера"         , ОбъектДрайвера);
	ПараметрыКоманды.Вставить("Параметры"              , Параметры);
	ПараметрыКоманды.Вставить("ПараметрыПодключения"   , ПараметрыПодключения);
	ПараметрыКоманды.Вставить("ВыходныеПараметры"      , ВыходныеПараметры);
	
	ОповещениеМетода = Новый ОписаниеОповещения("НачатьПолучениеОписаниеДрайвераВерсияЗавершение", ЭтотОбъект, ПараметрыКоманды);
	Попытка
		ОбъектДрайвера.НачатьВызовПолучитьНомерВерсии(ОповещениеМетода);
	Исключение
	КонецПопытки;
	
КонецПроцедуры

Процедура НачатьПолучениеОписаниеДрайвераВерсияЗавершение(РезультатВызова, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	ВыходныеПараметры = ДополнительныеПараметры.ВыходныеПараметры;
	ВыходныеПараметры[1] = РезультатВызова;
	
	НаименованиеДрайвера      = "";
	ОписаниеДрайвера          = "";
	ТипОборудования           = "";
	ИнтеграционнаяБиблиотека  = Истина;
	ОсновнойДрайверУстановлен = Ложь;
	РевизияИнтерфейса         = 1012;
	URLЗагрузкиДрайвера       = "";
	
	ОповещениеМетода = Новый ОписаниеОповещения("НачатьПолучениеОписаниеДрайвераПолучитьОписаниеЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	Попытка
		ДополнительныеПараметры.ОбъектДрайвера.НачатьВызовПолучитьОписание(ОповещениеМетода, НаименованиеДрайвера, ОписаниеДрайвера, ТипОборудования, РевизияИнтерфейса, 
									ИнтеграционнаяБиблиотека, ОсновнойДрайверУстановлен, URLЗагрузкиДрайвера);
	Исключение
	КонецПопытки;

КонецПроцедуры

Процедура НачатьПолучениеОписаниеДрайвераПолучитьОписаниеЗавершение(РезультатВызова, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	ВыходныеПараметры = ДополнительныеПараметры.ВыходныеПараметры;
	ВыходныеПараметры[2] = ПараметрыВызова[0]; // НаименованиеДрайвера;
	ВыходныеПараметры[3] = ПараметрыВызова[1]; // ОписаниеДрайвера;
	ВыходныеПараметры[4] = ПараметрыВызова[2]; // ТипОборудования;
	ВыходныеПараметры[5] = ПараметрыВызова[3]; // РевизияИнтерфейса;
	ВыходныеПараметры[6] = ПараметрыВызова[4]; // ИнтеграционнаяБиблиотека;
	ВыходныеПараметры[7] = ПараметрыВызова[5]; // ОсновнойДрайверУстановлен;
	ВыходныеПараметры[8] = ПараметрыВызова[6]; // URLЗагрузкиДрайвера;
	
	ПараметрыДрайвера = "";
	ОповещениеМетода = Новый ОписаниеОповещения("НачатьПолучениеОписаниеДрайвераПолучитьПараметрыЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	Попытка
		ДополнительныеПараметры.ОбъектДрайвера.НачатьВызовПолучитьПараметры(ОповещениеМетода, ПараметрыДрайвера);
	Исключение
	КонецПопытки;
	
КонецПроцедуры

Процедура НачатьПолучениеОписаниеДрайвераПолучитьПараметрыЗавершение(РезультатВызова, ПараметрыВызова, ДополнительныеПараметры) Экспорт
	
	ВыходныеПараметры = ДополнительныеПараметры.ВыходныеПараметры;
	ВыходныеПараметры[9] = ПараметрыВызова[0];
	
	РезультатВыполнения = Новый Структура("Результат, ВыходныеПараметры", Истина, ДополнительныеПараметры.ВыходныеПараметры);
	Если ДополнительныеПараметры.ОповещениеПриЗавершении <> Неопределено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеПриЗавершении, РезультатВыполнения);
	КонецЕсли;

КонецПроцедуры

#КонецОбласти     