﻿
#Область ПрограммныйИнтерфейс

//Создает список доступных для создания планов обмена
Процедура СписокПлановОбмена(ПланыОбменаПодсистемы) Экспорт
	
	ПланыОбменаПодсистемы.Добавить(Метаданные.ПланыОбмена.ОбменУправлениеПредприятиемДокументооборот);
	ПланыОбменаПодсистемы.Добавить(Метаданные.ПланыОбмена.ОбменУправлениеПредприятиемДокументооборот20);
	ПланыОбменаПодсистемы.Добавить(Метаданные.ПланыОбмена.ОбменУправлениеПредприятиемЗарплатаИУправлениеПерсоналом25);
	ПланыОбменаПодсистемы.Добавить(Метаданные.ПланыОбмена.СинхронизацияДанныхЧерезУниверсальныйФормат);
	
КонецПроцедуры

//Возвращает признак доступности плана обмена, для базовой или проф версии
Функция ПроверкаВозможностиСозданияУзлаОбмена(ПланОбмена) Экспорт
	
	Если СтандартныеПодсистемыСервер.ЭтоБазоваяВерсияКонфигурации() Тогда
		Возврат ДоступностьПланаОбменаВБазовойВерсии(ПланОбмена.Имя);
	КонецЕсли;

	Возврат Истина;
	
КонецФункции

// Блокирует изменение настроек узла плана обмена
//
Процедура УстановитьДоступностьНастроекУзлаИнформационнойБазы(Форма) Экспорт
	
	Если ОбменДаннымиПовтИспУТУП.ЭтоПодчиненныйУзелРИБСОтбором() Тогда
	
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Форма.Элементы,
			"ГруппаПояснениеБлокировки",
			"Видимость",
			Истина);

		Для Каждого ЭлементФормы Из Форма.Элементы Цикл
			
			Если ТипЗнч(ЭлементФормы) = Тип("ПолеФормы") Тогда
				
				ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(
					Форма.Элементы,
					ЭлементФормы.Имя,
					"Доступность",
					Ложь);
					
			КонецЕсли;
				
		КонецЦикла; 
		
	КонецЕсли;
	
КонецПроцедуры

Процедура РегистрацияИзмененияДляНачальнойВыгрузки(Получатель, СтандартнаяОбработка, Отбор) Экспорт
	
	Если ТипЗнч(Получатель) = Тип("ПланОбменаСсылка.ОбменУправлениеПредприятиемЗарплатаИУправлениеПерсоналом25") Тогда
		СтандартнаяОбработка = Ложь;
		
		ЗначенияРеквизитов = ОбщегоНазначения.ПолучитьЗначенияРеквизитов(Получатель, "ИспользоватьОтборПоОрганизациям, 
																					|ДатаНачалаВыгрузкиДокументов, 
																					|Организации");
																					
		Организации = ?(ЗначенияРеквизитов.ИспользоватьОтборПоОрганизациям, ЗначенияРеквизитов.Организации.Выгрузить().ВыгрузитьКолонку("Организация"), Неопределено);
		
		ОбменДаннымиСервер.ЗарегистрироватьДанныеПоДатеНачалаВыгрузкиИОрганизациям( Получатель, 
																					ЗначенияРеквизитов.ДатаНачалаВыгрузкиДокументов, 
																					Организации, 
																					Отбор);
		
	ИначеЕсли ТипЗнч(Получатель) = Тип("ПланОбменаСсылка.СинхронизацияДанныхЧерезУниверсальныйФормат") Тогда
		
		СтандартнаяОбработка = Ложь;
		
		ЗначенияРеквизитов = ОбщегоНазначения.ПолучитьЗначенияРеквизитов(Получатель, "ИспользоватьОтборПоОрганизациям, 
			|ДатаНачалаВыгрузкиДокументов, 
			|Организации, 
			|ВыгружатьУправленческуюОрганизацию,
			|ПравилаОтправкиСправочников,
			|ПравилаОтправкиДокументов");
		
		Организации = ?(ЗначенияРеквизитов.ИспользоватьОтборПоОрганизациям, ЗначенияРеквизитов.Организации.Выгрузить().ВыгрузитьКолонку("Организация"), Неопределено);
		
		Если ЗначенияРеквизитов.ПравилаОтправкиСправочников <> "НеСинхронизировать"
			ИЛИ ЗначенияРеквизитов.ПравилаОтправкиДокументов = "АвтоматическаяСинхронизация" Тогда
			
			Отбор = Новый Массив();
			ИмяПланаОбмена = ОбменДаннымиПовтИсп.ПолучитьИмяПланаОбмена(Получатель);
			СоставПланаОбмена = Метаданные.ПланыОбмена[ИмяПланаОбмена].Состав;
			
			Для Каждого ЭлементСоставаПланаОбмена Из СоставПланаОбмена Цикл 
			
				Если ОбщегоНазначения.ЭтоСправочник(ЭлементСоставаПланаОбмена.Метаданные) Тогда
					
					Если ЗначенияРеквизитов.ПравилаОтправкиСправочников = "АвтоматическаяСинхронизация"
						ИЛИ (ЗначенияРеквизитов.ПравилаОтправкиСправочников = "СинхронизироватьПоНеобходимости"
						И ЭлементСоставаПланаОбмена.Метаданные.Имя = "Организации") Тогда
						Отбор.Добавить(ЭлементСоставаПланаОбмена.Метаданные);
					КонецЕсли;
					
				ИначеЕсли ОбщегоНазначения.ЭтоДокумент(ЭлементСоставаПланаОбмена.Метаданные) Тогда
					
					Если ЗначенияРеквизитов.ПравилаОтправкиДокументов = "АвтоматическаяСинхронизация" Тогда
						Отбор.Добавить(ЭлементСоставаПланаОбмена.Метаданные);
					КонецЕсли;
					
				КонецЕсли;
				
			КонецЦикла;
			
			Если Отбор.Количество() = 0 Тогда
				Отбор = Неопределено;
			КонецЕсли;
			
			ОбменДаннымиСобытияУТУП.ЗарегистрироватьДанныеПоДатеНачалаВыгрузкиИОрганизациям(Получатель, 
				ЗначенияРеквизитов.ДатаНачалаВыгрузкиДокументов,
				Организации,
				Отбор);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

// Возвращает признак наличия действующих обменов, использующих формирование договоров по заказам.
// 
// Возвращаемое значение:
//  Булево - признак наличия узлов
//
Функция ЕстьОбменыСФормированиемДоговоровПоЗаказам() Экспорт
	
	Возврат Ложь;
	
КонецФункции

// Информирует пользователя о запрете создания или изменения объекта в этой 
// информационной базе
Процедура КонтрольСозданияДокументовВРаспределеннойИБ(Объект, Отказ) Экспорт
	Возврат;
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

//Возвращает признак доступности плана обмена в базовой версии
Функция ДоступностьПланаОбменаВБазовойВерсии(ИмяПланаОбмена,СообщатьПользователю = Истина)
	
	КоличествоУзловПланаОбмена = 1;
	СписокДоступныхПлановОбмена = ОбменДаннымиПовтИсп.ПланыОбменаБСП();
	
	Если СписокДоступныхПлановОбмена.Найти(ИмяПланаОбмена) <> Неопределено
		И ОбменДаннымиСобытия.ВсеУзлыПланаОбмена(ИмяПланаОбмена).Количество() < КоличествоУзловПланаОбмена Тогда
		Возврат Истина;
	Иначе
		Если СообщатьПользователю Тогда
			Сообщение = Новый СообщениеПользователю;
			Сообщение.Текст = НСтр("ru='Используются ограничения базовой версии. Выбранный план обмена создать невозможно.'");
			Сообщение.Сообщить();
		КонецЕсли;
		Возврат Ложь;
	КонецЕсли;
	
КонецФункции

#Область ОбработчикиОбновленияИнформационнойБазы

// Добавляет в список процедуры-обработчики обновления данных ИБ
// для всех поддерживаемых версий библиотеки или конфигурации.
// Вызывается перед началом обновления данных ИБ для построения плана обновления.
//
// Параметры:
//  Обработчики - ТаблицаЗначений - описание полей 
//                                  см. в процедуре ОбновлениеИнформационнойБазы.НоваяТаблицаОбработчиковОбновления
//
// Пример добавления процедуры-обработчика в список:
//  Обработчик = Обработчики.Добавить();
//  Обработчик.Версия              = "1.0.0.0";
//  Обработчик.Процедура           = "ОбновлениеИБ.ПерейтиНаВерсию_1_0_0_0";
//  Обработчик.МонопольныйРежим    = Ложь;
//  Обработчик.Опциональный        = Истина;
// 
Процедура ПриДобавленииОбработчиковОбновления(Обработчики) Экспорт
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
