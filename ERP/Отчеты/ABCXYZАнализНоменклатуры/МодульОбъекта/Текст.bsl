﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Процедура УстановитьЗаголовкиПолей()
	
	Если НЕ ОбщегоНазначенияПовтИсп.ДоступноИспользованиеРазделенныхДанных() Тогда
		Возврат;
	КонецЕсли;
	
	ВалютаУправленческогоУчета = Строка(Константы.ВалютаУправленческогоУчета.Получить());

	Для каждого ВложеннаяСхема из СхемаКомпоновкиДанных.ВложенныеСхемыКомпоновкиДанных Цикл
		
		Для каждого НаборДанных из ВложеннаяСхема.Схема.НаборыДанных Цикл
			
			Для каждого ПолеНабораДанных из НаборДанных.Поля Цикл
				
				Если Найти(ПолеНабораДанных.Заголовок, "%ВалютаУпр%") > 0 Тогда
					
					ПолеНабораДанных.Заголовок = СтрЗаменить(ПолеНабораДанных.Заголовок, "%ВалютаУпр%", ВалютаУправленческогоУчета);
					
				КонецЕсли;
				
			КонецЦикла;
			
		КонецЦикла;
		
		Для каждого ВычисляемоеПоле из ВложеннаяСхема.Схема.ВычисляемыеПоля Цикл
			
			Если Найти(ВычисляемоеПоле.Заголовок, "%ВалютаУпр%") > 0 Тогда
				
				ВычисляемоеПоле.Заголовок = СтрЗаменить(ВычисляемоеПоле.Заголовок, "%ВалютаУпр%", ВалютаУправленческогоУчета);
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЦикла;

КонецПроцедуры

УстановитьЗаголовкиПолей();

#КонецОбласти

#КонецЕсли