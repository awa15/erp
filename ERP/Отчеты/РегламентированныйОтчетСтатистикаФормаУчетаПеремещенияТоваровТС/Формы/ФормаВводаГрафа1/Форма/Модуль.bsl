﻿
&НаКлиенте
Перем Оповестить;

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОтправительИндекс 			  = Параметры.ОтправительИндекс;
	ОтправительНаименование       = Параметры.ОтправительНаименование;
	ОтправительНаселенныйПункт    = Параметры.ОтправительНаселенныйПункт;
	ОтправительОбласть 			  = Параметры.ОтправительОбласть;
	ОтправительРБ_УНП 			  = Параметры.ОтправительРБ_УНП;
	ОтправительРК_БИН 			  = Параметры.ОтправительРК_БИН;	
	ОтправительРК_ИИН 			  = Параметры.ОтправительРК_ИИН;
	ОтправительРФ_ИНН			  = Параметры.ОтправительРФ_ИНН;
	ОтправительРФ_КПП 			  = Параметры.ОтправительРФ_КПП;
	ОтправительРФ_ОГРН 			  = Параметры.ОтправительРФ_ОГРН;
	ОтправительСтранаКод 		  = Параметры.ОтправительСтранаКод;
	ОтправительСтранаНаименование = Параметры.ОтправительСтранаНаименование;
	ОтправительУлицаНомерДома     = Параметры.ОтправительУлицаНомерДома;
	
	СтруктураРеквизитов = Новый Структура;
	МассивРеквизитов = ПолучитьРеквизиты();
	Для Каждого Реквизит Из МассивРеквизитов Цикл
		Если Реквизит.Имя <> "СтруктураРеквизитов" Тогда
			СтруктураРеквизитов.Вставить(Реквизит.Имя);
		КонецЕсли;
	КонецЦикла;
			
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	УстановкаДляРФ = Ложь;
	УстановкаДляРБ = Ложь;
	УстановкаДляРК = Ложь;
	
	Если ВладелецФормы.СтруктураРеквизитовФормы.СтранаОтправления = "РФ" Тогда
	
		УстановкаДляРФ = Истина;
		ОтправительСтранаКод = "RU";
		ОтправительСтранаНаименование = "РОССИЯ";
		
	ИначеЕсли ВладелецФормы.СтруктураРеквизитовФормы.СтранаОтправления = "РБ" Тогда
		
		УстановкаДляРБ = Истина;
		ОтправительСтранаКод = "BY";
		ОтправительСтранаНаименование = "БЕЛАРУСЬ";
		
	ИначеЕсли ВладелецФормы.СтруктураРеквизитовФормы.СтранаОтправления = "РК" Тогда
		
		УстановкаДляРК = Истина;
		ОтправительСтранаКод = "KZ";
		ОтправительСтранаНаименование = "КАЗАХСТАН";
	
	КонецЕсли; 
	
	Элементы.ОтправительРФ_ОГРН.Доступность = УстановкаДляРФ;
	Элементы.ОтправительРФ_ИНН.Доступность  = УстановкаДляРФ;
	Элементы.ОтправительРФ_КПП.Доступность  = УстановкаДляРФ;
	
	Элементы.ОтправительРБ_УНП.Доступность  = УстановкаДляРБ;
	
	Элементы.ОтправительРК_БИН.Доступность  = УстановкаДляРК;
	Элементы.ОтправительРК_ИИН.Доступность  = УстановкаДляРК;
	
	Модифицированность = Ложь;
	Оповестить = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура КнопкаОКНажатие(Команда)
	    	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПередЗакрытиемЗавершениеПродолжение", ЭтотОбъект);
	ПрименитьИзменения(ОписаниеОповещения);

КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если Модифицированность Тогда
		
		ТекстВопроса = НСтр("ru = 'Данные были изменены. Применить изменения?'");
		ОписаниеОповещения = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса,  РежимДиалогаВопрос.ДаНетОтмена);
		Отказ = Истина;
				
	КонецЕсли;
	
	СтандартнаяОбработка = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ПередЗакрытиемЗавершениеПродолжение", ЭтотОбъект);
		ПрименитьИзменения(ОписаниеОповещения);
				
	ИначеЕсли Ответ = КодВозвратаДиалога.Нет Тогда
		
		Модифицированность = Ложь;
		Закрыть(Неопределено);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершениеПродолжение(Результат, ДополнительныеПараметры) Экспорт

	Если Результат = Истина Тогда
		
		Модифицированность = Ложь;
		Закрыть(СтруктураРеквизитов);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьПустуюСтруктуруРеквизитов() Экспорт
	
	ПустаяСтруктураРеквизитовФормы = Новый Структура;
		
	Для Каждого ЭлементФормы Из ЭтаФорма.Элементы Цикл
		
		Если ТипЗнч(ЭлементФормы) = Тип("ПолеФормы") Тогда
			Если ЗначениеЗаполнено(ЭлементФормы.ПутьКДанным) Тогда
				
				ПустаяСтруктураРеквизитовФормы.Вставить(ЭлементФормы.ПутьКДанным);
								
			КонецЕсли; 
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат ПустаяСтруктураРеквизитовФормы;
	
КонецФункции	

&НаКлиенте
Процедура ПрименитьИзменения(ВыполняемоеОповещение)
		
	РеквСтрокаСообщения	= "";
		
	Для Каждого РеквизитФормы Из СтруктураРеквизитов Цикл
						
		СтруктураРеквизитов[РеквизитФормы.Ключ] = ЭтаФорма[РеквизитФормы.Ключ];
								
		//Проверка на заполненность полей, обязательных к заполнению
		Если ТипЗнч(Элементы[РеквизитФормы.Ключ]) = Тип("ПолеФормы") Тогда
			Если Элементы[РеквизитФормы.Ключ].Доступность И Элементы[РеквизитФормы.Ключ].АвтоОтметкаНезаполненного Тогда
				Если НЕ ЗначениеЗаполнено(СтруктураРеквизитов[РеквизитФормы.Ключ]) Тогда
							
					ПредставлениеРекв = Элементы[РеквизитФормы.Ключ].Заголовок;
					РеквСтрокаСообщения = РеквСтрокаСообщения + ?(ПустаяСтрока(РеквСтрокаСообщения), "", "," + Символы.ПС) + """" + ПредставлениеРекв + """";
							 
				КонецЕсли;	
			КонецЕсли; 
		КонецЕсли;		
			
	КонецЦикла; 
	
	Если НЕ ПустаяСтрока(РеквСтрокаСообщения) Тогда 
		
		ТекстВопроса = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru='Не заполнены поля, обязательные к заполнению: %1 %1%2. %1 %1 Продолжить редактирование ?'"), Символы.ПС, РеквСтрокаСообщения);
		ОписаниеОповещения = Новый ОписаниеОповещения("ПрименитьИзмененияЗавершение", ЭтотОбъект, ВыполняемоеОповещение);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
		Возврат;
		
	КонецЕсли;		

	ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, Истина);

КонецПроцедуры

&НаКлиенте
Процедура ПрименитьИзмененияЗавершение(Ответ, ВыполняемоеОповещение) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, Ложь);
	ИначеЕсли Ответ = КодВозвратаДиалога.Нет Тогда
		ВыполнитьОбработкуОповещения(ВыполняемоеОповещение, Истина);	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОтправительНаименованиеПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительИндексПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительОбластьПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительНаселенныйПунктПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительУлицаНомерДомаПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительРФ_ИННПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительРФ_КПППриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительРФ_ОГРНПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительРБ_УНППриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительРК_ИИНПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ОтправительРК_БИНПриИзменении(Элемент)
	Модифицированность = Истина;
КонецПроцедуры

// Обработка выбора адресной информации из справочников
//
&НаКлиенте
Процедура ОтправительНаименованиеНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ИмяСправочникаДляВыбора = "Контрагенты";
	Если ВладелецФормы.СтруктураРеквизитовФормы.НапрПеремещения = "ЭК" Тогда
			ИмяСправочникаДляВыбора = "Организации";
	КонецЕсли;
	Если НЕ ВладелецФормы.СуществуетСправочник(ИмяСправочникаДляВыбора) Тогда
		Возврат;
	КонецЕсли;
	
	СтандартнаяОбработка = Ложь;
	
	ВыбранноеЗначение = Неопределено;
	ОписаниеОповещения = Новый ОписаниеОповещения("ОтправительНаименованиеНачалоВыбораЗавершение", ЭтотОбъект);
	ВладелецФормы.ПолучитьСведенияИзСправочника(Элемент.ТекстРедактирования, ИмяСправочникаДляВыбора, ВыбранноеЗначение, ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтправительНаименованиеНачалоВыбораЗавершение(СтруктураВозврата, ДополнительныеПараметры) Экспорт 
	
	СтруктураСведений = СтруктураВозврата.СтруктураСведений;
	ВыбранноеЗначение = СтруктураВозврата.ВыбранноеЗначение;
	
	Если ВыбранноеЗначение = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	СтруктураАдреса = ВладелецФормы.АдресВФормате9ЗапятыхВСтруктуруПорталаТСНаКлиенте(СтруктураСведений.Адрес);
	
	ОтправительНаименование = СтруктураСведений.Наименование;
	
	ОтправительИндекс =  СтруктураАдреса.Индекс;
	ОтправительОбласть = СтруктураАдреса.Область;
	ОтправительНаселенныйПункт = СтруктураАдреса.НаселенныйПункт;
	ОтправительУлицаНомерДома = СтруктураАдреса.УлицаНомерДома; 
	
	Если ВладелецФормы.СтруктураРеквизитовФормы.СтранаОтправления = "РФ" Тогда
		ОтправительРФ_ИНН  = СтруктураСведений.ИНН;
		ОтправительРФ_КПП  = СтруктураСведений.КПП;
		ОтправительРФ_ОГРН = СтруктураСведений.ОГРН;
	ИначеЕсли ВладелецФормы.СтруктураРеквизитовФормы.СтранаОтправления = "РБ" Тогда
		ОтправительРБ_УНП  = СтруктураСведений.ИНН;
	ИначеЕсли ВладелецФормы.СтруктураРеквизитовФормы.СтранаОтправления = "РК" Тогда	
	    ОтправительРК_ИИН  = СтруктураСведений.ИНН;
	КонецЕсли; 
	
КонецПроцедуры
