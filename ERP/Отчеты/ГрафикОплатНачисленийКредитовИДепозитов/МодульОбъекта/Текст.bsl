﻿
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПриКомпоновкеРезультата(ДокументРезультат, ДанныеРасшифровки, СтандартнаяОбработка)
	
	НастройкиДоговора = КомпоновщикНастроек.Настройки.Структура[0].Настройки;
	КомпоновкаДанныхКлиентСервер.УстановитьПараметр(НастройкиДоговора,"НесколькоВалют", ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоВалют"));
	КомпоновкаДанныхКлиентСервер.УстановитьПараметр(НастройкиДоговора,"ПартнерыИКонтрагенты", ПолучитьФункциональнуюОпцию("ИспользоватьПартнеровИКонтрагентов"));
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли