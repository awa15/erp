﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНеПроверяемыхРеквизитов = Новый Массив;
	
	Справочники.КлассыОбъектовЭксплуатации.ПроверкаЗаполненияПравилПланирования(ЭтотОбъект, МассивНеПроверяемыхРеквизитов, Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНеПроверяемыхРеквизитов);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли