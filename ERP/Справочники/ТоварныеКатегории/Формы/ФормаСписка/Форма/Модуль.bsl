﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	// Обработчик подсистемы "Дополнительные отчеты и обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	ЗначениеВладельца = Неопределено;
	Если Параметры.Свойство("Владелец",ЗначениеВладельца) Тогда
		Элементы.СписокВидовНоменклатуры.Видимость = Ложь;
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
			Список,
			"Владелец",
			ЗначениеВладельца,
			Истина,
			ВидСравненияКомпоновкиДанных.Равно);
	Иначе
		Элементы.СписокВидовНоменклатуры.Видимость = Истина;
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
			Список,
			"Владелец",
			Справочники.ВидыНоменклатуры.ПустаяСсылка(),
			Ложь,
			ВидСравненияКомпоновкиДанных.Равно);
	КонецЕсли;

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписоквидовноменклатуры

&НаКлиенте
Процедура СписокВидовНоменклатурыПриАктивизацииСтроки(Элемент)
	
	УстановитьОтборПоТекущейСтрокеВидаНоменклатуры(Элемент.ТекущаяСтрока);
	
	Элементы.Список.Доступность = (Элемент.ТекущиеДанные.ЭтоГруппа = Ложь);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура УстановитьОтборПоТекущейСтрокеВидаНоменклатуры(ТекущаяСтрокаВида)
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
		Список,
		"Владелец",
		ТекущаяСтрокаВида,
		Истина,
		ВидСравненияКомпоновкиДанных.Равно);
КонецПроцедуры

#КонецОбласти
