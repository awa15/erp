﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// Для нового элемента проверим наличие элемента с такими кодами НДФЛ и взносов.
	Если ТекущийОбъект.Ссылка.Пустая() Тогда
		
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("КодДоходаНДФЛ", ТекущийОбъект.КодДоходаНДФЛ);
		Запрос.УстановитьПараметр("КодДоходаСтраховыеВзносы", ТекущийОбъект.КодДоходаСтраховыеВзносы);
		Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	ВидыАвторскихДоговоров.Наименование КАК Наименование,
		|	ВидыАвторскихДоговоров.КодДоходаНДФЛ.Код КАК КодДоходаНДФЛ,
		|	ВидыАвторскихДоговоров.КодДоходаСтраховыеВзносы.Наименование КАК КодДоходаСтраховыеВзносы
		|ИЗ
		|	Справочник.ВидыДоговоровАвторскогоЗаказа КАК ВидыАвторскихДоговоров
		|ГДЕ
		|	ВидыАвторскихДоговоров.КодДоходаНДФЛ = &КодДоходаНДФЛ
		|	И ВидыАвторскихДоговоров.КодДоходаСтраховыеВзносы = &КодДоходаСтраховыеВзносы
		|
		|УПОРЯДОЧИТЬ ПО
		|	КодДоходаНДФЛ";
		РезультатЗапроса = Запрос.Выполнить();
		Если Не РезультатЗапроса.Пустой() Тогда
			
			Отказ = Истина;
			Выборка = РезультатЗапроса.Выбрать();
			Выборка.Следующий();
			КодДоходаНДФЛ = Выборка.КодДоходаНДФЛ;
			КодДоходаСтраховыеВзносы = Выборка.КодДоходаСтраховыеВзносы;
			Наименование = Выборка.Наименование;
			ТекстСообщения = "Вид авторского договора с такими кодами НДФЛ и страховых взносов уже существует:
					|""" + Выборка.Наименование + """.
					|Элемент не записан.";
			Сообщить(ТекстСообщения);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
