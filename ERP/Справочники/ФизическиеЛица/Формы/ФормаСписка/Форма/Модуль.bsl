﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.КоманднаяПанельФормы);
	// Конец СтандартныеПодсистемы.Печать
	
	Если Параметры.РежимВыбора = Истина Тогда
		
		Элементы.Список.РежимВыбора = Истина;
		Если НЕ ЭтаФорма.ЗакрыватьПриВыборе Тогда
		
			Если НЕ ПустаяСтрока(Параметры.АдресСпискаПодобранныхСотрудников) Тогда
				МассивПодобранных = ПолучитьИзВременногоХранилища(Параметры.АдресСпискаПодобранныхСотрудников);
				СписокПодобранных.ЗагрузитьЗначения(МассивПодобранных);
			КонецЕсли; 
		
		КонецЕсли; 
		
	КонецЕсли;
	
	УстановитьСписокПодобранныхСотрудников();
	
	ЗарплатаКадры.ПриСозданииНаСервереФормыСДинамическимСписком(ЭтотОбъект, "Список");
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если Элементы.Список.РежимВыбора И ИмяСобытия = "СозданоФизическоеЛицо" И Источник = Элементы.Список Тогда
		
		Если Элементы.Список.МножественныйВыбор И ТипЗнч(Параметр) <> Тип("Массив") Тогда
			ПараметрОповещения = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Параметр);
		Иначе
			ПараметрОповещения = Параметр;
		КонецЕсли; 
		
		ОповеститьОВыборе(ПараметрОповещения);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиСобытийЭлементовТаблицыФормыСписок

&НаКлиенте
Процедура СписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ОценкаПроизводительностиКлиентСервер.НачатьЗамерВремени("ОткрытиеФормыЭлементаСправочникаФизическиеЛица");
КонецПроцедуры

&НаКлиенте
Процедура СписокВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	
	Если Элементы.Список.РежимВыбора И НЕ ЗакрыватьПриВыборе Тогда
		ОбновитьСписокПодобранных(Значение);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	ОценкаПроизводительностиКлиентСервер.НачатьЗамерВремени("СозданиеНовогоЭлементаСправочникаФизическиеЛица");
	
	Если Не Группа И Элементы.Список.РежимВыбора Тогда
		
		Отказ = Истина;
		
		ПараметрыОткрытия = Новый Структура;
		ПараметрыОткрытия.Вставить("РежимВыбора", Истина);
		
		ОткрытьФорму("Справочник.ФизическиеЛица.ФормаОбъекта", ПараметрыОткрытия, Элемент);

	КонецЕсли; 
	
КонецПроцедуры

&НаСервере
Процедура СписокПередЗагрузкойПользовательскихНастроекНаСервере(Элемент, Настройки)
	ЗарплатаКадры.ПроверитьПользовательскиеНастройкиДинамическогоСписка(ЭтотОбъект, Истина);
КонецПроцедуры

&НаСервере
Процедура СписокПриОбновленииСоставаПользовательскихНастроекНаСервере(СтандартнаяОбработка)
	ЗарплатаКадры.ПроверитьПользовательскиеНастройкиДинамическогоСписка(ЭтотОбъект);
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ОбновитьСписокПодобранных(Значение)
	
	Если ТипЗнч(Значение) = Тип("Массив") Тогда
		
		Для каждого ВыбранноеЗначение Из Значение Цикл
			СписокПодобранных.Добавить(ВыбранноеЗначение);
		КонецЦикла;
		
	Иначе
		СписокПодобранных.Добавить(Значение);
	КонецЕсли;
	
	УстановитьСписокПодобранныхСотрудников();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьСписокПодобранныхСотрудников()
	
	ОбщегоНазначенияКлиентСервер.УстановитьПараметрДинамическогоСписка(
		Список,
		"СписокПодобранных",
		СписокПодобранных.ВыгрузитьЗначения());
		
КонецПроцедуры

#КонецОбласти
