﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки.СчетМУ,
	|	УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки.СчетБУ,
	|	УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки.СчетБУ.Код КАК СчетБУКод
	|ИЗ
	|	РегистрСведений.УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки КАК УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки
	|
	|УПОРЯДОЧИТЬ ПО
	|	СчетБУКод";
	
	ТаблицаСооветствия = Запрос.Выполнить().Выгрузить();
	ЗначениеВРеквизитФормы(ТаблицаСооветствия,"СписокСоответствия");
	Элементы.СписокСоответствия.ОтборСтрок = Новый ФиксированнаяСтруктура(Новый Структура("СчетМУ", 0));
	
	ЕстьПраво = ПравоДоступа("Изменение", Метаданные.РегистрыСведений.УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки);
	Элементы.ГруппаЗаписать.Доступность = ЕстьПраво;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если Модифицированность Тогда
		Текст = НСтр("ru = 'Сохранить изменения?'");
		Ответ = Вопрос(Текст,РежимДиалогаВопрос.ДаНетОтмена,,КодВозвратаДиалога.Да);
		Если Ответ = КодВозвратаДиалога.Да Тогда
			ЗаписатьСоответствие();
		ИначеЕсли Ответ = КодВозвратаДиалога.Отмена Тогда
			Отказ = Истина;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#Область БыстрыеОтборы

&НаКлиенте
Процедура ОтборЕстьНастройкиПриИзменении(Элемент)
	
	УстановитьОтборСчетовМФУ("ЕстьНастройки",ОтборЕстьНастройки = "ЕстьСоответствие", ЗначениеЗаполнено(ОтборЕстьНастройки));
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборСчетБУПриИзменении(Элемент)
	
	УстановитьОтборСчетовМФУ("СчетБУ",ОтборСчетБУ, ЗначениеЗаполнено(ОтборСчетБУ));
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписоксоответствия

&НаКлиенте
Процедура СписокСоответствияПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	Если НоваяСтрока И НЕ Копирование Тогда
		Элемент.ТекущиеДанные.СчетМУ = Элемент.ОтборСтрок.СчетМУ;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыСписокплансчетов

&НаКлиенте
Процедура СписокПланСчетовПриАктивизацииСтроки(Элемент)
	
	ЭтаФорма.ПодключитьОбработчикОжидания("СписокПриАктивизацииСтрокиОбработчикОжидания", 0.1, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПланСчетовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ОткрытьФормуСоответствияСчета(ВыбраннаяСтрока);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПланСчетовПередНачаломИзменения(Элемент, Отказ)
	
	Отказ = Истина;
	ОткрытьФормуСоответствияСчета(Элемент.ТекущаяСтрока);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

&НаКлиенте
Процедура СкопироватьСоответствие(Команда)
	
	СчетаБУ = ПолучитьСчетаБУ();
	Если НЕ ЗначениеЗаполнено(СчетаБУ) Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура("МножественныйВыбор",Истина);
	ВыбранныеСчета = Неопределено;

	ОткрытьФорму("ПланСчетов.Международный.ФормаВыбора",ПараметрыФормы,,,,, Новый ОписаниеОповещения("СкопироватьСоответствиеЗавершение", ЭтотОбъект, Новый Структура("СчетаБУ", СчетаБУ)), РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура СкопироватьСоответствиеЗавершение(Результат, ДополнительныеПараметры) Экспорт
    
    СчетаБУ = ДополнительныеПараметры.СчетаБУ;
    
    
    ВыбранныеСчета = Результат;
    Если ЗначениеЗаполнено(ВыбранныеСчета) Тогда
        КопироватьСчетаБУ(СчетаБУ,ВыбранныеСчета);
    КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура УдалитьСоответствие(Команда)
	
	СчетаБУ = ПолучитьСчетаБУ();
	Если НЕ ЗначениеЗаполнено(СчетаБУ) Тогда
		Возврат;
	КонецЕсли;
	
	КодыСчетов = "";
	Для Каждого Счет Из СчетаБУ Цикл
		КодыСчетов = КодыСчетов + Строка(Счет) + "; ";
	КонецЦикла;
	СтроковыеФункцииКлиентСервер.УдалитьПоследнийСимволВСтроке(КодыСчетов,2);
		
	ШаблонТекста = НСтр("ru = 'Удалить счет %1
	|из соответствия всех счетов международного учета?'");
	Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонТекста, КодыСчетов);
	Ответ = Неопределено;

	ПоказатьВопрос(Новый ОписаниеОповещения("УдалитьСоответствиеЗавершение", ЭтотОбъект, Новый Структура("СчетаБУ", СчетаБУ)), Текст,РежимДиалогаВопрос.ДаНет,,КодВозвратаДиалога.Да);
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьСоответствиеЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
    
    СчетаБУ = ДополнительныеПараметры.СчетаБУ;
    
    
    Ответ = РезультатВопроса;
    Если Ответ = КодВозвратаДиалога.Да Тогда
        УдалитьСчетаБУ(СчетаБУ);
    КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура Записать(Команда)
	
	ЗаписатьСоответствие();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьЗакрыть(Команда)
	
	ЗаписатьСоответствие();
	Закрыть();
	
КонецПроцедуры

#Область ОбработчикиОжидания

&НаКлиенте
Процедура СписокПриАктивизацииСтрокиОбработчикОжидания()
	
	СписокСчетов = Элементы.СписокПланСчетов;
	Если НЕ ЗначениеЗаполнено(СписокСчетов.ТекущаяСтрока) Тогда
		Возврат;
	КонецЕсли;
	
	Элементы.СписокСоответствия.ОтборСтрок = Новый ФиксированнаяСтруктура(Новый Структура("СчетМУ", СписокСчетов.ТекущаяСтрока));
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаСервере
Процедура УстановитьОтборСчетовМФУ(ИмяОтбора,ЗначениеОтбора, Использовать)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
		СписокПланСчетов, 
		ИмяОтбора, 
		ЗначениеОтбора, 
		ВидСравненияКомпоновкиДанных.Равно,
		, 
		Использовать);
	
	УстановитьОтображениеПланаСчетовМФУ();
	
КонецПроцедуры

&НаКлиенте
Функция ПолучитьСчетаБУ()
	
	СчетаБУ = Новый Массив;
	ВыделенныеСтроки = Элементы.СписокСоответствия.ВыделенныеСтроки;
	Если ВыделенныеСтроки.Количество() = 0 Тогда
		
		Текст = НСтр("ru = 'Не выбранны счета бухгалтерского учета.'");
		Возврат СчетаБУ;
		
	КонецЕсли;
	
	Для Каждого ВыделеннаяСтрока Из ВыделенныеСтроки Цикл
		СтрокаСоответствия = СписокСоответствия.НайтиПоИдентификатору(ВыделеннаяСтрока);
		СчетаБУ.Добавить(СтрокаСоответствия.СчетБУ);
	КонецЦикла;
	
	Возврат СчетаБУ;
	
КонецФункции

&НаСервере
Процедура УстановитьОтображениеПланаСчетовМФУ()
	
	Если ЗначениеЗаполнено(ОтборЕстьНастройки) ИЛИ ЗначениеЗаполнено(ОтборСчетБУ) Тогда
		Элементы.СписокПланСчетов.Отображение = ОтображениеТаблицы.Список;
	Иначе
		Элементы.СписокПланСчетов.Отображение = ОтображениеТаблицы.Дерево;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьФормуСоответствияСчета(СчетМУ)
	
	ПараметрыФормы = Новый Структура("Ключ", СчетМУ);
	ОткрытьФорму("РегистрСведений.УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки.Форма.ФормаСоответствияСчета",ПараметрыФормы);
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьСоответствие()
	
	Набор = РегистрыСведений.УдалитьСоответствиеСчетовМУиБУдляОтчетаСверки.СоздатьНаборЗаписей();
	ТаблицаСоответствия = РеквизитФормыВЗначение("СписокСоответствия",Тип("ТаблицаЗначений"));
	ТаблицаСоответствия.Свернуть("СчетМУ,СчетБУ");
	Набор.Загрузить(ТаблицаСоответствия);
	Набор.Записать();
	Модифицированность = Ложь;
	
КонецПроцедуры

&НаСервере
Процедура КопироватьСчетаБУ(СчетаБУ,СчетаМУ)
	
	Отбор = Новый Структура("СчетМУ,СчетБУ");
	Для Каждого СчетМУ Из СчетаМУ Цикл
		Для Каждого СчетБУ Из СчетаБУ Цикл
			Отбор.СчетМУ = СчетМУ;
			Отбор.СчетБУ = СчетБУ;
			МассивСтрок = СписокСоответствия.НайтиСтроки(Отбор);
			Если МассивСтрок.Количество() = 0 Тогда
				НоваяСтрока = СписокСоответствия.Добавить();
				НоваяСтрока.СчетМУ = СчетМУ;
				НоваяСтрока.СчетБУ = СчетБУ;
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура УдалитьСчетаБУ(СчетаБУ)
	
	Отбор = Новый Структура("СчетБУ");
	Для Каждого СчетБУ Из СчетаБУ Цикл
		Отбор.СчетБУ = СчетБУ;
		СтрокиКУдалению = СписокСоответствия.НайтиСтроки(Отбор);
		Для Каждого УдаляемаяСтрока Из СтрокиКУдалению Цикл
			СписокСоответствия.Удалить(УдаляемаяСтрока);
		КонецЦикла;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
