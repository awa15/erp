﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

Функция ПроверитьВозможностьВнесенияИзменений(ДокументСсылка, ИзменяемыеПозиции = Неопределено, ДатаИзменений = '00010101') Экспорт
	
	РезультатПроверки = Новый Структура("ИзмененияВозможны,РегистраторПредставление,Регистратор,Позиция,ДатаИзменений", Истина, "");
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Регистратор", ДокументСсылка);
	
	Если ИзменяемыеПозиции <> Неопределено Тогда
		
		Запрос.УстановитьПараметр("ИзменяемыеПозиции", ИзменяемыеПозиции);
		Запрос.УстановитьПараметр("ДатаИзменений", ДатаИзменений);
		
		Запрос.Текст =
			"ВЫБРАТЬ
			|	ШтатноеРасписание.Ссылка КАК ПозицияШтатногоРасписания,
			|	&ДатаИзменений КАК Дата
			|ПОМЕСТИТЬ ВТЗаписываемыеСведения
			|ИЗ
			|	Справочник.ШтатноеРасписание КАК ШтатноеРасписание
			|ГДЕ
			|	ШтатноеРасписание.Ссылка В(&ИзменяемыеПозиции)
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания КАК ПозицияШтатногоРасписания,
			|	ИсторияИспользованияШтатногоРасписания.Дата КАК Дата
			|ПОМЕСТИТЬ ВТПрежниеСведения
			|ИЗ
			|	РегистрСведений.ИсторияИспользованияШтатногоРасписания КАК ИсторияИспользованияШтатногоРасписания
			|ГДЕ
			|	ИсторияИспользованияШтатногоРасписания.Регистратор = &Регистратор
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ЕСТЬNULL(ПрежниеСведения.ПозицияШтатногоРасписания, ЗаписываемыеСведения.ПозицияШтатногоРасписания) КАК ПозицияШтатногоРасписания,
			|	ВЫБОР
			|		КОГДА ЕСТЬNULL(ПрежниеСведения.Дата, ЗаписываемыеСведения.Дата) < ЕСТЬNULL(ЗаписываемыеСведения.Дата, ПрежниеСведения.Дата)
			|			ТОГДА ЕСТЬNULL(ПрежниеСведения.Дата, ЗаписываемыеСведения.Дата)
			|		ИНАЧЕ ЕСТЬNULL(ЗаписываемыеСведения.Дата, ПрежниеСведения.Дата)
			|	КОНЕЦ КАК Дата
			|ПОМЕСТИТЬ ВТПозицииДокумента
			|ИЗ
			|	ВТПрежниеСведения КАК ПрежниеСведения
			|		ПОЛНОЕ СОЕДИНЕНИЕ ВТЗаписываемыеСведения КАК ЗаписываемыеСведения
			|		ПО ПрежниеСведения.ПозицияШтатногоРасписания = ЗаписываемыеСведения.ПозицияШтатногоРасписания";
			
	Иначе
		
		Запрос.Текст =
			"ВЫБРАТЬ
			|	ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания,
			|	ИсторияИспользованияШтатногоРасписания.Дата
			|ПОМЕСТИТЬ ВТПозицииДокумента
			|ИЗ
			|	РегистрСведений.ИсторияИспользованияШтатногоРасписания КАК ИсторияИспользованияШтатногоРасписания
			|ГДЕ
			|	ИсторияИспользованияШтатногоРасписания.Регистратор = &Регистратор";
		
	КонецЕсли;
		
	Запрос.Выполнить();	
		
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ПозицииДокумента.ПозицияШтатногоРасписания,
		|	ПозицииДокумента.Дата,
		|	МИНИМУМ(ИсторияИспользованияШтатногоРасписания.Дата) КАК ДатаПоследующихИзменений
		|ПОМЕСТИТЬ ВТДатыПоследующихИзменений
		|ИЗ
		|	ВТПозицииДокумента КАК ПозицииДокумента
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ИсторияИспользованияШтатногоРасписания КАК ИсторияИспользованияШтатногоРасписания
		|		ПО ПозицииДокумента.ПозицияШтатногоРасписания = ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания
		|			И ПозицииДокумента.Дата <= ИсторияИспользованияШтатногоРасписания.Дата
		|			И (ИсторияИспользованияШтатногоРасписания.Регистратор <> &Регистратор)
		|
		|СГРУППИРОВАТЬ ПО
		|	ПозицииДокумента.ПозицияШтатногоРасписания,
		|	ПозицииДокумента.Дата
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ ПЕРВЫЕ 1
		|	ДатыПоследующихИзменений.ПозицияШтатногоРасписания КАК Позиция,
		|	ДатыПоследующихИзменений.Дата,
		|	ДатыПоследующихИзменений.ДатаПоследующихИзменений КАК ДатаИзменений,
		|	ИсторияИспользованияШтатногоРасписания.Регистратор КАК Регистратор,
		|	ПРЕДСТАВЛЕНИЕССЫЛКИ(ИсторияИспользованияШтатногоРасписания.Регистратор) КАК РегистраторПредставление
		|ИЗ
		|	ВТДатыПоследующихИзменений КАК ДатыПоследующихИзменений
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ИсторияИспользованияШтатногоРасписания КАК ИсторияИспользованияШтатногоРасписания
		|		ПО ДатыПоследующихИзменений.ПозицияШтатногоРасписания = ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания
		|			И ДатыПоследующихИзменений.ДатаПоследующихИзменений = ИсторияИспользованияШтатногоРасписания.Дата
		|
		|УПОРЯДОЧИТЬ ПО
		|	ДатаИзменений,
		|	Позиция";
		
	УстановитьПривилегированныйРежим(Истина);
	
	РезультатЗапроса = Запрос.Выполнить();
	Если НЕ РезультатЗапроса.Пустой() Тогда
		
		Выборка = РезультатЗапроса.Выбрать();
		Выборка.Следующий();
		ЗаполнитьЗначенияСвойств(РезультатПроверки, Выборка);
		
		РезультатПроверки.ИзмененияВозможны = Ложь;
		
	КонецЕсли; 
	
	Возврат РезультатПроверки;
	
КонецФункции

Процедура ОбновитьСведенияПодразделений(СписокПозицийШтатногоРасписания) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("СписокПозицийШтатногоРасписания", СписокПозицийШтатногоРасписания);
	
	Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ШтатноеРасписание.Подразделение КАК Подразделение
		|ИЗ
		|	Справочник.ШтатноеРасписание КАК ШтатноеРасписание
		|ГДЕ
		|	ШтатноеРасписание.Ссылка В(&СписокПозицийШтатногоРасписания)";
		
	ОбновляемыеПодразделения = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Подразделение");
	
	Пока ОбновляемыеПодразделения.Количество() > 0 Цикл
		
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("ОбновляемыеПодразделения", ОбновляемыеПодразделения);
		
		Запрос.Текст =
			"ВЫБРАТЬ РАЗЛИЧНЫЕ
			|	ПодразделенияОрганизаций.Ссылка КАК Подразделение
			|ПОМЕСТИТЬ ВТОбновляемыеПодразделения
			|ИЗ
			|	Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
			|ГДЕ
			|	ПодразделенияОрганизаций.Ссылка В(&ОбновляемыеПодразделения)
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ОбновляемыеПодразделения.Подразделение,
			|	МИНИМУМ(ПодразделенияОрганизаций.ДатаСоздания) КАК ДатаСоздания,
			|	МИНИМУМ(ПодразделенияОрганизаций.Расформировано) КАК Расформировано,
			|	МАКСИМУМ(ПодразделенияОрганизаций.ДатаРасформирования) КАК ДатаРасформирования
			|ПОМЕСТИТЬ ВТСведенияПодчиненныхПодразделений
			|ИЗ
			|	ВТОбновляемыеПодразделения КАК ОбновляемыеПодразделения
			|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
			|		ПО ОбновляемыеПодразделения.Подразделение = ПодразделенияОрганизаций.Родитель
			|			И (ПодразделенияОрганизаций.ДатаСоздания > ДАТАВРЕМЯ(1, 1, 1))
			|
			|СГРУППИРОВАТЬ ПО
			|	ОбновляемыеПодразделения.Подразделение
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ОбновляемыеПодразделения.Подразделение,
			|	МИНИМУМ(ИсторияИспользованияШтатногоРасписания.Дата) КАК Дата
			|ПОМЕСТИТЬ ВТМинимальныеДатыИспользованияПодразделений
			|ИЗ
			|	ВТОбновляемыеПодразделения КАК ОбновляемыеПодразделения
			|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ИсторияИспользованияШтатногоРасписания КАК ИсторияИспользованияШтатногоРасписания
			|		ПО ОбновляемыеПодразделения.Подразделение = ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания.Подразделение
			|			И (ИсторияИспользованияШтатногоРасписания.Используется)
			|
			|СГРУППИРОВАТЬ ПО
			|	ОбновляемыеПодразделения.Подразделение
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ОбновляемыеПодразделения.Подразделение,
			|	ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания,
			|	МАКСИМУМ(ИсторияИспользованияШтатногоРасписания.Дата) КАК Дата
			|ПОМЕСТИТЬ ВТМаксимальныеДатыИзменений
			|ИЗ
			|	ВТОбновляемыеПодразделения КАК ОбновляемыеПодразделения
			|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ИсторияИспользованияШтатногоРасписания КАК ИсторияИспользованияШтатногоРасписания
			|		ПО ОбновляемыеПодразделения.Подразделение = ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания.Подразделение
			|
			|СГРУППИРОВАТЬ ПО
			|	ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания,
			|	ОбновляемыеПодразделения.Подразделение
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	МаксимальныеДатыИзменений.Подразделение,
			|	МАКСИМУМ(ИсторияИспользованияШтатногоРасписания.Используется) КАК Используется
			|ПОМЕСТИТЬ ВТПоследниеИспользованияПодразделений
			|ИЗ
			|	ВТМаксимальныеДатыИзменений КАК МаксимальныеДатыИзменений
			|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ИсторияИспользованияШтатногоРасписания КАК ИсторияИспользованияШтатногоРасписания
			|		ПО МаксимальныеДатыИзменений.ПозицияШтатногоРасписания = ИсторияИспользованияШтатногоРасписания.ПозицияШтатногоРасписания
			|			И МаксимальныеДатыИзменений.Дата = ИсторияИспользованияШтатногоРасписания.Дата
			|
			|СГРУППИРОВАТЬ ПО
			|	МаксимальныеДатыИзменений.Подразделение
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ПоследниеИспользованияПодразделений.Подразделение,
			|	МАКСИМУМ(МаксимальныеДатыИзменений.Дата) КАК Дата
			|ПОМЕСТИТЬ ВТДатыЗакрытияПодразделения
			|ИЗ
			|	ВТМаксимальныеДатыИзменений КАК МаксимальныеДатыИзменений
			|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТПоследниеИспользованияПодразделений КАК ПоследниеИспользованияПодразделений
			|		ПО МаксимальныеДатыИзменений.Подразделение = ПоследниеИспользованияПодразделений.Подразделение
			|			И (НЕ ПоследниеИспользованияПодразделений.Используется)
			|
			|СГРУППИРОВАТЬ ПО
			|	ПоследниеИспользованияПодразделений.Подразделение
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	ОбновляемыеПодразделения.Подразделение,
			|	ВЫБОР
			|		КОГДА МинимальныеДатыИспользованияПодразделений.Дата ЕСТЬ NULL 
			|			ТОГДА NULL
			|		КОГДА МинимальныеДатыИспользованияПодразделений.Дата = ДАТАВРЕМЯ(1, 1, 1)
			|			ТОГДА ЛОЖЬ
			|		ИНАЧЕ ИСТИНА
			|	КОНЕЦ КАК Сформировано,
			|	МинимальныеДатыИспользованияПодразделений.Дата КАК ДатаСоздания,
			|	ВЫБОР
			|		КОГДА МинимальныеДатыИспользованияПодразделений.Дата ЕСТЬ NULL 
			|			ТОГДА NULL
			|		КОГДА ДатыЗакрытияПодразделения.Дата ЕСТЬ NULL 
			|			ТОГДА NULL
			|		КОГДА МинимальныеДатыИспользованияПодразделений.Дата = ДАТАВРЕМЯ(1, 1, 1)
			|				ИЛИ ДатыЗакрытияПодразделения.Дата = ДАТАВРЕМЯ(1, 1, 1)
			|			ТОГДА ЛОЖЬ
			|		ИНАЧЕ ИСТИНА
			|	КОНЕЦ КАК Расформировано,
			|	ВЫБОР
			|		КОГДА МинимальныеДатыИспользованияПодразделений.Дата ЕСТЬ NULL 
			|			ТОГДА NULL
			|		КОГДА ДатыЗакрытияПодразделения.Дата ЕСТЬ NULL 
			|			ТОГДА NULL
			|		КОГДА МинимальныеДатыИспользованияПодразделений.Дата = ДАТАВРЕМЯ(1, 1, 1)
			|			ТОГДА ДАТАВРЕМЯ(1, 1, 1)
			|		ИНАЧЕ ДатыЗакрытияПодразделения.Дата
			|	КОНЕЦ КАК ДатаРасформирования
			|ПОМЕСТИТЬ ВТСформированностьПодразделенийПредварительно
			|ИЗ
			|	ВТОбновляемыеПодразделения КАК ОбновляемыеПодразделения
			|		ЛЕВОЕ СОЕДИНЕНИЕ ВТМинимальныеДатыИспользованияПодразделений КАК МинимальныеДатыИспользованияПодразделений
			|		ПО ОбновляемыеПодразделения.Подразделение = МинимальныеДатыИспользованияПодразделений.Подразделение
			|		ЛЕВОЕ СОЕДИНЕНИЕ ВТДатыЗакрытияПодразделения КАК ДатыЗакрытияПодразделения
			|		ПО ОбновляемыеПодразделения.Подразделение = ДатыЗакрытияПодразделения.Подразделение
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	СформированностьПодразделений.Подразделение,
			|	ВЫБОР
			|		КОГДА ЕСТЬNULL(СведенияПодчиненныхПодразделений.ДатаСоздания, ДАТАВРЕМЯ(1, 1, 1)) <> ДАТАВРЕМЯ(1, 1, 1)
			|			ТОГДА ИСТИНА
			|		ИНАЧЕ ЕСТЬNULL(СформированностьПодразделений.Сформировано, ЛОЖЬ)
			|	КОНЕЦ КАК Сформировано,
			|	ВЫБОР
			|		КОГДА ЕСТЬNULL(СведенияПодчиненныхПодразделений.ДатаСоздания, ДАТАВРЕМЯ(1, 1, 1)) <= ЕСТЬNULL(СформированностьПодразделений.ДатаСоздания, ДАТАВРЕМЯ(1, 1, 1))
			|					И ЕСТЬNULL(СведенияПодчиненныхПодразделений.ДатаСоздания, ДАТАВРЕМЯ(1, 1, 1)) <> ДАТАВРЕМЯ(1, 1, 1)
			|				ИЛИ СформированностьПодразделений.ДатаСоздания ЕСТЬ NULL 
			|			ТОГДА СведенияПодчиненныхПодразделений.ДатаСоздания
			|		КОГДА ЕСТЬNULL(СведенияПодчиненныхПодразделений.ДатаСоздания, ДАТАВРЕМЯ(1, 1, 1)) > ЕСТЬNULL(СформированностьПодразделений.ДатаСоздания, ДАТАВРЕМЯ(1, 1, 1))
			|					И ЕСТЬNULL(СформированностьПодразделений.ДатаСоздания, ДАТАВРЕМЯ(1, 1, 1)) <> ДАТАВРЕМЯ(1, 1, 1)
			|				ИЛИ СведенияПодчиненныхПодразделений.ДатаСоздания ЕСТЬ NULL 
			|			ТОГДА СформированностьПодразделений.ДатаСоздания
			|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
			|	КОНЕЦ КАК ДатаСоздания,
			|	ВЫБОР
			|		КОГДА ПоследниеИспользованияПодразделений.Используется = ИСТИНА
			|			ТОГДА ЛОЖЬ
			|		КОГДА СформированностьПодразделений.Расформировано = ИСТИНА
			|				И СведенияПодчиненныхПодразделений.Расформировано ЕСТЬ NULL 
			|			ТОГДА ИСТИНА
			|		КОГДА СведенияПодчиненныхПодразделений.Расформировано = ИСТИНА
			|				И СформированностьПодразделений.Расформировано ЕСТЬ NULL 
			|			ТОГДА ИСТИНА
			|		КОГДА СведенияПодчиненныхПодразделений.Расформировано = ИСТИНА
			|				И СформированностьПодразделений.Расформировано = ИСТИНА
			|			ТОГДА ИСТИНА
			|		ИНАЧЕ ЛОЖЬ
			|	КОНЕЦ КАК Расформировано,
			|	ВЫБОР
			|		КОГДА ПоследниеИспользованияПодразделений.Используется = ИСТИНА
			|			ТОГДА ДАТАВРЕМЯ(1, 1, 1)
			|		КОГДА СформированностьПодразделений.Расформировано = ИСТИНА
			|				И СведенияПодчиненныхПодразделений.Расформировано ЕСТЬ NULL 
			|			ТОГДА СформированностьПодразделений.ДатаРасформирования
			|		КОГДА СведенияПодчиненныхПодразделений.Расформировано = ИСТИНА
			|				И СформированностьПодразделений.Расформировано ЕСТЬ NULL 
			|			ТОГДА СведенияПодчиненныхПодразделений.ДатаРасформирования
			|		КОГДА СведенияПодчиненныхПодразделений.Расформировано = ИСТИНА
			|				И СформированностьПодразделений.Расформировано = ИСТИНА
			|			ТОГДА ВЫБОР
			|					КОГДА СформированностьПодразделений.ДатаРасформирования >= СведенияПодчиненныхПодразделений.ДатаРасформирования
			|						ТОГДА СформированностьПодразделений.ДатаРасформирования
			|					ИНАЧЕ СведенияПодчиненныхПодразделений.ДатаРасформирования
			|				КОНЕЦ
			|		ИНАЧЕ ДАТАВРЕМЯ(1, 1, 1)
			|	КОНЕЦ КАК ДатаРасформирования
			|ПОМЕСТИТЬ ВТСформированностьПодразделений
			|ИЗ
			|	ВТСформированностьПодразделенийПредварительно КАК СформированностьПодразделений
			|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСведенияПодчиненныхПодразделений КАК СведенияПодчиненныхПодразделений
			|		ПО СформированностьПодразделений.Подразделение = СведенияПодчиненныхПодразделений.Подразделение
			|		ЛЕВОЕ СОЕДИНЕНИЕ ВТПоследниеИспользованияПодразделений КАК ПоследниеИспользованияПодразделений
			|		ПО СформированностьПодразделений.Подразделение = ПоследниеИспользованияПодразделений.Подразделение
			|;
			|
			|////////////////////////////////////////////////////////////////////////////////
			|ВЫБРАТЬ
			|	СформированностьПодразделений.Подразделение,
			|	СформированностьПодразделений.Сформировано,
			|	СформированностьПодразделений.ДатаСоздания,
			|	СформированностьПодразделений.Расформировано,
			|	СформированностьПодразделений.ДатаРасформирования,
			|	СформированностьПодразделений.Подразделение.Родитель КАК Родитель
			|ИЗ
			|	ВТСформированностьПодразделений КАК СформированностьПодразделений
			|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
			|		ПО СформированностьПодразделений.Подразделение = ПодразделенияОрганизаций.Ссылка
			|ГДЕ
			|	(СформированностьПодразделений.Сформировано <> ПодразделенияОрганизаций.Сформировано
			|			ИЛИ СформированностьПодразделений.ДатаСоздания <> ПодразделенияОрганизаций.ДатаСоздания
			|			ИЛИ СформированностьПодразделений.Расформировано <> ПодразделенияОрганизаций.Расформировано
			|			ИЛИ СформированностьПодразделений.ДатаРасформирования <> ПодразделенияОрганизаций.ДатаРасформирования)";
			
		РезультатЗапроса = Запрос.Выполнить();
		Если РезультатЗапроса.Пустой() Тогда
			Прервать;
		Иначе
			
			ОбновляемыеПодразделения.Очистить();
			ТекущиеРодители = Новый Массив;
			
			Выборка = РезультатЗапроса.Выбрать();
			Пока Выборка.Следующий() Цикл
				
				ПодразделениеОбъект = Выборка.Подразделение.ПолучитьОбъект();
				
				Попытка 
					ПодразделениеОбъект.Заблокировать();
				Исключение
					
					ТекстИсключенияЗаписи = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						НСтр("ru = 'Не удалось изменить подразделение ""%1"".
						|Возможно, подразделение редактируется другим пользователем'"),
						ПодразделениеОбъект.Наименование);
						
					ВызватьИсключение ТекстИсключенияЗаписи;
					
				КонецПопытки;
				
				ЗаполнитьЗначенияСвойств(ПодразделениеОбъект, Выборка);
				
				Если НЕ ПодразделениеОбъект.Расформировано И ПодразделениеОбъект.ПометкаУдаления Тогда
					ПодразделениеОбъект.ПометкаУдаления = Ложь;
				КонецЕсли; 
				
				УправлениеШтатнымРасписанием.ОтключитьОбновлениеСтруктурыШтатногоРасписания(ПодразделениеОбъект);
				
				ПодразделениеОбъект.Записать();
				
				Если ЗначениеЗаполнено(Выборка.Родитель) Тогда
					ТекущиеРодители.Добавить(Выборка.Родитель);
				КонецЕсли; 
				
			КонецЦикла;
			
			ОбщегоНазначенияКлиентСервер.ДополнитьМассив(ОбновляемыеПодразделения, ТекущиеРодители, Истина);
			
		КонецЕсли;
		
	КонецЦикла; 
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли